# 0.10.0-beta.6
- `#equal` handlebars helper support else branch

# 0.10.0-beta.5
- Set `Content-Lenght` headers

# 0.10.0-beta.4
- Add condition to composite templates

# 0.10.0-beta.3
- Add raw template (read and return PDF file directly)
- Add helper `markdown`

# 0.10.0-beta.2
- Fix examples and schema on composite template

# 0.10.0-beta.1
- Add composition templates. See `--composition-templates` flag.

# 0.9.2
- Support sub-dir into partials
- Add "Refresh" button to example
- Expand current template menu item pn page reloading
- Independent scroll on render panels

# 0.9.1
- Add `format_time` helper

# 0.9.0
- Support waitLifecycleEvent configuration via template params.

# 0.9.0-beta.2
- Support handlebars template engine, with support partials.
  Partials can be defined in `partials/` dir.
- **INTERNAL** skip test if browser not found
- Backend return internal error
- Add copyable curl command to tester
- Keep only one log per loaded template
- Capture all browser logs
- Downgrade to alpine 3.16
- Reload examples and schema
- Add settings to app bar
- Allow resize inputs and preview panel
- Allow hide inputs and preview panel
- Move examples selector to bottom of page
- Clean selected example when template is switched
- Allow build pre-release (alpha, beta, rc and etc) versions

# 0.9.0-beta.1
- Support partials for golang templates. Partials can be defined in `partials/` dir.
  Markdown partials can be defined with `.md` extension.
- Change partial extension from .md.mustache to .mustache.md
- Mustache template read partials from `partials/` dir instead of `includes/`
- **INTERNAL** Fix all browser warnings on tester
- **INTERNAL** Update tester to react 18
- Add example selector to input panel in tester
- Add reloading templates by passing `--templates-reload-mode` flags.
  Supported values:
  - `none` - legacy behavior. Templates not reload.
  - `always` - templates reload on each request.
- Allow run http server listener with address reuse option (`--server-reuse-addr` flag).
- Render partials with extension '.md.mustache' as markdown
- Cache mustache partials.
  Now partials load only once on startup, and not reload on each request.
- **BREAKING** load mustache partials into `includes/` dir
- **INTERNAL** Update earthly to 0.6.30
- **INTERNAL** Update go to 1.20

# 0.8.0
- **INTERNAL** Remove grid from status page
- Show version into tester UI
- Print version on server startup
- Show error into status page
- Add default chromium arguments required for headless arm64 version
- Add arm64 image (multi-arch)
- **INTERNAL** Update earthly to 0.6.22
- **BREAKING** Disable building of firefox image
- **INTERNAL** Upgrade golang to 1.18
- **INTERNAL** Update base image to alpine 3.16
- **INTERNAL** Not generate mocks on release

# 0.7.2
- Swap (fix) firefox and chromium images.
- Update pool defaults. Now pdf-server start with one idle browser.
- Add status page to Tester UI.
- Define `GET /_status` into `GET /swagger.json`.
- Log link to debug tools after start.
- Add redirect from root (`/`) to static path.
- Add static dir config and bind flags with envvars (see `server --help`).
- **INTERNAL** Update golangci-lint to 1.48.0 and fix new warnings.
- **INTERNAL** Migrate from golint to revive and fix new warnings.

# 0.7.1
- Re-Enable testOnReturn. Invalidate broken render objects.
- Fix "Create page" logging level
- Add lorem ipsum example
- Not start with template without body

# 0.7.0
- Not validate render before return to pool.
- Fix `--version` into images.
- **BRAKING** Fix typo into argument `--render-pool-time-between-eviction`
    (ENV: `RENDER_POOL_TIME_BETWEEN_EVICTION`)
- Add tracing. See `--tracing-*` and `--otel-collector-*` flags.
- Add render tester browser app (`GET /static/tester/`).
- Add swagger API doc page `/static/swagger.html`.
- Add simple `GET /spec/swagger.json` spec endpoint.
  Known issues:
  - not supported json schemas with `$ref` and `$defs` keywords.
- **BRAKING**: Add suffix `-chromium` to chromium based image tags. New tag examples:
  - `latest` -> `latest-chromium`.
  - `v0.7.0` -> `v0.7.0-chromium`.
- Provide firefox based images (image tag have suffix `-firefox`).
  Firefox tag examples:
  - `latest-firefox`.
  - `v0.7.0-firefox`.
- Migrate image build to earthly.
- **BRAKING**: Remove ONBUILD from produced image.
  Now user templates should be add to image explicitly.
  Now minimal Dockerfile look like:
    ```Dockerfile
    FROM registry.gitlab.com/c0va23/pdf-server:latest-chromium

    ADD templates templates
    ```
- Add render timeout, configurable by `render-timeout` argument or
  by `RENDER_TIMEOUT` environment variable.
- Add argument `browser-command` (ENV: `BROWSER_COMMAND`) as new canonical form
  of `chromium-command` (ENV: `CHROMIUM_COMMAND`) argument.
- Add argument `browser-args` (ENV: `BROWSER_ARGS`) as new canonical form of
  `chromium-args` (ENV: `CHROMIUM_ARG`) argument.
- Allow log into JSON format (with `LOG_FORMAT=json` environment variable).
- Status endpoint (`GET /_status`) now get version with CDP client.
  Status return 500, when render pool not ready.
- Add page pool factory. Enabled by `FACTORY_TYPE=page`
- **BRAKING**: Remove `POST /render/:template_name` endpoint
- **BRAKING**: Rename environment variable `CHROMIUM__ARGS` to `CHROMIUM_ARGS`
- Migrate to golang 1.17
- Migrate to alpine 3.15
- Build with Earthly

# 0.6.3
- Log request duration in seconds (float)

# 0.6.2
- Allow configure RPC buffer size via `--factory-rpc-buffer-size`

# 0.6.1
- Fix close page connection
- Up default render-pool-time-between-eviction to 1 hour
- Up default factory-max-usage-count to 1000
- Wrap container process into tini for prevent zombie process
- Add delay before get page attempts. Can be configured via `--factory-get-page-retry-delay`
- Fix CMD into Dockerfile

# 0.6.0
- Fixed graceful shutdown
- Log request_id
- Migrate to golang 1.13
- Fixed race when waiting for page lifecycle event.
- Added argument `--factory-max-usage-count` (`$FACTORY_MAX_USAGE_COUNT`) to configure the maximum number of uses pages from the pool.
- Added argument `--factory-max-get-page-attempts` (`$FACTORY_MAX_GET_PAGE_ATTEMPTS`) to configure the number of attempts to get the default page "about".
- Now pages are always deleted from the pool, not after n seconds of idle time.
- Added argument `--render-pool-time-between-eviction` (`$RENDER_POOL_TIME_BETWEEN_EVICTION`) to configure the frequency of checking the idle pages.
- A page pool creates new browser instances instead of pages in a single browser.
- Expanded the standard list of chromium startup arguments.
- Migrate from github.com/chromedp/chromedp to github.com/mafredri/cdp.
- Add argument `--wait-lifecycle-event`. This argument allows you to specify the
  page event that will be expected before printing to PDF is called.
- Refactor chromerender package.
- Add one year caching for assets.
- Log time with millisecond.

# v0.5.1
- Fix chrome page create
- Fix defaults for render pool

# v0.5.0
- Add versioning

# 2019-08-23
- Add command `validate`. Its command parse all templates and render all
  examples. Command `validate` useful for test templates on CI/CD.

# 2019-08-22
- Add template examples. Examples can added into `examples/` into template
  directory and can by rendered by
  `GET /templates/:template_name/examples/:example_name`.

# 2019-08-21
- Rename render API endpoint to `POST /templates/:template_name/render`
  (from `POST /render/:template_name`)
- Add header and footer templates. Now template directory can contains
  `(header|footer)\.(html|tmpl|mustache)` files. Its templates rendered with
  main template data and replace appropriate values into params JSON.
  Its templates parsed only if params value `pdf.displayHeaderFooter` is equal
  `true`.

# 2019-08-20
- Assets server return `Content-Type` header with value based on asset
  extension.
