import { TabContext, TabPanel } from '@mui/lab'
import { Refresh, Download } from '@mui/icons-material'
import { Button, Tab, Tabs } from '@mui/material'
import { useEffect, useState } from 'react'
import { TemplateMetadata } from '../swaggerFetcher'
import { Box } from '@mui/system'
import { RenderData } from './types'

export interface PreviewProps {
  renderData: RenderData,
  templateMetadata?: TemplateMetadata
}

async function blobToDataUri (blob: Blob): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = () => resolve(reader.result as string)
    reader.onerror = reject
    reader.readAsDataURL(blob)
  })
}

export async function fetchPdf (
  renderPath: string,
  renderData: RenderData,
): Promise<string> {
  return fetch(renderPath, {
    method: 'POST',
    body: JSON.stringify(renderData),
    headers: {
      'Content-Type': 'application/json',
      Cookie: undefined,
    },
  }).then(response => response.blob())
    .then(blobToDataUri)
}

export function PreviewPanel ({
  renderData, templateMetadata,
}: PreviewProps) {
  const a4AspectRation = '210/297'

  const pdfPreview = 'pdf'
  const downloadPreview = 'download'

  const emptyPdfPayload = undefined

  const [currentTab, setCurrentTab] = useState(pdfPreview)
  const [pdfPayload, setPdfPayload] = useState(emptyPdfPayload)

  const updatePdfPayload = () => {
    if (templateMetadata?.renderPath !== undefined) {
      fetchPdf(templateMetadata.renderPath, renderData)
        .then(setPdfPayload)
        .catch(console.error)
    } else {
      setPdfPayload(emptyPdfPayload)
    }
  }

  useEffect(
    updatePdfPayload,
    [templateMetadata, renderData],
  )

  const iconButtonStyles = {
    padding: '1em',
  }

  return (
    <TabContext value={currentTab}>
      <Box sx={{
        display: 'flex',
      }}>
        <Tabs
          onChange={(_event, newValue) => setCurrentTab(newValue)}
          value={currentTab}
        >
          <Tab label="PDF preview" value={pdfPreview} />
        </Tabs>
        <span style={{
          flexGrow: 1,
        }} />
        <Box>
          {pdfPayload && <Button
            variant='text'
            size='small'
            title='Download'
            href={pdfPayload}
            download="preview.pdf"
            sx={iconButtonStyles}
          >
            <Download />
          </Button>}
          <Button
            onClick={updatePdfPayload}
            variant='text'
            title='Reload Preview'
            size='small'
            sx={iconButtonStyles}
          >
            <Refresh />
          </Button>
        </Box>
      </Box>

      <TabPanel value={pdfPreview}>
        {pdfPayload && <iframe
          style={{
            width: '100%',
            aspectRatio: a4AspectRation,
          }}
          sandbox='allow-scripts'
          src={pdfPayload} />}
      </TabPanel>

      <TabPanel value={downloadPreview}>
      </TabPanel>
    </TabContext>
  )
}
