import { Collapse, List, ListItemButton, ListItemText, ListSubheader } from '@mui/material'
import { ExpandLess, ExpandMore } from '@mui/icons-material'
import { useState } from 'react'
import { TemplateMetadata } from '../swaggerFetcher'
import { Link, useLocation } from 'react-router-dom'

interface MenuItemProperties {
    templateMetadata: TemplateMetadata,
    isOpen: boolean,
}

function MenuItem (props: MenuItemProperties) {
  const {
    templateMetadata,
    isOpen,
  } = props

  const [open, setOpen] = useState(isOpen)

  function toggleOpen () {
    setOpen(!open)
  }

  const currentPath = useLocation()

  return (
        <>
            <ListItemButton key={`${templateMetadata.templateName}-toggler`} onClick={toggleOpen}>
                <ListItemText>
                        {templateMetadata.templateName}
                </ListItemText>

                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>

            <Collapse in={open} key={`${templateMetadata.templateName}-submenu`}>
                <List component="div" >
                    <ListItemButton
                        key={`${templateMetadata.templateName}-render`}
                        sx={{ pl: 4 }}
                        component={Link}
                        to={templateMetadata.renderPath}
                        selected={currentPath.pathname === templateMetadata.renderPath}
                    >
                        <ListItemText>
                            Render
                        </ListItemText>
                    </ListItemButton>
                    {templateMetadata.examples.map(example => (
                        <ListItemButton
                            key={`${templateMetadata.templateName}-examples-${example.exampleName}`}
                            sx={{ pl: 4 }}
                            component={Link}
                            to={example.examplePath}
                            selected={currentPath.pathname === example.examplePath}
                        >
                            <ListItemText>
                                Example: {example.exampleName}
                            </ListItemText>
                        </ListItemButton>
                    ))}
                </List>
            </Collapse>
        </>
  )
}

interface MenuProperties {
    templateMetadataList?: TemplateMetadata[]
}

export default function Menu (props: MenuProperties) {
  const { templateMetadataList } = props
  const currentPath = useLocation()

  return (
        <List
            subheader={
                <ListSubheader>Templates</ListSubheader>
            }
        >
            <>
                {templateMetadataList?.map(templateMetadata => (
                    <MenuItem
                        key={templateMetadata.templateName}
                        templateMetadata={templateMetadata}
                        isOpen={isCurrentTemplate(currentPath.pathname, templateMetadata)}
                    />
                ))}
            </>

            <ListItemButton
                key='status-item'
                component={Link}
                to='/status'
                selected={currentPath.pathname === '/status'}
            >
                <ListItemText>
                    Status
                </ListItemText>
            </ListItemButton>

        </List>
  )
}

function isCurrentTemplate (currentPath: string, templateMetadata: TemplateMetadata): boolean {
  const examplesPaths = templateMetadata.examples.map(example => example.examplePath)

  console.debug('isCurrentTemplate', {
    currentPath,
    renderPath: templateMetadata.renderPath,
    examplesPaths,
  })

  return currentPath === templateMetadata.renderPath ||
      examplesPaths.includes(currentPath)
}
