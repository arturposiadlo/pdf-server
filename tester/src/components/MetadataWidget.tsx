import { Typography } from '@mui/material'
import { useEffect, useState } from 'react'

interface VersionInfo {
  version: string
  os: string
  arch: string
}

interface Metadata {
  versionInfo: VersionInfo
}

export default function MetadataWidget () {
  const [metadata, setMetadata] = useState<Metadata>(null)

  useEffect(() => {
    if (metadata !== null) {
      return
    }

    fetch('/_metadata')
      .then(response => response.json())
      .then(setMetadata)
  })

  if (metadata === null) {
    return null
  }

  const { versionInfo } = metadata

  return (
    <Typography align='right'>
      Version: {versionInfo.version}; Platform: {versionInfo.os}/{versionInfo.arch}.
    </Typography>
  )
}
