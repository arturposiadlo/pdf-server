import { AppBar, Box, CssBaseline, IconButton, Paper, Popper, Toolbar, Typography } from '@mui/material'
import { useRef, useState } from 'react'
import MenuIcon from '@mui/icons-material/Menu'
import SettingsIcon from '@mui/icons-material/Settings'
import { HashRouter, Route, Routes, useParams } from 'react-router-dom'
import { fetchSpec, TemplateMetadata } from '../swaggerFetcher'
import { ErrorProvider } from './errors'
import { Example } from './Example'
import Menu from './Menu'
import MetadataWidget from './MetadataWidget'
import { RenderTemplate } from './RenderTemplate'
import { StatusPage } from './StatusPage'
import { TitleContext } from './Title'
import { InfoOutlined } from '@mui/icons-material'

function Welcome () {
  const params = useParams()
  console.debug('Welcome', { params })

  return (
        <div>Select template</div>
  )
}

export default function App () {
  const [templateMetadataList, setTemplateMetadataList] = useState<TemplateMetadata[]>(null)

  if (templateMetadataList === null) {
    fetchSpec().then(setTemplateMetadataList)
  }

  const [drawerOpen, setDrawerOpen] = useState(true)

  const toggleDrawer = () => setDrawerOpen(!drawerOpen)

  const [title, setTitle] = useState(undefined)

  const titleContextData = {
    title,
    setTitle,
  }

  const settingsRef = useRef(null)

  return (
    <HashRouter>
        <CssBaseline />
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position='static'>
                <Toolbar>
                    <IconButton onClick={toggleDrawer}>
                      <MenuIcon />
                    </IconButton>

                    <Typography sx={{ flexGrow: 1 }}>
                        PDF server {title && `/ ${title}`}
                    </Typography>

                    <SettingsButton settingsRef={settingsRef} />
                    <InfoButton />
                </Toolbar>
            </AppBar>
            <Box sx={{ flexDirection: 'row', display: 'flex' }}>
                <Box sx={{
                  flex: '0 0 20em',
                  overflowY: 'scroll',
                  scrollbarWidth: 'thin',
                  maxHeight: '90vh',
                  display: drawerOpen ? undefined : 'none',
                }}>
                    <Menu templateMetadataList={templateMetadataList} />
                </Box>

                <Box sx={{
                  flex: 1,
                }}>
                    <ErrorProvider>
                        <TitleContext.Provider value={titleContextData}>
                          <MainRouter
                            templateMetadataList={templateMetadataList}
                            settingsRef={settingsRef}
                          />
                        </TitleContext.Provider>
                    </ErrorProvider>
                </Box>
            </Box>
        </Box>
    </HashRouter>)
}

interface MainRouterProps {
  templateMetadataList?: TemplateMetadata[]
  settingsRef: React.RefObject<HTMLDivElement>,
}

function MainRouter (props: MainRouterProps) {
  const {
    templateMetadataList,
    settingsRef,
  } = props

  return (
      <Routes>
          <Route
              path='/templates/:templateName/render'
              element={<RenderTemplate
                  templateMetadataList={templateMetadataList}
                  settingsRef={settingsRef}
              />}
          />
          <Route
              path='/templates/:templateName/examples/:exampleName'
              element={<Example
                  templateMetadataList={templateMetadataList}
              />}
          />
          <Route
              path='/status'
              element={<StatusPage />}
          />
          <Route path='*' element={<Welcome />} />
      </Routes>
  )
}

const InfoButton = () => {
  const [infoOpen, setInfoOpen] = useState(false)
  const infoButtonId = 'info-button'

  return (
    <>
      <IconButton
        id={infoButtonId}
        onClick={() => setInfoOpen(!infoOpen)}
      >
        <InfoOutlined />
      </IconButton>

      <Popper
        open={infoOpen}
        anchorEl={() => document.getElementById(infoButtonId)}
        placement='bottom-end'
      >
        <Paper>
          <Box sx={{ padding: '1em' }}>
            <MetadataWidget />
          </Box>
        </Paper>
      </Popper>
    </>
  )
}

const SettingsButton = ({ settingsRef }) => {
  const [isOpen, setIsOpen] = useState(false)
  const renameButtonId = 'settings-button'

  return (
    <>
      <IconButton
        id={renameButtonId}
        onClick={() => setIsOpen(!isOpen)}
      >
        <SettingsIcon />
      </IconButton>

      <Popper
        open={isOpen}
        anchorEl={() => document.getElementById(renameButtonId)}
        placement='bottom-end'
        keepMounted={true}
      >
        <Paper>
          <Box sx={{ padding: '1em' }} ref={settingsRef}>
            <Typography variant='h5'>Settings</Typography>
          </Box>
        </Paper>
      </Popper>
    </>
  )
}
