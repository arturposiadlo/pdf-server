import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Checkbox,
  FormControlLabel,
  FormGroup,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TextField,
  Typography,
} from '@mui/material'
import { Theme } from '@mui/system'
import { useContext, useEffect, useState } from 'react'
import { ErrorContext } from './errors'

type RawMetadata = Record<string, string>
type RawPoolStatsItem = Record<string, number>

interface StatusData {
  render_pool_type: string
  pool_stats: RawPoolStatsItem
  metadata: RawMetadata
}

function projectMetadata (rawMetadata: RawMetadata): string[][] {
  return Object
    .entries<string>(rawMetadata)
}

function projectPoolStatsItems (rawPoolStatsItems: RawPoolStatsItem): string[][] {
  return Object
    .entries<number>(rawPoolStatsItems)
    .map(([label, value]) => ([label, value.toString()]))
}

const defaultLiveUpdate = true
const defaultUpdateInterval = 5_000

const sectionStyles: SxProps<Theme> = {
  margin: '1em 0',
}

export function StatusPage () {
  const [statusData, setStatusData] = useState<StatusData>(undefined)
  const [lastUpdateTime, setLastUpdateTime] = useState<Date>(undefined)

  const [liveUpdate, setLiveUpdate] = useState(defaultLiveUpdate)

  const [updateInterval, setUpdateInterval] = useState(defaultUpdateInterval)

  const { showError } = useContext(ErrorContext)

  const updateStatusData =
      () => fetch('/_status', {})
        .then(async response => {
          if (!response.ok) {
            throw new Error(await response.text())
          }

          return await response.json()
        })
        .then(newStatusData => {
          setStatusData(newStatusData)
          setLastUpdateTime(new Date())
        })
        .catch(error => showError(error.message))

  useEffect(() => {
    if (!liveUpdate) return

    updateStatusData()

    const timer = setInterval(
      updateStatusData,
      updateInterval,
    )

    return () => clearInterval(timer)
  }, [updateInterval, liveUpdate])

  return (
      <Box
        sx={{
          padding: '1em',
        }}
      >
        <Typography
          variant='h5'
        >
          Status
        </Typography>

        <Box sx={sectionStyles}>
          <Typography variant='body1'>
            Render Pool Type: {statusData?.render_pool_type}
          </Typography>
        </Box>

        <Box sx={sectionStyles}>

          <Typography variant='h5'>
            Pool Stats
          </Typography>

          <Box sx={{
            width: '100%',
          }}>
            {statusData !== undefined && (
              <SimpleTable
                rows={projectPoolStatsItems(statusData.pool_stats)}
              />)
            }
          </Box>
        </Box>

        <Box sx={sectionStyles}>
          <Typography variant='h5'>
            Metadata
          </Typography>

          <Box sx={{
            width: '100%',
          }}>

            {statusData !== undefined && (
              <SimpleTable
                rows={projectMetadata(statusData.metadata)}
              />)
            }
          </Box>
        </Box>

        <Box sx={sectionStyles}>
          <Typography variant='h5'>
            Updates
          </Typography>

          <Typography>
            Last update time: {lastUpdateTime?.toISOString()}
          </Typography>

          <Accordion>
            <AccordionSummary>
              Raw Status Data
            </AccordionSummary>
            <AccordionDetails>
              <code>
                {statusData !== undefined && JSON.stringify(statusData, null, 2)}
              </code>
            </AccordionDetails>
          </Accordion>
        </Box>

        <Box sx={sectionStyles}>
          <Typography variant='h5'>
            Settings
          </Typography>

          <FormGroup>
            <FormControlLabel
              label="Live update"
              control={
                <Checkbox
                  checked={liveUpdate}
                  onChange={input => setLiveUpdate(input.target.checked)}
                />}
            />
            <TextField
              label="Update interval"
              type='number'
              value={updateInterval}
              onChange={input => setUpdateInterval(parseInt(input.target.value))}
            />
          </FormGroup>
        </Box>
      </Box>
  )
}

interface SimpleTableProps {
  rows: string[][]
}

function SimpleTable (props: SimpleTableProps) {
  return (
    <Table>
      <TableBody>
        {props.rows.map(row => (
          <TableRow>
            {row[0] && (
              <TableCell sx={{
                width: '100px',
                fontWeight: '700',
              }}>
                {row[0]}
              </TableCell>
            )}

            {row.slice(1).map(cell => (
              <TableCell>
                {cell}
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}
