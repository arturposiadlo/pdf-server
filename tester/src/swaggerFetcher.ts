import groupBy from 'lodash/groupBy'

const specPath = '/spec/swagger.json'

interface ExampleMetadata {
    exampleName: string
    examplePath: string
}

export interface TemplateMetadata {
    templateName: string
    renderPath: string
    schemaPath?: string
    examples: ExampleMetadata[]
    examplesDataPath?: string
}

interface SchemaSpec {
  '$ref': string
}

interface ParameterSpec {
  name: string
  in: string
  required: true
  schema: SchemaSpec
}

interface OperationSpec {
    tags: string[]
    parameters: ParameterSpec[]
}

interface PathSpec {
    get?: OperationSpec
    post?: OperationSpec
}

type PathsSpec = Record<string, PathSpec>

interface SwaggerSpec {
    paths: PathsSpec
}

const DATA_PARAMETER = 'data'

const TEMPLATE_TAG_PREFIX = 'template:'
const RENDER_TAG = '_render'
const EXAMPLES_TAG = '_examples'
const EXAMPLES_DATA_TAG = '_examplesData'

function extractTemplateName ({ tags }: {tags: string[]}): string {
  for (const tag of tags) {
    if (tag.startsWith(TEMPLATE_TAG_PREFIX)) {
      return tag.slice(TEMPLATE_TAG_PREFIX.length)
    }
  }
}

const PATH_SEPARATOR = '/'

function extractExampleName (examplePath: string): string {
  return examplePath.slice(examplePath.lastIndexOf(PATH_SEPARATOR) + 1)
}

function parserSwaggerSpec (swaggerSpec: SwaggerSpec): TemplateMetadata[] {
  interface PathWithOperation{
    path: string
    operation: OperationSpec
  }

  const operationWithPaths: PathWithOperation[] = Object
    .entries(swaggerSpec.paths)
    .flatMap(([path, { get, post }]) => [
      { path, operation: get },
      { path, operation: post },
    ])
    .filter(({ operation }) =>
      operation !== undefined && (
        operation.tags.includes(RENDER_TAG) ||
          operation.tags.includes(EXAMPLES_DATA_TAG) ||
          operation.tags.includes(EXAMPLES_TAG)
      ),
    )

  const templateOperations = Object.entries<PathWithOperation[]>(groupBy(
    operationWithPaths,
    ({ operation }) => extractTemplateName(operation),
  )).map(([templateName, operationWithPaths]) => {
    const renderOperationWithPath = operationWithPaths
      .find(({ operation }) => operation.tags.includes(RENDER_TAG))

    const renderPath = renderOperationWithPath.path

    const schemaPath = renderOperationWithPath.operation
      .parameters?.find(parameter => parameter.name === DATA_PARAMETER)?.schema?.$ref

    const examples = operationWithPaths
      .filter(({ operation }) => operation.tags.includes(EXAMPLES_TAG))
      .map(({ path }) => {
        const exampleName = extractExampleName(path)

        return {
          exampleName,
          examplePath: path,
        }
      })

    const examplesDataPath = operationWithPaths
      .find(({ operation }) => operation.tags.includes(EXAMPLES_DATA_TAG))
      ?.path

    return {
      templateName,
      renderPath,
      schemaPath,
      examples,
      examplesDataPath,
    }
  })

  return templateOperations
}

export async function fetchSpec (): Promise<TemplateMetadata[]> {
  try {
    const response = await fetch(specPath)
    const json = await response.json()

    return parserSwaggerSpec(json)
  } catch (error) {
    console.error(error)
    throw error
  }
}
