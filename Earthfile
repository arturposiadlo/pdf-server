VERSION 0.7

# Alpine 3.17 after 2023.03.28 have broken chromium (with dbus issue).
ARG --global ALPINE_VERSION=3.16

ARG --global USERPLATFORM
ARG --global USEROS
ARG --global USERARCH

base-node:
	ARG NODE_VERSION=16 # LTS

	FROM node:${NODE_VERSION}-alpine${ALPINE_VERSION}

	WORKDIR /app

	RUN apk --update-cache add \
		git

	RUN npm install -g npm@latest

	SAVE IMAGE --cache-hint

build:
	ARG GO_VERSION=1.20

	FROM golang:${GO_VERSION}-alpine${ALPINE_VERSION}

	# Static
	ENV BROWSER_COMMAND=chromium-browser

	RUN apk --update-cache add \
		make \
		curl \
		git \
		musl-dev \
		chromium

	ARG WORK_DIR=/app
	WORKDIR ${WORK_DIR}

	COPY go.mod go.sum Makefile .
	RUN make -r cache-dependencies

	COPY --dir app cmd examples entrypoint.sh ${WORK_DIR}
	RUN make -r gen

	COPY \
		--platform=${USERPLATFORM} \
		./tester+build/dist/ \
		./app/server/static/tester/

	SAVE IMAGE --cache-hint

binary:
	FROM +build

	ARG APP_VERSION=develop
	ENV VERSION=${APP_VERSION}

	ARG GOOS=${USEROS}
	ARG GOARCH=${USERARCH}

	RUN make -r build

	SAVE ARTIFACT ./bin/pdf-server ./bin/pdf-server AS LOCAL ./bin/pdf-server
	SAVE IMAGE --cache-hint

check:
	BUILD +test
	BUILD +lint
	BUILD +cspell
	BUILD +ec

all-checks:
	BUILD +check
	BUILD ./tester+check
	BUILD ./integration-tests+all-tests

testing-base:
	FROM +build

	RUN make mocks

test:
	FROM +testing-base

	RUN make -r test

lint:
	FROM +testing-base

	COPY .golangci.yml .

	ARG GOLANGCI_LINT_CACHE=/tmp/golangci-lint/cache
	RUN --mount=type=cache,target=${GOLANGCI_LINT_CACHE} \
		GOLANGCI_LINT_CACHE=${GOLANGCI_LINT_CACHE} \
			make -r lint

cspell:
	FROM +base-node

	ARG CSPELL_VERSION=5.20.0

	RUN npm install -g cspell@${CSPELL_VERSION}

	COPY . .

	RUN --mount=type=cache,target=/tmp/.cspellcache \
		time \
			cspell \
			--cache \
			--gitignore \
			--cache-location=/tmp/.cspellcache \
			--dot \
			**/* \
			**/**/* \
			**/**/**/* \
			**/**/**/**/* \
			**/**/**/**/**/* \
			**/**/**/**/**/**/* \
			**/**/**/**/**/**/**/*

ec:
	FROM mstruebing/editorconfig-checker:2.4.0

	COPY . .

	RUN time \
		ec .

USER:
	COMMAND

	RUN addgroup -S pdf-server \
		&& adduser -S pdf-server -G pdf-server

	USER pdf-server

COMMIT:
	COMMAND

	ARG GIT_COMMIT_SHA
	LABEL GIT_COMMIT_SHA=${GIT_COMMIT_SHA}

base-image:
	FROM alpine:${ALPINE_VERSION}

	ENV SERVER_ADDR=:9999

	ENV TEMPLATES_DIR=/src/templates/
	WORKDIR /src

	RUN apk add --update --no-cache tini \
		&& mkdir -p ${TEMPLATES_DIR}

	ENTRYPOINT [ "/sbin/tini", "--", "/usr/bin/entrypoint.sh" ]

	COPY entrypoint.sh /usr/bin/

	CMD [ "server" ]


chromium-base-image:
	FROM +base-image

	RUN apk add --update --no-cache chromium

	ENV BROWSER_COMMAND=chromium-browser

	SAVE IMAGE --cache-hint

chromium-image:
	ARG TARGETOS
	ARG TARGETARCH

	FROM +chromium-base-image

	COPY --platform=$USERPLATFORM ( \
		+binary/bin/pdf-server \
		--GOOS=${TARGETOS} \
		--GOARCH=${TARGETARCH} \
	) /usr/bin/

	DO +USER
	DO +COMMIT

	ARG PDF_SERVER_IMAGE=${PDF_SERVER_IMAGE}
	SAVE IMAGE --push ${PDF_SERVER_IMAGE}

firefox-base-image:
	FROM +base-image

	RUN apk add --update --no-cache firefox ttf-dejavu

	ENV BROWSER_COMMAND=firefox
	ENV BROWSER_ARGS="--new-instance,--headless,--profile,%{TMP_DIR},-P,%{TMP_ID}"

	SAVE IMAGE --cache-hint

firefox-image:
	FROM +firefox-base-image

	COPY (+binary/bin/pdf-server) /usr/bin/

	DO +USER
	DO +COMMIT

	ARG PDF_SERVER_IMAGE=${PDF_SERVER_IMAGE}
	SAVE IMAGE --push ${PDF_SERVER_IMAGE}

all-images:
	# Trigger dependency builds
	BUILD ./tester+build

	BUILD \
		--platform=linux/amd64 \
		--platform=linux/arm64 \
		+chromium-base-image

	# ARG FIREFOX_IMAGE
	# BUILD +firefox-image \
	# 	--PDF_SERVER_IMAGE=${FIREFOX_IMAGE} \
	# 	--APP_VERSION=${APP_VERSION}

	ARG CHROMIUM_IMAGE
	BUILD \
		--platform=linux/amd64 \
		--platform=linux/arm64 \
		+chromium-image \
		--PDF_SERVER_IMAGE=${CHROMIUM_IMAGE}
