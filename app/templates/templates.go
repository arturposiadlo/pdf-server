package templates

import (
	"context"
)

// Data for render and examples.
type Data map[string]interface{}

// Examples map.
type Examples map[string]Data

// Name of template.
type Name string

// Template interface.
type Template interface {
	GetName() Name
	GetExamples() Examples
	GetValidator() Validator
	Render(ctx context.Context, data Data) ([]byte, error)
}
