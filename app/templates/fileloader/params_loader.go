package fileloader

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/c0va23/pdf-server/app/log"
)

const (
	paramsFileName = "params.json"
)

// FileParamsLoader load params from file.
type FileParamsLoader struct{}

// NewFileParamsLoader constructor.
func NewFileParamsLoader() *FileParamsLoader {
	return new(FileParamsLoader)
}

// LoadParams from templateDirPath.
func (*FileParamsLoader) LoadParams(
	templateDirPath string,
	logger log.Logger,
) (*Params, error) {
	logger = logger.WithField(log.FieldTemplateLoader, "params")

	paramsPath := filepath.Join(templateDirPath, paramsFileName)

	paramsFile, err := os.Open(paramsPath) /* #nosec */
	if os.IsNotExist(err) {
		logger.Debug("Params not found")

		return nil, nil
	} else if nil != err {
		return nil, fmt.Errorf("open params: %w", err)
	}

	var params Params
	if err = json.NewDecoder(paramsFile).Decode(&params); nil != err {
		return nil, fmt.Errorf("decode params: %w", err)
	}

	logger.Debug("Params loaded")

	return &params, nil
}
