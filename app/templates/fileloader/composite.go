package fileloader

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/itchyny/gojq"
	pdfAPI "github.com/pdfcpu/pdfcpu/pkg/api"
	"gopkg.in/yaml.v3"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

const (
	compositeTemplateFileName     = "composition.yaml"
	compositionTemplateNamePrefix = "composition:"
)

type compositionTemplateConditionDefinition struct {
	Jq *string `yaml:"jq"`
}

type compositionTemplateDefinition struct {
	TemplateName string                                  `yaml:"templateName"`
	Condition    *compositionTemplateConditionDefinition `yaml:"if"`
}

type compositionDefinition struct {
	Templates []compositionTemplateDefinition `yaml:"templates"`
}

func newCompositionDefinition() compositionDefinition {
	return compositionDefinition{
		Templates: []compositionTemplateDefinition{},
	}
}

type compositionLoader struct {
	store          templates.Store
	schemaLoader   templates.SchemaLoader
	examplesLoader templates.ExamplesLoader
}

// LoadTemplate load template dir.
func (loader *compositionLoader) LoadTemplate(
	templatesDirPath string,
	templateName string,
	logger log.Logger,
) (templates.Template, error) {
	// Truncate prefix on reload (dev mode).
	templateName = strings.TrimPrefix(templateName, compositionTemplateNamePrefix)

	templateDirPath := filepath.Join(templatesDirPath, templateName)

	templateLogger := logger.
		WithField(log.FieldTemplateLoader, "composition").
		WithField(log.FieldTemplateName, templateName)

	definition, err := loader.loadDefinition(templateDirPath)
	if err != nil {
		return nil, fmt.Errorf("load definition: %w", err)
	}

	items, err := loader.parseTemplateDefinitionList(definition.Templates)
	if err != nil {
		return nil, fmt.Errorf("parse template definition list: %w", err)
	}

	schema, err := loader.schemaLoader.LoadSchema(templateDirPath, templateLogger)
	if err != nil {
		return nil, fmt.Errorf("load schema: %w", err)
	}

	examples, err := loader.examplesLoader.LoadExamples(templateDirPath, templateLogger)
	if err != nil {
		return nil, fmt.Errorf("load examples: %w", err)
	}

	templateLogger.Infof("Composition is loaded")

	return &compositeTemplate{
		name:     templates.Name(compositionTemplateNamePrefix + templateName),
		schema:   schema,
		examples: examples,
		items:    items,
	}, nil
}

func (loader *compositionLoader) loadDefinition(
	templateDirPath string,
) (compositionDefinition, error) {
	templatePath := filepath.Join(templateDirPath, compositeTemplateFileName)

	file, err := os.Open(templatePath)
	if err != nil {
		if os.IsNotExist(err) {
			return compositionDefinition{}, NewTemplateFileNotFoundError(loader.TemplateType(), templatePath)
		}

		return compositionDefinition{}, fmt.Errorf("open file: %w", err)
	}

	definition := newCompositionDefinition()

	err = yaml.NewDecoder(file).Decode(&definition)
	if err != nil {
		return compositionDefinition{}, fmt.Errorf("decode file: %w", err)
	}

	return definition, nil
}

func (loader *compositionLoader) parseTemplateDefinitionList(
	templateDefinitions []compositionTemplateDefinition,
) ([]compositeTemplateItem, error) {
	items := make([]compositeTemplateItem, 0, len(templateDefinitions))

	for _, templateDefinition := range templateDefinitions {
		item, err := loader.parseTemplateDefinition(templateDefinition)
		if err != nil {
			return nil, fmt.Errorf("parse template definition: %s: %w", templateDefinition.TemplateName, err)
		}

		items = append(items, item)
	}

	return items, nil
}

func (loader *compositionLoader) parseTemplateDefinition(
	templateDefinition compositionTemplateDefinition,
) (compositeTemplateItem, error) {
	template, err := loader.store.Fetch(templates.Name(templateDefinition.TemplateName))
	if err != nil {
		return compositeTemplateItem{},
			fmt.Errorf("get template %s: %w", templateDefinition.TemplateName, err)
	}

	condition, err := loader.parseTemplateConditionDefinition(templateDefinition.Condition)
	if err != nil {
		return compositeTemplateItem{},
			fmt.Errorf("parse condition (.if): %w", err)
	}

	return compositeTemplateItem{
		template:  template,
		condition: condition,
	}, nil
}

func (*compositionLoader) parseTemplateConditionDefinition(
	conditionDefinition *compositionTemplateConditionDefinition,
) (*compositeTemplateItemCondition, error) {
	if conditionDefinition == nil {
		return nil, nil
	}

	if conditionDefinition.Jq == nil {
		return nil, &parseCompositionError{
			message: "jq is required",
		}
	}

	jqQuery, err := gojq.Parse(*conditionDefinition.Jq)
	if err != nil {
		return nil, fmt.Errorf("parse jq: %w", err)
	}

	return &compositeTemplateItemCondition{
		jqQuery: jqQuery,
	}, nil
}

// TemplateType return template type.
func (*compositionLoader) TemplateType() string {
	return "composition"
}

type compositionLoaderBuilder struct {
	schemaLoader   templates.SchemaLoader
	examplesLoader templates.ExamplesLoader
}

// NewCompositionLoaderBuilder constructor.
func NewCompositionLoaderBuilder(
	schemaLoader templates.SchemaLoader,
	examplesLoader templates.ExamplesLoader,
) *compositionLoaderBuilder {
	return &compositionLoaderBuilder{
		schemaLoader:   schemaLoader,
		examplesLoader: examplesLoader,
	}
}

// BuildCompositionLoader build template loader.
func (builder *compositionLoaderBuilder) BuildTemplateLoader(
	store templates.Store,
) (templates.TemplateLoader, error) {
	return &compositionLoader{
		store:          store,
		schemaLoader:   builder.schemaLoader,
		examplesLoader: builder.examplesLoader,
	}, nil
}

type compositeTemplateItemCondition struct {
	jqQuery *gojq.Query
}

type compositeTemplateItem struct {
	template  templates.Template
	condition *compositeTemplateItemCondition
}

type compositeTemplate struct {
	name     templates.Name
	examples templates.Examples
	schema   templates.Validator
	items    []compositeTemplateItem
}

// GetName return template name.
func (template *compositeTemplate) GetName() templates.Name {
	return template.name
}

// GetExamples return template examples.
func (template *compositeTemplate) GetExamples() templates.Examples {
	return template.examples
}

// GetValidator return template validator.
func (template *compositeTemplate) GetValidator() templates.Validator {
	return template.schema
}

// Render render template.
func (template *compositeTemplate) Render(
	ctx context.Context,
	data templates.Data,
) ([]byte, error) {
	items := template.filterTemplates(ctx, data)

	renderResults := make([]io.ReadSeeker, 0, len(items))

	for _, item := range items {
		templateLogger := log.LoggerFromContext(ctx, "compositeTemplate").
			WithField(log.FieldTemplateName, item.template.GetName())

		templateLogger.Infof("Render template")

		renderResult, err := item.template.Render(ctx, data)
		if err != nil {
			return nil, fmt.Errorf("render template %s: %w", item.template.GetName(), err)
		}

		renderResults = append(renderResults, bytes.NewReader(renderResult))
	}

	mergedResult := new(bytes.Buffer)

	err := pdfAPI.MergeRaw(renderResults, mergedResult, nil)
	if err != nil {
		return nil, fmt.Errorf("merge templates: %w", err)
	}

	return mergedResult.Bytes(), nil
}

func (template *compositeTemplate) filterTemplates(
	ctx context.Context,
	data templates.Data,
) []compositeTemplateItem {
	items := make([]compositeTemplateItem, 0, len(template.items))

	for _, templateItem := range template.items {
		if matchItemCondition(ctx, templateItem.condition, data) {
			items = append(items, templateItem)
		}
	}

	return items
}

func matchItemCondition(
	ctx context.Context,
	condition *compositeTemplateItemCondition,
	data templates.Data,
) bool {
	if condition == nil {
		return true
	}

	if condition.jqQuery == nil {
		// Unreachable
		return false
	}

	result, found := condition.jqQuery.RunWithContext(ctx, map[string]any(data)).Next()
	if !found {
		return false
	}

	log.LoggerFromContext(ctx, "compositeTemplateItemCondition").
		WithFields(map[string]any{
			"data":   data,
			"query":  condition.jqQuery.String(),
			"result": result,
		}).
		Debug("matcher result")

	resultBool, resultIsBool := result.(bool)

	return resultIsBool && resultBool
}

type parseCompositionError struct {
	message string
}

func (err *parseCompositionError) Error() string {
	return fmt.Sprintf("parse composition: %s", err.message)
}
