package fileloader

import (
	"errors"
	"fmt"
)

// TemplateFileNotFoundError returned when template not founded on disk.
type TemplateFileNotFoundError struct {
	templateType string
	templatePath string
}

// NewTemplateFileNotFoundError construct new TemplateFileNotFoundError.
func NewTemplateFileNotFoundError(
	templateType string,
	templatePath string,
) *TemplateFileNotFoundError {
	return &TemplateFileNotFoundError{
		templateType: templateType,
		templatePath: templatePath,
	}
}

func (err *TemplateFileNotFoundError) Error() string {
	return fmt.Sprintf(
		"not found template %s: %s",
		err.templateType,
		err.templatePath,
	)
}

// Is for errors.Is.
func (err *TemplateFileNotFoundError) Is(otherErr error) bool {
	if castedOtherErr := new(TemplateFileNotFoundError); errors.As(otherErr, &castedOtherErr) {
		return *castedOtherErr == *err
	}

	return false
}

// MarkdownHelpers helpers for markdown.
type MarkdownHelpers interface {
	Markdown(source string) string
}
