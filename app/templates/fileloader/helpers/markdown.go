package helpers

import "fmt"

// MarkdownProcessor preprocess markdown.
type MarkdownProcessor interface {
	PreprocessString(
		rawPayload []byte,
	) ([]byte, error)
}

// MarkdownHelpers helpers for markdown.
type MarkdownHelpers struct {
	processor MarkdownProcessor
}

// NewMarkdownProcessor constructor.
func NewMarkdownProcessor(processor MarkdownProcessor) *MarkdownHelpers {
	return &MarkdownHelpers{
		processor: processor,
	}
}

// Markdown render markdown to html.
func (helpers *MarkdownHelpers) Markdown(source string) string {
	html, err := helpers.processor.PreprocessString([]byte(source))
	if err != nil {
		panic(fmt.Sprintf("Markdown error: %s", err))
	}

	return string(html)
}
