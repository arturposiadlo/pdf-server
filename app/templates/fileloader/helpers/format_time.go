package helpers

import (
	"fmt"
	"time"

	"github.com/lestrrat-go/strftime"
)

// FormatTime formats time string with strftime format.
func FormatTime(formatString string, timeStr string) (string, error) {
	timeValue, err := time.Parse(time.RFC3339, timeStr)
	if err != nil {
		return "", fmt.Errorf("parse time: %w", err)
	}

	formattedTime, err := strftime.Format(formatString, timeValue)
	if err != nil {
		return "", fmt.Errorf("format time: %w", err)
	}

	return formattedTime, nil
}

// MustFormatTime formats time string with strftime format. Panic on invalid format or time value.
func MustFormatTime(formatString string, timeStr string) string {
	result, err := FormatTime(formatString, timeStr)
	if err != nil {
		panic(err)
	}

	return result
}
