package fileloader_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"

	pdfRenderMocks "gitlab.com/c0va23/pdf-server/mocks/pdfrender"
	templatesMocks "gitlab.com/c0va23/pdf-server/mocks/templates"
	fileloaderMocks "gitlab.com/c0va23/pdf-server/mocks/templates/fileloader"
)

type DefaultRendererTestCase struct {
	name             string
	fetchTemplate    fileloader.HTML2PDFTemplate
	fetchError       error
	renderHTMLResult *pdfrender.Document
	expectedParams   *fileloader.PDFParams
	expectedPayload  []byte
	expectedError    error
}

func defaultRendererTestCases(
	templateName templates.Name,
	templateData templates.Data,
	examples templates.Examples,
) []DefaultRendererTestCase {
	var (
		renderError        = errors.New("render error")
		emptyPayload       = ""
		validBodyPayload   = "<p>valid body</p>"
		validHeaderPayload = "<p>valid header</p>"
		validFooterPayload = "<p>valid footer</p>"

		landscape = true

		validParams = fileloader.Params{
			PDF: fileloader.PDFParams{
				Landscape: &landscape,
			},
		}
	)

	templateInstanceMock := func(payload string, err error) fileloader.HTMLTemplate {
		templateInstance := new(fileloaderMocks.HTMLTemplate)

		templateInstance.
			EXPECT().Render(templateData).
			Return(payload, err)

		return templateInstance
	}

	validatorMock := func(validateError error) templates.Validator {
		validatorMock := new(templatesMocks.Validator)

		validatorMock.EXPECT().ValidateData(mock.Anything, mock.Anything).Return(validateError)

		return validatorMock
	}

	return []DefaultRendererTestCase{
		{
			name:       "body render error",
			fetchError: nil,
			fetchTemplate: fileloader.HTML2PDFTemplate{
				Name:     templateName,
				Body:     templateInstanceMock(emptyPayload, renderError),
				Header:   nil,
				Footer:   nil,
				Examples: examples,
			},
			renderHTMLResult: nil,
			expectedError:    renderError,
		},
		{
			name:       "body success",
			fetchError: nil,
			fetchTemplate: fileloader.HTML2PDFTemplate{
				Name:     templateName,
				Body:     templateInstanceMock(validBodyPayload, nil),
				Header:   nil,
				Footer:   nil,
				Params:   nil,
				Examples: examples,
			},
			renderHTMLResult: &pdfrender.Document{
				TemplateName: pdfrender.TemplateName(templateName),
				BodyHTML:     validBodyPayload,
				HeaderHTML:   nil,
				FooterHTML:   nil,
			},
			expectedParams: nil,
			expectedError:  nil,
		},
		{
			name:       "body with prams success",
			fetchError: nil,
			fetchTemplate: fileloader.HTML2PDFTemplate{
				Name:     templateName,
				Body:     templateInstanceMock(validBodyPayload, nil),
				Header:   nil,
				Footer:   nil,
				Params:   &validParams,
				Examples: examples,
			},
			renderHTMLResult: &pdfrender.Document{
				TemplateName: pdfrender.TemplateName(templateName),
				BodyHTML:     validBodyPayload,
				HeaderHTML:   nil,
				FooterHTML:   nil,
			},
			expectedParams: &validParams.PDF,
			expectedError:  nil,
		},
		{
			name:       "header render error",
			fetchError: nil,
			fetchTemplate: fileloader.HTML2PDFTemplate{
				Name:     templateName,
				Body:     templateInstanceMock(validBodyPayload, nil),
				Header:   templateInstanceMock(emptyPayload, renderError),
				Footer:   nil,
				Params:   &validParams,
				Examples: examples,
			},
			renderHTMLResult: nil,
			expectedError:    renderError,
		},
		{
			name:       "header render success",
			fetchError: nil,
			fetchTemplate: fileloader.HTML2PDFTemplate{
				Name:     templateName,
				Body:     templateInstanceMock(validBodyPayload, nil),
				Header:   templateInstanceMock(validHeaderPayload, nil),
				Footer:   nil,
				Params:   &validParams,
				Examples: examples,
			},
			renderHTMLResult: &pdfrender.Document{
				TemplateName: pdfrender.TemplateName(templateName),
				BodyHTML:     validBodyPayload,
				HeaderHTML:   &validHeaderPayload,
				FooterHTML:   nil,
			},
			expectedParams: &validParams.PDF,
			expectedError:  nil,
		},
		{
			name:       "footer render error",
			fetchError: nil,
			fetchTemplate: fileloader.HTML2PDFTemplate{
				Name:     templateName,
				Body:     templateInstanceMock(validBodyPayload, nil),
				Header:   nil,
				Footer:   templateInstanceMock(emptyPayload, renderError),
				Params:   &validParams,
				Examples: examples,
			},
			renderHTMLResult: nil,
			expectedError:    renderError,
		},
		{
			name:       "footer render success",
			fetchError: nil,
			fetchTemplate: fileloader.HTML2PDFTemplate{
				Name:     templateName,
				Body:     templateInstanceMock(validBodyPayload, nil),
				Header:   nil,
				Footer:   templateInstanceMock(validFooterPayload, nil),
				Params:   &validParams,
				Examples: examples,
			},
			renderHTMLResult: &pdfrender.Document{
				TemplateName: pdfrender.TemplateName(templateName),
				BodyHTML:     validBodyPayload,
				HeaderHTML:   nil,
				FooterHTML:   &validFooterPayload,
			},
			expectedParams: &validParams.PDF,
			expectedError:  nil,
		},
		{
			name:       "all features",
			fetchError: nil,
			fetchTemplate: fileloader.HTML2PDFTemplate{
				Name:      templateName,
				Body:      templateInstanceMock(validBodyPayload, nil),
				Header:    templateInstanceMock(validHeaderPayload, nil),
				Footer:    templateInstanceMock(validFooterPayload, nil),
				Params:    &validParams,
				Validator: validatorMock(nil),
				Examples:  examples,
			},
			renderHTMLResult: &pdfrender.Document{
				TemplateName: pdfrender.TemplateName(templateName),
				BodyHTML:     validBodyPayload,
				HeaderHTML:   &validHeaderPayload,
				FooterHTML:   &validFooterPayload,
			},
			expectedParams: &validParams.PDF,
			expectedError:  nil,
		},
	}
}

func TestDefaultRenderer_RenderHTMLWithData(t *testing.T) {
	t.Parallel()

	var (
		logger = log.NewLogger(log.LevelFatal, log.TextFormat)
		ctx    = log.ContextWithLogger(context.TODO(), logger)

		templateName = templates.Name("testTemplate")
		templateData = templates.Data{
			"key": "value",
		}
	)

	for _, testCase := range defaultRendererTestCases(templateName, templateData, nil) {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			pdfRender := new(pdfRenderMocks.PDFRenderer)

			if testCase.renderHTMLResult != nil {
				pdfRender.EXPECT().
					RenderPDF(
						mock.Anything,
						testCase.expectedParams.BuildPDFRenderConfig(),
						*testCase.renderHTMLResult,
					).
					Return(testCase.expectedPayload, testCase.expectedError)

				defer pdfRender.AssertExpectations(t)
			}

			template := testCase.fetchTemplate
			template.PdfRender = pdfRender

			actualPayload, actualError := template.Render(
				ctx,
				templateData,
			)

			assert.Equal(t, testCase.expectedPayload, actualPayload)

			if testCase.expectedError == nil {
				assert.NoError(t, testCase.expectedError)
			} else {
				assert.ErrorIs(t, actualError, testCase.expectedError)
			}
		})
	}
}
