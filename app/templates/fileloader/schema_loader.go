package fileloader

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

const (
	schemaFileName = "schema.json"
)

// FileSchemaLoader load JSON Schema from file system.
type FileSchemaLoader struct{}

// NewFileSchemaLoader construct.
func NewFileSchemaLoader() *FileSchemaLoader {
	return new(FileSchemaLoader)
}

// LoadSchema from templateDir.
func (*FileSchemaLoader) LoadSchema(
	templateDirPath string,
	logger log.Logger,
) (templates.Validator, error) {
	loaderLogger := logger.WithField(log.FieldTemplateLoader, "schema")

	schemaPath := filepath.Join(templateDirPath, schemaFileName)

	schemaBody, err := os.ReadFile(schemaPath)
	if os.IsNotExist(err) {
		loaderLogger.Debug("Schema not found")

		return nil, nil
	} else if err != nil {
		return nil, fmt.Errorf("read schema file: %w", err)
	}

	jsonSchemaValidator, err := templates.NewJSONSchemaValidator(schemaBody)
	if err != nil {
		return nil, fmt.Errorf("new json schema validator: %w", err)
	}

	loaderLogger.Debug("Schema loaded")

	return jsonSchemaValidator, nil
}
