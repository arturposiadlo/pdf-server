package fileloader_test

import (
	"errors"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader/helpers"

	fileloaderMocks "gitlab.com/c0va23/pdf-server/mocks/templates/fileloader"
)

func TestFileTemplateInstanceLoader_LoadTemplateInstance_Default(t *testing.T) {
	t.Parallel()

	markdownPartialPreprocessor := fileloader.NewMarkdownPartialPreprocessor()
	orderedPartialsLoader := fileloader.OrderedPartialProcessors{markdownPartialPreprocessor}
	markdownHelpers := helpers.NewMarkdownProcessor(markdownPartialPreprocessor)

	partialsLoader := fileloader.NewDirPartialsLoader(orderedPartialsLoader)
	templateInstanceLoader := fileloader.DefaultFileTemplateInstanceLoader(
		partialsLoader,
		markdownHelpers,
	)

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	type TestCase struct {
		name            string
		templateDirPath string
		expectedError   error
	}

	testCases := []TestCase{
		{
			name:            "template not exists",
			templateDirPath: "not_found",
			expectedError:   fileloader.NewTemplateFileNotFoundError("all", "not_found"),
		},
		{
			name:            "go template dir path",
			templateDirPath: "fixtures/templates/go_template",
			expectedError:   nil,
		},
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			actualTemplateInstance, actualError := templateInstanceLoader.LoadTemplateInstance(
				testCase.templateDirPath,
				bodyPartName,
				logger,
			)

			assert.ErrorIs(t, actualError, testCase.expectedError)
			if testCase.expectedError != nil {
				assert.Nil(t, actualTemplateInstance)
			} else {
				assert.NotNil(t, actualTemplateInstance)
			}
		})
	}
}

func TestFileTemplateInstanceLoader_LoadTemplateInstance_CustomLoader(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	loader := new(fileloaderMocks.TemplateInstanceLoader)
	defer loader.AssertExpectations(t)

	templateLoadErr := errors.New("template load error")

	templateLogger := logger.
		WithField(log.FieldTemplateLoader, "instance").
		WithField(log.FieldTemplatePart, bodyPartName).
		WithField(log.FieldTemplateType, "example").
		WithField(log.FieldTemplatePath, "template.txt")
	loader.EXPECT().LoadTemplateInstance(
		"fixtures/go_template",
		bodyPartName,
		templateLogger,
	).Return(nil, templateLoadErr)
	loader.EXPECT().TemplateType().Return("example")
	loader.EXPECT().TemplateName(bodyPartName).Return("template.txt")

	templateDirPath := filepath.Join("fixtures", "go_template")
	templateInstanceLoader := fileloader.NewFileTemplateInstanceLoader(loader)

	resultTemplateInstance, resultErr := templateInstanceLoader.
		LoadTemplateInstance(templateDirPath, bodyPartName, logger)

	assert.ErrorIs(t, resultErr, templateLoadErr)

	assert.Equal(t, resultTemplateInstance, nil)
}
