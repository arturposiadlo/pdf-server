package fileloader_test

import (
	"io"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
)

func TestExamplesLoader(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)
	examplesLoader := fileloader.NewFileExamplesLoader()

	type TestCase struct {
		name             string
		templatePath     string
		expectedExamples templates.Examples
		expectedError    error
	}

	testCases := []TestCase{
		{
			name:         "template have valid examples",
			templatePath: "fixtures/templates/examples_template",
			expectedExamples: templates.Examples{
				"alice": templates.Data{
					"name": "Alice",
				},
				"bob": templates.Data{
					"name": "Bob",
				},
			},
			expectedError: nil,
		},
		{
			name:             "template have invalid examples",
			templatePath:     "fixtures/templates/invalid_example",
			expectedExamples: templates.Examples(nil),
			expectedError:    io.ErrUnexpectedEOF,
		},
	}

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			actualExamples, actualErr := examplesLoader.LoadExamples(
				testCase.templatePath,
				logger,
			)

			assert.Equal(t, testCase.expectedExamples, actualExamples)

			if testCase.expectedError == nil {
				assert.NoError(t, actualErr)
			} else {
				assert.ErrorIs(t, actualErr, testCase.expectedError)
			}
		})
	}
}
