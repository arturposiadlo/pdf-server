package fileloader

import (
	"context"
	"fmt"
	"io"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

// HTMLTemplate render template with data.
type HTMLTemplate interface {
	Render(data interface{}) (string, error)
}

// PDFParams definition.
type PDFParams struct {
	Landscape         *bool    `json:"landscape"`
	PrintBackground   *bool    `json:"printBackground"`
	PreferCSSPageSize *bool    `json:"preferCSSPageSize"`
	Scale             *float64 `json:"scale"`
	PaperWidth        *float64 `json:"paperWidth"`
	PaperHeight       *float64 `json:"paperHeight"`
	MarginTop         *float64 `json:"marginTop"`
	MarginBottom      *float64 `json:"marginBottom"`
	MarginLeft        *float64 `json:"marginLeft"`
	MarginRight       *float64 `json:"marginRight"`

	WaitLifecycleEvent *pdfrender.WaitLifecycleEvent `json:"waitLifecycleEvent"`
}

// BuildPDFRenderConfig build pdfrender.Config from PDFParams.
func (params *PDFParams) BuildPDFRenderConfig() *pdfrender.Config {
	if params == nil {
		return nil
	}

	return &pdfrender.Config{
		Landscape:          params.Landscape,
		PrintBackground:    params.PrintBackground,
		Scale:              params.Scale,
		PaperWidth:         params.PaperWidth,
		PaperHeight:        params.PaperHeight,
		MarginTop:          params.MarginTop,
		MarginBottom:       params.MarginBottom,
		MarginLeft:         params.MarginLeft,
		MarginRight:        params.MarginRight,
		PreferCSSPageSize:  params.PreferCSSPageSize,
		WaitLifecycleEvent: params.WaitLifecycleEvent,
	}
}

// Params definition.
type Params struct {
	PDF PDFParams `json:"pdf"`
}

// Assets interface.
type Assets interface {
	Open(string) (io.Reader, error)
}

// HTML2PDFTemplate wrap TemplateInstance with Schema.
type HTML2PDFTemplate struct {
	Name      templates.Name
	Body      HTMLTemplate
	Header    HTMLTemplate
	Footer    HTMLTemplate
	Params    *Params
	Validator templates.Validator
	Assets    Assets
	Examples  templates.Examples
	PdfRender pdfrender.PDFRenderer
}

// GetName return template name.
func (template *HTML2PDFTemplate) GetName() templates.Name {
	return template.Name
}

// GetExamples return template examples.
func (template *HTML2PDFTemplate) GetExamples() templates.Examples {
	return template.Examples
}

// GetValidator return template validator.
func (template *HTML2PDFTemplate) GetValidator() templates.Validator {
	return template.Validator
}

// Render template with data.
func (template *HTML2PDFTemplate) Render(ctx context.Context, data templates.Data) ([]byte, error) {
	document, pdfParams, err := template.renderHTML(ctx, data)
	if err != nil {
		return nil, fmt.Errorf("render: %w", err)
	}

	config := pdfParams.BuildPDFRenderConfig()

	payload, err := template.PdfRender.RenderPDF(ctx, config, *document)
	if err != nil {
		return nil, fmt.Errorf("render pdf: %w", err)
	}

	return payload, nil
}

// revive:disable:cyclomatic,cognitive-complexity,function-length // TODO: rewrite function
func (template *HTML2PDFTemplate) renderHTML(
	ctx context.Context,
	data templates.Data,
) (*pdfrender.Document, *PDFParams, error) {
	logger := log.LoggerFromContext(ctx, "render").WithField(log.FieldRenderData, data)

	logger.Debug("Render with data")

	bodyPayload, err := template.Body.Render(data)
	if nil != err {
		return nil, nil, fmt.Errorf("render body: %w", err)
	}

	var headerPayload *string

	if nil != template.Header {
		header, err := template.Header.Render(data)
		if nil != err {
			return nil, nil, fmt.Errorf("render header: %w", err)
		}

		headerPayload = &header
	}

	var footerPayload *string

	if nil != template.Footer {
		footer, err := template.Footer.Render(data)
		if nil != err {
			return nil, nil, fmt.Errorf("render footer: %w", err)
		}

		footerPayload = &footer
	}

	var pdfParams *PDFParams
	if template.Params != nil {
		pdfParams = &template.Params.PDF
	}

	return &pdfrender.Document{
		BodyHTML:     bodyPayload,
		HeaderHTML:   headerPayload,
		FooterHTML:   footerPayload,
		TemplateName: pdfrender.TemplateName(template.Name),
		Assets:       template.Assets,
	}, pdfParams, nil
}
