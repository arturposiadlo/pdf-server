package fileloader

import (
	"errors"
	"fmt"
	"os"

	"github.com/urfave/cli/v2"
	"golang.org/x/exp/maps"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

type templateReloadMode string

// Template Reload modes.
const (
	TemplateReloadModeNone   = templateReloadMode("none")
	TemplateReloadModeAlways = templateReloadMode("always")
)

func allTemplateReloadModes() []templateReloadMode {
	return []templateReloadMode{
		TemplateReloadModeNone,
		TemplateReloadModeAlways,
	}
}

// Set implement flag.Value interface.
func (reload *templateReloadMode) Set(value string) error {
	for _, allowedReloadMode := range allTemplateReloadModes() {
		if string(allowedReloadMode) == value {
			*reload = templateReloadMode(value)

			return nil
		}
	}

	return newUnknownTemplateReloadModeError(value)
}

// String implement Stringer interface.
func (reload *templateReloadMode) String() string {
	return string(*reload)
}

// UnknownTemplateReloadModeError returned when unknown template reload mode passed.
type UnknownTemplateReloadModeError struct {
	mode string
}

func newUnknownTemplateReloadModeError(mode string) *UnknownTemplateReloadModeError {
	return &UnknownTemplateReloadModeError{
		mode: mode,
	}
}

func (err *UnknownTemplateReloadModeError) Error() string {
	return fmt.Sprintf("unknown template reload mode %s", err.mode)
}

// Config for templates.
type Config struct {
	DirPath             string
	CompositionsDirPath string
	ReloadMode          templateReloadMode
}

// DefaultConfig return default config.
func DefaultConfig() Config {
	return Config{
		DirPath:             "templates/",
		CompositionsDirPath: "compositions/",
		ReloadMode:          TemplateReloadModeNone,
	}
}

// BindFlags return cli flag bindings.
func (config *Config) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "templates-dir",
			Aliases:     []string{"t"},
			EnvVars:     []string{"TEMPLATES_DIR"},
			Value:       config.DirPath,
			Destination: &config.DirPath,
			Hidden:      false,
			Usage:       "Templates directory path",
		},
		&cli.StringFlag{
			Name:        "compositions-dir",
			Aliases:     []string{},
			EnvVars:     []string{"COMPOSITIONS_DIR"},
			Value:       config.CompositionsDirPath,
			Destination: &config.CompositionsDirPath,
			Hidden:      false,
			Usage:       "Compositions directory path",
		},
		&cli.GenericFlag{
			Name:    "templates-reload-mode",
			Aliases: []string{},
			EnvVars: []string{"TEMPLATES_RELOAD_MODE"},
			Value:   &config.ReloadMode,
			Hidden:  false,
			Usage: fmt.Sprintf(
				"Mode of templates reloading. Allowed values: %s",
				allTemplateReloadModes(),
			),
		},
	}
}

// OrderedTemplateLoaders is a list of template loaders.
type OrderedTemplateLoaders []templates.TemplateLoader

// DefaultOrderedTemplateLoaders return default template loaders.
func DefaultOrderedTemplateLoaders(
	fileTemplateLoader *FileTemplateLoader,
	rawTemplateLoader *RawTemplateLoader,
) OrderedTemplateLoaders {
	return OrderedTemplateLoaders{
		fileTemplateLoader,
		rawTemplateLoader,
	}
}

// OrderedTemplateLoaderBuilders is a list of template loader builders.
type OrderedTemplateLoaderBuilders []templates.TemplateLoaderBuilder

// DefaultOrderedTemplateLoaderBuilders return default template loader builders.
func DefaultOrderedTemplateLoaderBuilders(
	compositionTemplateLoaderBuilder *compositionLoaderBuilder,
) OrderedTemplateLoaderBuilders {
	return OrderedTemplateLoaderBuilders{
		compositionTemplateLoaderBuilder,
	}
}

// LoadTemplatesMap load templates from directory.
func LoadTemplatesMap(
	config Config,
	orderedTemplateLoaders OrderedTemplateLoaders,
	orderedTemplateLoaderBuilders OrderedTemplateLoaderBuilders,
	logger log.Logger,
) (templates.Store, error) {
	templatesLogger := logger.WithField(log.LoggerField, "templates")

	templateStores, err := loadTemplates(
		config.ReloadMode,
		config.DirPath,
		orderedTemplateLoaders,
		templatesLogger,
	)
	if err != nil {
		return nil, fmt.Errorf("load templates: %w", err)
	}

	dependentTemplateLoaders := make(OrderedTemplateLoaders, 0, len(orderedTemplateLoaderBuilders))

	for _, templateLoaderBuilder := range orderedTemplateLoaderBuilders {
		//nolint:govet // false positive
		templateLoader, err := templateLoaderBuilder.BuildTemplateLoader(templateStores)
		if err != nil {
			return nil, fmt.Errorf("build composition loader: %w", err)
		}

		dependentTemplateLoaders = append(dependentTemplateLoaders, templateLoader)
	}

	dependentTemplateStores, err := loadTemplates(
		config.ReloadMode,
		config.CompositionsDirPath,
		dependentTemplateLoaders,
		templatesLogger,
	)
	if err != nil {
		return nil, fmt.Errorf("load compositions: %w", err)
	}

	return fallbackStore{
		templateStores,
		dependentTemplateStores,
	}, nil
}

func loadTemplates(
	reloadMode templateReloadMode,
	dirPath string,
	orderedTemplateLoaders OrderedTemplateLoaders,
	templatesLogger log.Logger,
) (templates.Store, error) {
	stores := make(map[templates.TemplateLoader]templates.Store, len(orderedTemplateLoaders))

	for _, templateLoader := range orderedTemplateLoaders {
		store, err := buildStore(reloadMode, dirPath, templateLoader, templatesLogger)
		if err != nil {
			return nil, fmt.Errorf("build store: %w", err)
		}

		stores[templateLoader] = store
	}

	templateDirs, err := os.ReadDir(dirPath)
	if nil != err {
		return nil, fmt.Errorf("read templates dir: %w", err)
	}

	for _, templateDir := range templateDirs {
		templateName := templateDir.Name()

		err := loadTemplate(dirPath, templateName, orderedTemplateLoaders, stores, templatesLogger)
		if err != nil {
			return nil, fmt.Errorf("load template name %s: %w", templateName, err)
		}
	}

	return fallbackStore(maps.Values(stores)), nil
}

// revive:disable:cognitive-complexity
func loadTemplate(
	dirPath string,
	templateName string,
	orderedTemplateLoaders OrderedTemplateLoaders,
	stores map[templates.TemplateLoader]templates.Store,
	templatesLogger log.Logger,
) error {
	var (
		loadErrors    []error
		templateFound = false
	)

	for _, templateLoader := range orderedTemplateLoaders {
		template, err := templateLoader.LoadTemplate(dirPath, templateName, templatesLogger)
		if nil != err {
			templatesLogger.
				WithError(err).
				WithField("templateName", templateName).
				WithField("loader type", templateLoader.TemplateType()).
				Error("load template")

			if notFound := new(templates.TemplateNotExistsError); errors.As(err, &notFound) {
				loadErrors = append(loadErrors, err)

				continue
			}

			// HTML template not found
			if notFound := new(TemplateFileNotFoundError); errors.As(err, &notFound) {
				loadErrors = append(loadErrors, err)

				continue
			}

			return fmt.Errorf("load template type %s: %w", templateLoader.TemplateType(), err)
		}

		store := stores[templateLoader]

		if err := store.Put(template); err != nil {
			return fmt.Errorf("put template: %w", err)
		}

		templateFound = true
	}

	if !templateFound {
		return fmt.Errorf("template not loaded: %w", errors.Join(loadErrors...))
	}

	return nil
}

func buildStore(
	reloadMode templateReloadMode,
	templatesDirPath string,
	templateLoader templates.TemplateLoader,
	templatesLogger log.Logger,
) (templates.Store, error) {
	templatesMap := make(map[templates.Name]templates.Template)

	mapStore := templates.MapStore(templatesMap)

	switch reloadMode {
	case TemplateReloadModeNone:
		return mapStore, nil
	case TemplateReloadModeAlways:
		return NewDevStore(
			templatesDirPath,
			templatesLogger,
			templateLoader,
			mapStore,
		), nil
	default:
		return nil, newUnknownTemplateReloadModeError(reloadMode.String())
	}
}

// DevStore is store for development mode. Reload template on every fetch.
type DevStore struct {
	templateDirPath string
	logger          log.Logger
	templateLoader  templates.TemplateLoader
	originStore     templates.Store
}

// NewDevStore construct new DevStore.
func NewDevStore(
	templateDirPath string,
	logger log.Logger,
	templateLoader templates.TemplateLoader,
	originStore templates.Store,
) *DevStore {
	return &DevStore{
		templateDirPath: templateDirPath,
		logger:          logger,
		templateLoader:  templateLoader,
		originStore:     originStore,
	}
}

// List all templates.
func (store *DevStore) List() ([]templates.Template, error) {
	templateList, err := store.originStore.List()
	if err != nil {
		return nil, fmt.Errorf("list templates: %w", err)
	}

	return templateList, nil
}

// Fetch template by name. Check original store first, then reload template.
func (store *DevStore) Fetch(templateName templates.Name) (templates.Template, error) {
	_, err := store.originStore.Fetch(templateName)
	if err != nil {
		return nil, fmt.Errorf("fetch template: %w", err)
	}

	template, err := store.templateLoader.LoadTemplate(
		store.templateDirPath,
		string(templateName),
		store.logger,
	)
	if err != nil {
		return nil, fmt.Errorf("load template: %w", err)
	}

	return template, nil
}

// Put template to store.
func (store *DevStore) Put(template templates.Template) error {
	if err := store.originStore.Put(template); err != nil {
		return fmt.Errorf("put template: %w", err)
	}

	return nil
}

type fallbackStore []templates.Store

// List all templates.
func (templateStores fallbackStore) List() ([]templates.Template, error) {
	allTemplatesList := []templates.Template{}

	for _, templateStore := range templateStores {
		templateList, err := templateStore.List()
		if err != nil {
			return nil, fmt.Errorf("list templates: %w", err)
		}

		allTemplatesList = append(allTemplatesList, templateList...)
	}

	return allTemplatesList, nil
}

// Fetch template by name.
func (templateStores fallbackStore) Fetch(templateName templates.Name) (templates.Template, error) {
	var errorList []error

	for _, templateStore := range templateStores {
		template, err := templateStore.Fetch(templateName)
		if err != nil {
			errorList = append(errorList, err)

			continue
		}

		return template, nil
	}

	return nil, fmt.Errorf("fetch template: %w", errors.Join(errorList...))
}

// Put template to store.
func (fallbackStore) Put(_ templates.Template) error {
	panic("not implemented")
}
