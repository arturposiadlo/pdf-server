package fileloader_test

import (
	"testing"

	"github.com/cbroglie/mustache"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
)

func TestMustacheTemplateInstanceLoader_LoadTemplateInstance(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	markdownPartialPreprocessor := fileloader.NewMarkdownPartialPreprocessor()
	orderedPartialsLoader := fileloader.OrderedPartialProcessors{markdownPartialPreprocessor}
	templateLoader := fileloader.NewMustacheTemplateInstanceLoader(
		fileloader.NewDirPartialsLoader(orderedPartialsLoader),
	)

	testCases := []LoadTemplateInstanceTestCase{
		{
			name:            "template found",
			templateDirPath: "fixtures/templates/mustache_template",
			expectedError:   nil,
		},
		{
			name:            "template not found",
			templateDirPath: "fixtures/templates/not_found",
			expectedError: fileloader.NewTemplateFileNotFoundError(
				templateLoader.TemplateType(),
				"fixtures/templates/not_found/template.mustache",
			),
		},
	}

	loader := func(templatePath string) (fileloader.HTMLTemplate, error) {
		return templateLoader.LoadTemplateInstance(templatePath, bodyPartName, logger)
	}

	testLoadTemplateInstance(t, loader, testCases)
}

func TestMustacheTemplate_Render(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)
	template, _ := mustache.ParseFile("fixtures/templates/mustache_template/template.mustache")
	mustacheTemplate := fileloader.NewMustacheTemplate(template, logger)

	testCases := []RenderTemplateTestCase{
		{
			name: "with valid data",
			templateData: map[string]int{
				"value": 123,
			},
			expectedPayload: "<html><body><h1>123</h1></body></html>\n",
			expectedError:   "",
		},
		{
			name:            "with invalid data",
			templateData:    struct{}{},
			expectedPayload: "",
			expectedError:   `missing variable "value"`,
		},
	}

	testRenderTemplateTestCases(t, mustacheTemplate, testCases)
}
