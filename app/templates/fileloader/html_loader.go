package fileloader

import "gitlab.com/c0va23/pdf-server/app/log"

// HTMLTemplateLoader interface.
type HTMLTemplateLoader interface {
	LoadTemplateInstance(
		templateDirPath string,
		partName string,
		logger log.Logger,
	) (HTMLTemplate, error)
}

// ParamsLoader interface.
type ParamsLoader interface {
	LoadParams(
		templateDirPath string,
		logger log.Logger,
	) (*Params, error)
}

// AssetsLoader interface.
type AssetsLoader interface {
	LoadAssets(
		templateDirPath string,
		logger log.Logger,
	) Assets
}
