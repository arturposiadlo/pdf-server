package fileloader_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
)

func TestFileParamsLoader_LoadParams(t *testing.T) {
	t.Parallel()

	paramsLoader := new(fileloader.FileParamsLoader)
	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	var nilParams *fileloader.Params

	type TestCase struct {
		name string

		templateDirPath string

		expectedParams *fileloader.Params
		expectedError  error
	}

	testCases := []TestCase{
		{
			name:            "params.json not exists",
			templateDirPath: staticTemplatePath,
			expectedParams:  nilParams,
			expectedError:   nil,
		},
		{
			name:            "params.json exists",
			templateDirPath: "fixtures/templates/params_template",
			expectedParams: func() *fileloader.Params {
				landscape := true
				printBackground := true
				scale := 2.0
				paperWidth := 20.0
				paperHeight := 20.0
				marginTop := 2.0
				marginBottom := 2.0
				marginLeft := 2.0
				marginRight := 2.0
				preferCSSPageSize := true

				return &fileloader.Params{
					PDF: fileloader.PDFParams{
						Landscape:         &landscape,
						PrintBackground:   &printBackground,
						Scale:             &scale,
						PaperWidth:        &paperHeight,
						PaperHeight:       &paperWidth,
						MarginTop:         &marginTop,
						MarginBottom:      &marginBottom,
						MarginLeft:        &marginLeft,
						MarginRight:       &marginRight,
						PreferCSSPageSize: &preferCSSPageSize,
					},
				}
			}(),
			expectedError: nil,
		},
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			actualParams, actualErr := paramsLoader.LoadParams(testCase.templateDirPath, logger)

			assert.Equal(t, testCase.expectedParams, actualParams)
			assert.Equal(t, testCase.expectedError, actualErr)
		})
	}
}
