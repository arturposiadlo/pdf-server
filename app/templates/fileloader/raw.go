package fileloader

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

const rawTemplateFileName = "raw.pdf"

// RawTemplateLoader loads raw templates.
type RawTemplateLoader struct{}

// NewRawTemplateLoader constructor.
func NewRawTemplateLoader() *RawTemplateLoader {
	return &RawTemplateLoader{}
}

// LoadTemplate loads template.
func (*RawTemplateLoader) LoadTemplate(
	templatesDirPath string,
	templateName string,
	logger log.Logger,
) (templates.Template, error) {
	templateDirPath := filepath.Join(templatesDirPath, templateName)

	templateLogger := logger.
		WithField(log.FieldTemplateDir, templateDirPath).
		WithField(log.FieldTemplateName, templateName)

	templatePath := filepath.Join(templateDirPath, rawTemplateFileName)

	name := templates.Name(templateName)

	payload, err := os.ReadFile(templatePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, templates.NewTemplateNotExistsError(name)
		}

		return nil, fmt.Errorf("read template file: %w", err)
	}

	templateLogger.Infof("Raw template loaded")

	return &RawTemplate{
		name:    name,
		payload: payload,
	}, nil
}

// TemplateType returns template type.
func (*RawTemplateLoader) TemplateType() string {
	return "raw"
}

// RawTemplate render raw template.
type RawTemplate struct {
	name    templates.Name
	payload []byte
}

// GetName returns template name.
func (template *RawTemplate) GetName() templates.Name {
	return template.name
}

// GetExamples returns template examples.
func (*RawTemplate) GetExamples() templates.Examples {
	return nil
}

// GetValidator returns template validator.
func (*RawTemplate) GetValidator() templates.Validator {
	return nil
}

// Render renders template.
func (template *RawTemplate) Render(_ context.Context, _ templates.Data) ([]byte, error) {
	return template.payload, nil
}
