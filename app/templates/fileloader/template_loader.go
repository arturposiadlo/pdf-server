package fileloader

import (
	"errors"
	"fmt"
	"path/filepath"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

const (
	bodyPartName   = "template"
	headerPartName = "header"
	footerPartName = "footer"
)

// FileTemplateLoader load template from directory on file system.
type FileTemplateLoader struct {
	htmlTemplateLoader HTMLTemplateLoader
	templates.SchemaLoader
	paramsLoader ParamsLoader
	assetsLoader AssetsLoader
	templates.ExamplesLoader
	pdfRender pdfrender.PDFRenderer
}

// NewFileTemplateLoader constructor.
//
// revive:disable:argument-limit
func NewFileTemplateLoader(
	templateInstanceLoader HTMLTemplateLoader,
	schemaLoader templates.SchemaLoader,
	paramsLoader ParamsLoader,
	assetsLoader AssetsLoader,
	examplesLoader templates.ExamplesLoader,
	pdfRender pdfrender.PDFRenderer,
) *FileTemplateLoader {
	return &FileTemplateLoader{
		htmlTemplateLoader: templateInstanceLoader,
		SchemaLoader:       schemaLoader,
		paramsLoader:       paramsLoader,
		assetsLoader:       assetsLoader,
		ExamplesLoader:     examplesLoader,
		pdfRender:          pdfRender,
	}
}

// LoadTemplate load template dir.
//
//nolint:funlen,revive // TODO: rewrite function
func (templateLoader *FileTemplateLoader) LoadTemplate(
	templatesDirPath string,
	templateName string,
	logger log.Logger,
) (templates.Template, error) {
	templateDirPath := filepath.Join(templatesDirPath, templateName)
	templateLogger := logger.
		WithField(log.FieldTemplateDir, templateDirPath).
		WithField(log.FieldTemplateName, templateName)

	templateNotFound := new(TemplateFileNotFoundError)

	body, err := templateLoader.htmlTemplateLoader.LoadTemplateInstance(
		templateDirPath,
		bodyPartName,
		templateLogger,
	)
	if err != nil {
		return nil, fmt.Errorf("load %s template: %w", bodyPartName, err)
	}

	header, err := templateLoader.htmlTemplateLoader.LoadTemplateInstance(
		templateDirPath,
		headerPartName,
		templateLogger,
	)
	if err != nil && !errors.As(err, &templateNotFound) {
		return nil, fmt.Errorf("load %s template: %w", headerPartName, err)
	}

	footer, err := templateLoader.htmlTemplateLoader.LoadTemplateInstance(
		templateDirPath,
		footerPartName,
		templateLogger,
	)
	if err != nil && !errors.As(err, &templateNotFound) {
		return nil, fmt.Errorf("load %s templates: %w", footerPartName, err)
	}

	schemaValidator, err := templateLoader.LoadSchema(templateDirPath, templateLogger)
	if nil != err {
		return nil, fmt.Errorf("load schema: %w", err)
	}

	params, err := templateLoader.paramsLoader.LoadParams(templateDirPath, templateLogger)
	if nil != err {
		return nil, fmt.Errorf("load params: %w", err)
	}

	assets := templateLoader.assetsLoader.LoadAssets(templateDirPath, templateLogger)

	examples, err := templateLoader.LoadExamples(templateDirPath, templateLogger)
	if nil != err {
		return nil, fmt.Errorf("load examples: %w", err)
	}

	templateLogger.Info("Template loaded")

	return &HTML2PDFTemplate{
		PdfRender: templateLoader.pdfRender,
		Name:      templates.Name(templateName),
		Body:      body,
		Header:    header,
		Footer:    footer,
		Validator: schemaValidator,
		Params:    params,
		Assets:    assets,
		Examples:  examples,
	}, nil
}

// TemplateType return template type.
func (*FileTemplateLoader) TemplateType() string {
	return "html2pdf"
}
