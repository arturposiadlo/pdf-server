package fileloader

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/c0va23/pdf-server/app/log"
)

// FileAssets type.
type FileAssets struct {
	fileSystem http.FileSystem
}

// NewFileAssets constructor.
func NewFileAssets(fileSystem http.FileSystem) *FileAssets {
	return &FileAssets{
		fileSystem: fileSystem,
	}
}

// Open file by path.
func (assets *FileAssets) Open(path string) (io.Reader, error) {
	reader, err := assets.fileSystem.Open(path)
	if err != nil {
		return nil, fmt.Errorf("open filesystem: %w", err)
	}

	return reader, nil
}

// FileAssetsLoader type.
type FileAssetsLoader struct{}

// NewFileAssetsLoader constructor.
func NewFileAssetsLoader() *FileAssetsLoader {
	return new(FileAssetsLoader)
}

const assetsDirName = "assets"

// LoadAssets FileSystem from assets dir.
func (*FileAssetsLoader) LoadAssets(
	templateDirPath string,
	logger log.Logger,
) Assets {
	logger = logger.WithField(log.FieldTemplateLoader, "assets")

	assetsDirPath := filepath.Join(templateDirPath, assetsDirName)
	assetsDirInfo, err := os.Stat(assetsDirPath)

	if err == nil && assetsDirInfo.IsDir() {
		logger.Info("Assets loaded")

		return NewFileAssets(
			http.Dir(assetsDirPath),
		)
	}

	if os.IsNotExist(err) {
		logger.Debug("Assets not exists")
	} else {
		logger.WithError(err).Warn("Assets load error")
	}

	return nil
}
