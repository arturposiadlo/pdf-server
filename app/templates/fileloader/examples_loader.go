package fileloader

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

const (
	examplesGlob = "examples/*.json"
)

// FileExamplesLoader type.
type FileExamplesLoader struct{}

// NewFileExamplesLoader constructor.
func NewFileExamplesLoader() *FileExamplesLoader {
	return new(FileExamplesLoader)
}

// LoadExamples by path.
func (*FileExamplesLoader) LoadExamples(
	templateDirPath string,
	logger log.Logger,
) (templates.Examples, error) {
	logger = logger.WithField(log.FieldTemplateLoader, "examples")

	examplePaths, err := filepath.Glob(filepath.Join(templateDirPath, examplesGlob))
	if nil != err {
		return nil, fmt.Errorf("examples glob: %w", err)
	}

	examples := make(templates.Examples, len(examplePaths))
	exampleExtLen := len(filepath.Ext(examplesGlob))

	for _, examplePath := range examplePaths {
		exampleBase := filepath.Base(examplePath)
		exampleName := exampleBase[:len(exampleBase)-exampleExtLen]

		logger = logger.WithField(log.FieldTemplateExample, exampleName)

		exampleFile, err := os.Open(examplePath) // #nosec
		if nil != err {
			return nil, fmt.Errorf("open example: %w", err)
		}

		var example templates.Data
		err = json.NewDecoder(exampleFile).Decode(&example)

		if nil != err {
			return nil, fmt.Errorf("decode example: %w", err)
		}

		logger.Debug("Example loaded")

		examples[exampleName] = example
	}

	return examples, nil
}
