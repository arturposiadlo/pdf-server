package fileloader_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader/helpers"
)

func TestGoTemplateInstanceLoader_LoadTemplateInstance(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	markdownPartialPreprocessor := fileloader.NewMarkdownPartialPreprocessor()
	orderedPartialsLoader := fileloader.OrderedPartialProcessors{markdownPartialPreprocessor}
	templateLoader := fileloader.NewGoTemplateInstanceLoader(
		fileloader.NewDirPartialsLoader(orderedPartialsLoader),
		helpers.NewMarkdownProcessor(markdownPartialPreprocessor),
	)

	testCases := []LoadTemplateInstanceTestCase{
		{
			name:            "template found",
			templateDirPath: "fixtures/templates/go_template",
			expectedError:   nil,
		},
		{
			name:            "template not found",
			templateDirPath: "fixtures/templates/not_found",
			expectedError: fileloader.NewTemplateFileNotFoundError(
				templateLoader.TemplateType(),
				"fixtures/templates/not_found/template.tmpl",
			),
		},
	}

	loader := func(templatePath string) (fileloader.HTMLTemplate, error) {
		return templateLoader.LoadTemplateInstance(templatePath, bodyPartName, logger)
	}

	testLoadTemplateInstance(t, loader, testCases)
}

func TestGoTemplate_Render(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	markdownPartialPreprocessor := fileloader.NewMarkdownPartialPreprocessor()
	orderedPartialsLoader := fileloader.OrderedPartialProcessors{markdownPartialPreprocessor}
	templateLoader := fileloader.NewGoTemplateInstanceLoader(
		fileloader.NewDirPartialsLoader(orderedPartialsLoader),
		helpers.NewMarkdownProcessor(markdownPartialPreprocessor),
	)
	goTemplate, err := templateLoader.LoadTemplateInstance(
		"fixtures/templates/go_template", bodyPartName, logger)
	assert.NoError(t, err)

	testCases := []RenderTemplateTestCase{
		{
			name: "with valid data",
			templateData: map[string]int{
				"value": 123,
			},
			expectedPayload: "<html><body><h1>123</h1></body></html>\n",
			expectedError:   "",
		},
		{
			name:            "with invalid data",
			templateData:    struct{}{},
			expectedPayload: "",
			expectedError: "template: template.tmpl:1:19: executing \"template.tmpl\" " +
				"at <.value>: can't evaluate field value in type struct {}",
		},
	}

	testRenderTemplateTestCases(t, goTemplate, testCases)
}
