package fileloader

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"path"

	"github.com/cbroglie/mustache"

	"gitlab.com/c0va23/pdf-server/app/log"
)

const (
	mustacheTemplateExtension = ".mustache"
)

//nolint:gochecknoinits
func init() {
	mustache.AllowMissingVariables = false
}

// MustacheTemplate engine.
type MustacheTemplate struct {
	template *mustache.Template
	logger   log.Logger
}

// NewMustacheTemplate constructor.
func NewMustacheTemplate(
	template *mustache.Template,
	logger log.Logger,
) *MustacheTemplate {
	return &MustacheTemplate{
		template: template,
		logger:   logger,
	}
}

// Render mustache template.
func (mt *MustacheTemplate) Render(data interface{}) (string, error) {
	buf := new(bytes.Buffer)
	err := mt.template.FRender(buf, data)

	if nil != err {
		return "", fmt.Errorf("render mustache template: %w", err)
	}

	return buf.String(), nil
}

// MustacheTemplateInstanceLoader load mustache template.
type MustacheTemplateInstanceLoader struct {
	partialsLoader PartialsLoader
}

// NewMustacheTemplateInstanceLoader constructor.
func NewMustacheTemplateInstanceLoader(
	partialsLoader PartialsLoader,
) *MustacheTemplateInstanceLoader {
	return &MustacheTemplateInstanceLoader{
		partialsLoader: partialsLoader,
	}
}

// TemplateType return mustache template type.
func (*MustacheTemplateInstanceLoader) TemplateType() string {
	return "mustache"
}

// TemplateName return mustache template name.
func (*MustacheTemplateInstanceLoader) TemplateName(partName string) string {
	return partName + mustacheTemplateExtension
}

// LoadTemplateInstance mustache partName template from templateDir.
func (loader *MustacheTemplateInstanceLoader) LoadTemplateInstance(
	templateDir string,
	partName string,
	logger log.Logger,
) (HTMLTemplate, error) {
	// TODO: pass logger via context
	ctx := log.ContextWithLogger(context.Background(), logger)
	templatePath := path.Join(templateDir, loader.TemplateName(partName))

	partials, err := loader.partialsLoader.LoadPartials(ctx, templateDir, mustacheTemplateExtension)
	if err != nil {
		return nil, fmt.Errorf("load partials: %w", err)
	}

	partialsProvider := loader.buildPartialsProvider(partials)

	template, err := mustache.ParseFilePartials(templatePath, partialsProvider)
	if nil != err {
		if os.IsNotExist(err) {
			return nil, NewTemplateFileNotFoundError(loader.TemplateType(), templatePath)
		}

		return nil, fmt.Errorf("parse mustache template: %w", err)
	}

	return NewMustacheTemplate(template, logger), nil
}

func (*MustacheTemplateInstanceLoader) buildPartialsProvider(
	rawPartials map[string][]byte,
) *mustache.StaticProvider {
	partials := make(map[string]string, len(rawPartials))

	for name, rawPartial := range rawPartials {
		partials[name] = string(rawPartial)
	}

	return &mustache.StaticProvider{
		Partials: partials,
	}
}
