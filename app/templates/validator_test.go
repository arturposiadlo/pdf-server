package templates_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

func TestJSONSchemaValidator_ValidateData(t *testing.T) {
	t.Parallel()

	var (
		logger = log.NewLogger(log.LevelFatal, log.TextFormat)
		ctx    = log.ContextWithLogger(context.TODO(), logger)
	)

	rawJSONSchema := []byte(`{
		"type": "object",
		"properties": {
			"value": {
				"type": "string"
			}
		},
		"required": [
			"value"
		]
	}`)

	jsonSchemaValidator, err := templates.NewJSONSchemaValidator(rawJSONSchema)

	assert.NoError(t, err)

	type TestCase struct {
		name          string
		data          map[string]any
		expectedError error
	}

	testCases := []TestCase{
		{
			name: "valid schema",
			data: map[string]interface{}{
				"value": "example",
			},
			expectedError: nil,
		},
		{
			name: "invalid data",
			data: map[string]interface{}{
				"invalid_key": 123,
			},
			expectedError: new(templates.InvalidDataSchemaError),
		},
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			actualError := jsonSchemaValidator.ValidateData(ctx, templates.Data(testCase.data))

			if testCase.expectedError == nil {
				assert.NoError(t, actualError)
			} else {
				assert.ErrorIs(t, testCase.expectedError, actualError)
			}
		})
	}
}

func TestInvalidSchema(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name   string
		schema string
	}{
		{
			name: "invalid array item declaration",
			schema: `{
				"type": "object",
				"lines": {
					"type": "array",
					"items": [
						{
							"type": "string",
						}
					]
				}
			}`,
		},
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			_, err := templates.NewJSONSchemaValidator([]byte(testCase.schema))

			assert.Error(t, err)
		})
	}
}
