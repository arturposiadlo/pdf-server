package templates

import (
	"gitlab.com/c0va23/pdf-server/app/log"
)

// TemplateLoader interface.
type TemplateLoader interface {
	LoadTemplate(
		templateDirPath string,
		templateName string,
		logger log.Logger,
	) (Template, error)

	TemplateType() string
}

// TemplateLoaderBuilder interface.
type TemplateLoaderBuilder interface {
	BuildTemplateLoader(Store) (TemplateLoader, error)
}

// SchemaLoader interface.
type SchemaLoader interface {
	LoadSchema(
		templateDirPath string,
		logger log.Logger,
	) (Validator, error)
}

// ExamplesLoader interface.
type ExamplesLoader interface {
	LoadExamples(
		templateDirPath string,
		logger log.Logger,
	) (Examples, error)
}
