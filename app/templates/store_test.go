package templates_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"

	fileloaderMocks "gitlab.com/c0va23/pdf-server/mocks/templates/fileloader"
)

func TestMapStore_Fetch(t *testing.T) {
	t.Parallel()

	validTemplateName := "example"

	templateMock := &fileloader.HTML2PDFTemplate{
		Body: new(fileloaderMocks.HTMLTemplate),
	}

	mapStore := templates.MapStore(map[templates.Name]templates.Template{
		templates.Name(validTemplateName): templateMock,
	})

	t.Run("exists template", func(t *testing.T) {
		t.Parallel()

		actualTemplate, actualErr := mapStore.Fetch(templates.Name(validTemplateName))

		assert.Equal(t, templateMock, actualTemplate)
		assert.NoError(t, actualErr)
	})

	t.Run("not exists template", func(t *testing.T) {
		t.Parallel()

		invalidTemplateName := templates.Name("invalid")

		actualTemplate, actualErr := mapStore.Fetch(invalidTemplateName)

		assert.Nil(t, actualTemplate)
		assert.ErrorIs(t, actualErr, templates.NewTemplateNotExistsError(invalidTemplateName))
	})
}

func TestMapStore_List(t *testing.T) {
	t.Parallel()

	t.Run("exists template", func(t *testing.T) {
		t.Parallel()

		firstTemplateMock := &fileloader.HTML2PDFTemplate{
			Name: "1_first",
			Body: new(fileloaderMocks.HTMLTemplate),
		}

		secondTemplateMock := &fileloader.HTML2PDFTemplate{
			Name:   "2_second",
			Body:   new(fileloaderMocks.HTMLTemplate),
			Header: new(fileloaderMocks.HTMLTemplate),
		}

		mapStore := templates.MapStore(map[templates.Name]templates.Template{
			templates.Name("1_first"):  firstTemplateMock,
			templates.Name("2_second"): secondTemplateMock,
		})

		actualTemplateList, actualErr := mapStore.List()

		expectedTemplateList := []templates.Template{
			firstTemplateMock,
			secondTemplateMock,
		}

		assert.Equal(t, expectedTemplateList, actualTemplateList)
		assert.NoError(t, actualErr)
	})
}
