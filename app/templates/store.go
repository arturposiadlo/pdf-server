package templates

import (
	"errors"
	"fmt"
	"sort"
)

// TemplateNotExistsError returned from Store.Fetch.
type TemplateNotExistsError struct {
	templateName Name
}

// NewTemplateNotExistsError construct new TemplateNotExistsError.
func NewTemplateNotExistsError(templateName Name) *TemplateNotExistsError {
	return &TemplateNotExistsError{
		templateName: templateName,
	}
}

func (err *TemplateNotExistsError) Error() string {
	return fmt.Sprintf("template not found: %s", err.templateName)
}

// Is implement errors.Is.
func (err *TemplateNotExistsError) Is(otherErr error) bool {
	castedError := new(TemplateNotExistsError)
	if !errors.As(otherErr, &castedError) {
		return false
	}

	return err.templateName == castedError.templateName
}

// Store of templates.
type Store interface {
	Fetch(Name) (Template, error)
	List() ([]Template, error)
	Put(Template) error
}

// MapStore is simple templates store.
type MapStore map[Name]Template

// Fetch template by name.
func (store MapStore) Fetch(templateName Name) (Template, error) {
	template, exists := store[templateName]
	if !exists {
		return nil, NewTemplateNotExistsError(templateName)
	}

	return template, nil
}

// List all templates
//
//nolint:unparam
func (store MapStore) List() ([]Template, error) {
	templates := make([]Template, 0, len(store))
	for _, template := range store {
		templates = append(templates, template)
	}

	sort.Sort(templatesList(templates))

	return templates, nil
}

// Put template to store.
//
//nolint:unparam
func (store MapStore) Put(template Template) error {
	store[template.GetName()] = template

	return nil
}

// templatesList wrap templates slice and implement sort.Sort interface.
type templatesList []Template

func (list templatesList) Len() int {
	return len(list)
}

func (list templatesList) Less(i, j int) bool {
	return list[i].GetName() < list[j].GetName()
}

func (list templatesList) Swap(i, j int) {
	list[i], list[j] = list[j], list[i]
}
