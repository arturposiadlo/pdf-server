package server

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"syscall"
	"time"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
)

// Router is routed handler.
type Router interface {
	http.Handler
}

const (
	defaultStopTimeout   = 10 * time.Second
	defaultListenAddress = ":9999"
)

// HTTPServerConfig for NewHTTPServer.
type HTTPServerConfig struct {
	Addr        string
	StopTimeout time.Duration
	ReuseAddr   bool
	Static      StaticDirConfig
}

// NewHTTPServerConfig return config with default values.
func NewHTTPServerConfig() HTTPServerConfig {
	return HTTPServerConfig{
		Addr:        defaultListenAddress,
		StopTimeout: defaultStopTimeout,
		ReuseAddr:   false,
		Static:      DefaultStaticDirConfig(),
	}
}

// HTTPServer wrapper on http.Server.
type HTTPServer struct {
	server http.Server
	logger log.Logger
	ctx    context.Context
	router Router
	config HTTPServerConfig
}

// ProvideHTTPServer create and start HTTP server.
func ProvideHTTPServer(
	ctx context.Context,
	router Router,
	config HTTPServerConfig,
	tracerProvider trace.TracerProvider,
	propagator propagation.TextMapPropagator,
) (*HTTPServer, func(), error) {
	logger := log.LoggerFromContext(ctx, "http-server")

	listener, err := buildListener(ctx, config)
	if err != nil {
		return nil, nil, fmt.Errorf("build listener: %w", err)
	}

	logger.WithField(log.FieldListenAddr, listener.Addr()).Info("Start HTTP server")

	handler := otelhttp.NewHandler(
		router,
		"http-request",
		otelhttp.WithTracerProvider(tracerProvider),
		otelhttp.WithPropagators(propagator),
	)

	// TODO: configure ErrorLog
	httpServer := &HTTPServer{
		server: http.Server{
			Handler: handler,
		},
		logger: logger,
		ctx:    ctx,
		router: router,
		config: config,
	}

	go httpServer.serve(listener)

	logger.Infof("Debug tools (Tester UI, Swagger UI and etc.): http://%s/", listener.Addr())

	closer := func() {
		httpServer.Close(ctx)
	}

	return httpServer, closer, nil
}

func buildListener(
	ctx context.Context,
	config HTTPServerConfig,
) (net.Listener, error) {
	listenConfig := net.ListenConfig{
		Control: func(network, address string, conn syscall.RawConn) error {
			err := conn.Control(func(fd uintptr) {
				configureCOnnection(ctx, config, fd)
			})
			if err != nil {
				return fmt.Errorf("control: %w", err)
			}

			return nil
		},
	}

	listener, err := listenConfig.Listen(ctx, "tcp", config.Addr)
	if err != nil {
		return nil, fmt.Errorf("listen: %w", err)
	}

	return listener, nil
}

func configureCOnnection(ctx context.Context, config HTTPServerConfig, fd uintptr) {
	logger := log.LoggerFromContext(ctx, "build-listener")

	if !config.ReuseAddr {
		return
	}

	//nolint:nosnakecase
	err := syscall.SetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1)
	if err != nil {
		logger.WithError(err).Error("SetsockoptInt")
	}
}

func (httpServer *HTTPServer) serve(listener net.Listener) {
	if err := httpServer.server.Serve(listener); errors.Is(err, http.ErrServerClosed) {
		httpServer.logger.Debug("Server is closed")
	} else {
		httpServer.logger.WithError(err).Error("Server closed with error")
	}
}

// Close HTTP server.
func (httpServer *HTTPServer) Close(ctx context.Context) {
	ctx, cancel := context.WithTimeout(ctx, httpServer.config.StopTimeout)
	defer cancel()

	if err := httpServer.server.Shutdown(ctx); nil != err {
		httpServer.logger.WithError(err).Error("Stop HTTP server error")
	} else {
		httpServer.logger.Info("Server shutdown is successful")
	}
}
