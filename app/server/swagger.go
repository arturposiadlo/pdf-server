package server

import (
	"fmt"
	"net/http"

	"gitlab.com/c0va23/pdf-server/app/services"
)

const (
	renderTag       = "_render"
	examplesTag     = "_examples"
	schemaTag       = "_schema"
	examplesDataTag = "_examplesData"
)

// SwaggerSpecificationInfo define info object subset of swagger 2.0 specification.
type SwaggerSpecificationInfo struct {
	Title   string `json:"title"`
	Version string `json:"version"`
}

// SwaggerSpecificationParameter define parameter object subset of swagger 2.0 specification.
type SwaggerSpecificationParameter struct {
	Name     string     `json:"name"`
	In       string     `json:"in"`
	Required bool       `json:"required"`
	Schema   JSONSchema `json:"schema,omitempty"`
}

// SwaggerSpecificationResponse define response object subset of swagger 2.0 specification.
type SwaggerSpecificationResponse struct {
	Description string     `json:"description"`
	Schema      JSONSchema `json:"schema,omitempty"`
}

// SwaggerSpecificationResponses define responses object subset of swagger 2.0 specification.
type SwaggerSpecificationResponses map[int]SwaggerSpecificationResponse

// SwaggerSpecificationOperation define operation object subset of swagger 2.0 specification.
type SwaggerSpecificationOperation struct {
	Consumes   []string                        `json:"consumes,omitempty"`
	Produces   []string                        `json:"produces,omitempty"`
	Tags       []string                        `json:"tags"`
	Responses  SwaggerSpecificationResponses   `json:"responses"`
	Parameters []SwaggerSpecificationParameter `json:"parameters,omitempty"`
}

// SwaggerSpecificationPath define path object subset of swagger 2.0 specification.
type SwaggerSpecificationPath struct {
	Get  *SwaggerSpecificationOperation `json:"get,omitempty"`
	Post *SwaggerSpecificationOperation `json:"post,omitempty"`
}

// SwaggerSpecification define swagger 2.0 specification subset.
type SwaggerSpecification struct {
	Swagger string                              `json:"swagger"`
	Info    SwaggerSpecificationInfo            `json:"info"`
	Paths   map[string]SwaggerSpecificationPath `json:"paths"`
}

// JSONSchema define JSON schema object subset.
type JSONSchema interface {
	IsJSONSchema()
}

// JSONSchemaType define JSON object type.
type JSONSchemaType string

// JSON object types.
const (
	JSONSchemaTypeObject = JSONSchemaType("object")
	JSONSchemaTypeString = JSONSchemaType("string")
	JSONSchemaTypeNumber = JSONSchemaType("number")
)

// JSONSchemaObject define JSON schema object.
type JSONSchemaObject struct {
	Type       JSONSchemaType        `json:"type"`
	Properties map[string]JSONSchema `json:"properties,omitempty"`
	Items      JSONSchema            `json:"items,omitempty"`
	Required   []string              `json:"required,omitempty"`
	Additional JSONSchema            `json:"additionalProperties,omitempty"`
}

// IsJSONSchema implement JSONSchema interface.
func (JSONSchemaObject) IsJSONSchema() {}

// JSONRef define JSON reference object.
type JSONRef struct {
	Ref string `json:"$ref"`
}

// IsJSONSchema implement JSONSchema interface.
func (JSONRef) IsJSONSchema() {}

func buildTemplateTag(templateName string) string {
	return fmt.Sprintf("template:%s", templateName)
}

// BuildSwaggerSpecification v2.0 from internal entities.
func BuildSwaggerSpecification(
	templateMetadataList []services.TemplateSpec,
) SwaggerSpecification {
	paths := make(map[string]SwaggerSpecificationPath, len(templateMetadataList)+1)

	for _, templateMetadata := range templateMetadataList {
		renderPath, renderPathItem := buildRenderRoute(templateMetadata)
		paths[renderPath] = renderPathItem

		if schemaPath, schemaPathItem := buildSchemaRoute(templateMetadata); schemaPathItem != nil {
			paths[schemaPath] = *schemaPathItem
		}

		if len(templateMetadata.Examples) > 0 {
			examplesDataPath, examplesDataItem := buildExamplesData(templateMetadata)

			paths[examplesDataPath] = examplesDataItem
		}

		for _, exampleMetadata := range templateMetadata.Examples {
			examplePath, examplePathItem := buildExampleRoute(
				templateMetadata,
				exampleMetadata,
			)

			paths[examplePath] = examplePathItem
		}
	}

	statusPath, statusPathItem := buildStatusRoute()
	paths[statusPath] = *statusPathItem

	return SwaggerSpecification{
		Swagger: "2.0",
		Info: SwaggerSpecificationInfo{
			Title:   "PDF server",
			Version: "0.0",
		},
		Paths: paths,
	}
}

func buildRenderRoute(
	templateMetadata services.TemplateSpec,
) (string, SwaggerSpecificationPath) {
	path := fmt.Sprintf("/templates/%s/render", templateMetadata.Name)

	var parameters []SwaggerSpecificationParameter

	if templateMetadata.Schema != nil {
		schemaPath := buildSchemaPath(templateMetadata)
		parameters = []SwaggerSpecificationParameter{
			{
				Name:     "data",
				In:       "body",
				Required: true,
				Schema: JSONRef{
					Ref: schemaPath,
				},
			},
		}
	}

	pathItem := SwaggerSpecificationPath{
		Get: nil,
		Post: &SwaggerSpecificationOperation{
			Consumes: []string{
				jsonContentType,
			},
			Produces: []string{
				pdfContentType,
			},
			Tags: []string{
				buildTemplateTag(templateMetadata.Name),
				renderTag,
			},
			Parameters: parameters,
			Responses: SwaggerSpecificationResponses{
				http.StatusOK: SwaggerSpecificationResponse{
					Description: "Return PDF payload",
				},
				http.StatusBadRequest: SwaggerSpecificationResponse{
					Description: "Body is not valid JSON",
				},
				http.StatusUnprocessableEntity: SwaggerSpecificationResponse{
					Description: "Body JSON is not valid against JSON schema",
				},
				http.StatusInternalServerError: internalErrorResponse(),
			},
		},
	}

	return path, pathItem
}

func buildExampleRoute(
	templateMetadata services.TemplateSpec,
	exampleMetadata services.ExampleSpec,
) (string, SwaggerSpecificationPath) {
	path := fmt.Sprintf(
		"/templates/%s/examples/%s",
		templateMetadata.Name,
		exampleMetadata.Name,
	)
	pathItem := SwaggerSpecificationPath{
		Get: &SwaggerSpecificationOperation{
			Produces: []string{
				pdfContentType,
			},
			Consumes: nil,
			Tags: []string{
				buildTemplateTag(templateMetadata.Name),
				examplesTag,
			},
			Responses: SwaggerSpecificationResponses{
				http.StatusOK: SwaggerSpecificationResponse{
					Description: "Return PDF payload",
				},
				http.StatusInternalServerError: internalErrorResponse(),
			},
		},
		Post: nil,
	}

	return path, pathItem
}

func buildExamplesData(
	templateMetadata services.TemplateSpec,
) (string, SwaggerSpecificationPath) {
	var schema JSONSchema

	if templateMetadata.Schema != nil {
		schema = JSONSchemaObject{
			Type: JSONSchemaTypeObject,
			Additional: JSONRef{
				Ref: buildSchemaPath(templateMetadata),
			},
		}
	}

	path := fmt.Sprintf(
		"/templates/%s/examples-data",
		templateMetadata.Name,
	)
	pathItem := SwaggerSpecificationPath{
		Get: &SwaggerSpecificationOperation{
			Produces: []string{
				jsonContentType,
			},
			Consumes: nil,
			Tags: []string{
				buildTemplateTag(templateMetadata.Name),
				examplesDataTag,
			},
			Responses: SwaggerSpecificationResponses{
				http.StatusOK: SwaggerSpecificationResponse{
					Description: "Template Examples Data",
					Schema:      schema,
				},
				http.StatusInternalServerError: internalErrorResponse(),
			},
		},
		Post: nil,
	}

	return path, pathItem
}

func buildSchemaPath(templateMetadata services.TemplateSpec) string {
	return fmt.Sprintf("/templates/%s/schema.json", templateMetadata.Name)
}

func buildSchemaRoute(
	templateMetadata services.TemplateSpec,
) (string, *SwaggerSpecificationPath) {
	path := buildSchemaPath(templateMetadata)

	if templateMetadata.Schema == nil {
		return path, nil
	}

	schemaItem := SwaggerSpecificationPath{
		Get: &SwaggerSpecificationOperation{
			Produces: []string{
				jsonContentType,
			},
			Consumes: nil,
			Tags: []string{
				buildTemplateTag(templateMetadata.Name),
				schemaTag,
			},
			Responses: SwaggerSpecificationResponses{
				http.StatusOK: SwaggerSpecificationResponse{
					Description: "Return JSON schema of template data",
				},
				http.StatusNotFound: SwaggerSpecificationResponse{
					Description: "Schema not exists",
				},
				http.StatusInternalServerError: internalErrorResponse(),
			},
		},
		Post: nil,
	}

	return path, &schemaItem
}

func buildStatusRoute() (string, *SwaggerSpecificationPath) {
	itemInfo := &SwaggerSpecificationPath{
		Get: &SwaggerSpecificationOperation{
			Produces: []string{
				jsonContentType,
			},
			Consumes: nil,
			Tags:     []string{"status"},
			Responses: SwaggerSpecificationResponses{
				http.StatusOK: SwaggerSpecificationResponse{
					Description: "Return status",
					Schema: &JSONRef{
						Ref: statusSchemaPath,
					},
				},
			},
		},
		Post: nil,
	}

	return statusPath, itemInfo
}

func internalErrorResponse() SwaggerSpecificationResponse {
	return SwaggerSpecificationResponse{
		Description: "Internal server error",
	}
}
