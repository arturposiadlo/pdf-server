package server

import (
	"embed"
	"fmt"
	"io/fs"
	"net/http"
	"os"

	echo "github.com/labstack/echo/v4"
	cliV2 "github.com/urfave/cli/v2"
	"go.opentelemetry.io/contrib/instrumentation/github.com/labstack/echo/otelecho"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
)

const staticSuffix = "/static/"

// HandlersInterface define Handlers interface.
type HandlersInterface interface {
	RenderData(echo.Context) error
	RenderExample(echo.Context) error
	GetSwagger20Spec(echo.Context) error
	GetSchema(echo.Context) error
	GetExamplesData(echo.Context) error
	Status(echo.Context) error
	StatusSchema(echo.Context) error
	Metadata(echo.Context) error
}

// EchoRouter implement router with echo.
type EchoRouter struct {
	handlers HandlersInterface
	*echo.Echo
}

//go:embed static
var embeddedStaticFS embed.FS

// FSDebugger log static file system calls.
type FSDebugger struct {
	logger log.Logger
	fs     http.FileSystem
}

// Open implements http.FS.
func (fsDebugger *FSDebugger) Open(path string) (http.File, error) {
	logger := fsDebugger.logger.WithField("path", path)

	file, err := fsDebugger.fs.Open(path)
	if err != nil {
		logger.WithError(err).Error("Open file error")

		return nil, fmt.Errorf("file open: %w", err)
	}

	return file, nil
}

// StaticDirConfig define path and enabling to static dir.
type StaticDirConfig struct {
	Path    string
	Enabled bool
}

// DefaultStaticDirConfig provider.
func DefaultStaticDirConfig() StaticDirConfig {
	return StaticDirConfig{
		Path:    "./app/server/static/",
		Enabled: false,
	}
}

// Flags return config bindings for cli.
func (config *StaticDirConfig) Flags() []cliV2.Flag {
	return []cliV2.Flag{
		&cliV2.BoolFlag{
			Name:        "static-dir-enabled",
			Usage:       "Read static from dir instead embedded",
			Destination: &config.Enabled,
			Value:       config.Enabled,
			Hidden:      false,
			Aliases:     []string{},
			EnvVars:     []string{"STATIC_DIR_ENABLED"},
			FilePath:    "",
			Required:    false,
			HasBeenSet:  false,
			DefaultText: "",
		},
		&cliV2.StringFlag{
			Name:        "static-dir-path",
			Usage:       "Path to static dir",
			Destination: &config.Path,
			Value:       config.Path,
			Aliases:     []string{},
			EnvVars:     []string{"STATIC_DIR_PATH"},
			FilePath:    "",
			Required:    false,
			Hidden:      false,
			HasBeenSet:  false,
			DefaultText: "",
			TakesFile:   false,
		},
	}
}

func staticDir(logger log.Logger, config StaticDirConfig) (http.FileSystem, error) {
	if config.Enabled {
		logger.Infof("Static dir path %s", config.Path)

		if _, err := os.Lstat(config.Path); err != nil {
			return nil, fmt.Errorf("lstat on %s: %w", config.Path, err)
		}

		return &FSDebugger{
			logger: logger,
			fs:     http.Dir(config.Path),
		}, nil
	}

	staticFs, err := fs.Sub(embeddedStaticFS, "static")
	if err != nil {
		return nil, fmt.Errorf("sub fs: %w", err)
	}

	return http.FS(staticFs), nil
}

const (
	statusPath       = "/_status"
	statusSchemaPath = statusPath + "/schema.json"
)

// NewEchoRouter constructor.
func NewEchoRouter(
	handlers HandlersInterface,
	logger log.Logger,
	tracerProvider trace.TracerProvider,
	staticDirConfig StaticDirConfig,
) (*EchoRouter, error) {
	e := echo.New()
	e.Use(RequestIDLogger(logger))
	e.Use(panicRecovery(logger))
	e.Use(otelecho.Middleware(
		"router",
		otelecho.WithTracerProvider(tracerProvider),
	))
	e.POST("/templates/:template_name/render", handlers.RenderData)
	e.GET("/templates/:template_name/examples/:example_name", handlers.RenderExample)
	e.GET("/templates/:template_name/examples-data", handlers.GetExamplesData)
	e.GET("/templates/:template_name/schema.json", handlers.GetSchema)
	e.GET("/spec/swagger.json", handlers.GetSwagger20Spec)
	e.GET(statusPath, handlers.Status)
	e.GET(statusSchemaPath, handlers.StatusSchema)
	e.GET("/_metadata", handlers.Metadata)

	staticFs, err := staticDir(logger, staticDirConfig)
	if err != nil {
		return nil, fmt.Errorf("static dir: %w", err)
	}

	e.GET(
		staticSuffix+"*",
		echo.WrapHandler(
			http.StripPrefix(
				"/static/",
				http.FileServer(staticFs),
			),
		),
	)

	e.GET("/", func(c echo.Context) error {
		return c.Redirect(http.StatusTemporaryRedirect, staticSuffix)
	})

	return &EchoRouter{
		handlers: handlers,
		Echo:     e,
	}, nil
}
