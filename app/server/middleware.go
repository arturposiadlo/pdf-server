package server

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"runtime/debug"
	"time"

	echo "github.com/labstack/echo/v4"

	"gitlab.com/c0va23/pdf-server/app/log"
)

const requestIDqueryParam = "request_id"

func generateRequestID() string {
	srcID := rand.Uint64()

	return fmt.Sprintf("%x", srcID)
}

// RequestIDLogger inject logger to request context and extract or generate request ID.
func RequestIDLogger(logger log.Logger) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(echoCtx echo.Context) error {
			startTime := time.Now()

			requestID := echoCtx.Request().URL.Query().Get(requestIDqueryParam)
			if requestID == "" {
				requestID = generateRequestID()
			}

			requestLogger := logger.
				WithField(log.LoggerField, "request").
				WithField(log.FieldRequestID, requestID)

			contextWithLogger := log.ContextWithLogger(echoCtx.Request().Context(), requestLogger)
			contextWithLogger = context.WithValue(contextWithLogger, log.ContextKeyRequestID, requestID)

			requestWithLogger := echoCtx.Request().WithContext(contextWithLogger)
			echoCtx.SetRequest(requestWithLogger)

			handlerErr := next(echoCtx)

			duration := time.Since(startTime)

			responseLogger := requestLogger.
				WithField(log.FieldRequestRoute, echoCtx.Path()).
				WithField(log.FieldRequestIP, echoCtx.RealIP()).
				WithField(log.FieldRequestMethod, echoCtx.Request().Method).
				WithField(log.FieldRequestPath, echoCtx.Request().URL.Path).
				WithField(log.FieldRequestQuery, echoCtx.QueryString()).
				WithField(log.FieldDuration, log.Duration(duration)).
				WithField(log.FieldResponseCode, echoCtx.Response().Status).
				WithField(log.FieldResponseSize, echoCtx.Response().Size)

			if handlerErr != nil {
				responseLogger.WithError(handlerErr).Error("Response error")

				return handlerErr
			}

			responseLogger.Info("Response success")

			return nil
		}
	}
}

// revive:disable:cognitive-complexity // TODO: rewrite function
func panicRecovery(logger log.Logger) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(echoCtx echo.Context) (err error) {
			defer func() {
				panicErr := recover()
				if panicErr == nil {
					return
				}

				stack := debug.Stack()

				logger := logger.WithField(log.FieldStack, string(stack))

				castedError, ok := panicErr.(error)
				if ok {
					logger.WithError(castedError).Error("Panic recovered")

					err = echo.NewHTTPError(http.StatusInternalServerError, castedError)

					return
				}

				logger.Errorf("Handler panic without error: %s", panicErr)

				err = echo.ErrInternalServerError
			}()

			err = next(echoCtx)

			return err
		}
	}
}
