package httputil

import (
	"fmt"

	"github.com/labstack/echo/v4"
)

// SetContentLength set content length header.
func SetContentLength(c echo.Context, content []byte) {
	c.Response().Header().Set(
		echo.HeaderContentLength,
		fmt.Sprintf("%d", len(content)),
	)
}
