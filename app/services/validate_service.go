package services

import (
	"context"
	"fmt"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

// ValidateService interface.
type ValidateService interface {
	Validate(ctx context.Context) error
}

const validateServiceLogger = "validator"

// SimpleValidateService parse templates and render all examples.
type SimpleValidateService struct {
	templatesStore templates.Store
}

// NewSimpleValidateService constructor.
func NewSimpleValidateService(
	templatesStore templates.Store,
) *SimpleValidateService {
	return &SimpleValidateService{
		templatesStore,
	}
}

// Validate iterate over all templates and render all examples. Return false
// if any render of example return error.
func (validateService *SimpleValidateService) Validate(ctx context.Context) error {
	logger := log.LoggerFromContext(ctx, validateServiceLogger)

	templateList, err := validateService.templatesStore.List()
	if nil != err {
		return fmt.Errorf("list templates: %w", err)
	}

	for _, template := range templateList {
		for exampleName, exampleData := range template.GetExamples() {
			logger := logger.
				WithField(log.FieldTemplateName, template.GetName()).
				WithField(log.FieldTemplateExample, exampleName)

			_, err := template.Render(
				ctx,
				exampleData,
			)
			if nil != err {
				return fmt.Errorf("render example %s/%s: %w", template.GetName(), exampleName, err)
			}

			logger.Info("Success render template")
		}
	}

	return nil
}
