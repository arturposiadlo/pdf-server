package services_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/services"
	"gitlab.com/c0va23/pdf-server/app/templates"

	templatesMocks "gitlab.com/c0va23/pdf-server/mocks/templates"
)

type RenderPDFTestCase struct {
	name                 string
	storeFetchResult     *pdfrender.Document
	storeFetchError      error
	templateValidator    templates.Validator
	validationError      error
	templateRenderResult []byte
	templateRenderErr    error
	expectedError        error
	expectedPayload      []byte
}

func TestBasePDFRenderService_RenderPDF(t *testing.T) {
	t.Parallel()

	var (
		logger = log.NewLogger(log.LevelWarn, log.TextFormat)
		ctx    = log.ContextWithLogger(context.TODO(), logger)

		templateData = map[string]any{
			"key": "value",
		}
		templateName = "testTemplate"

		validPDFPayload = []byte("PDF payload")
		testCases       = []RenderPDFTestCase{
			func() RenderPDFTestCase {
				templateErr := templates.NewTemplateNotExistsError(templates.Name(templateName))

				return RenderPDFTestCase{
					name:                 "template not found",
					storeFetchResult:     nil,
					storeFetchError:      templateErr,
					templateValidator:    nil,
					templateRenderResult: nil,
					templateRenderErr:    nil,
					expectedError:        services.NewTemplateNotFound(templateErr),
				}
			}(),
			func() RenderPDFTestCase {
				renderHTMLError := errors.New("render HTML error")

				return RenderPDFTestCase{
					name:                 "render error",
					storeFetchError:      nil,
					templateValidator:    nil,
					validationError:      nil,
					templateRenderResult: nil,
					templateRenderErr:    renderHTMLError,
					expectedError:        renderHTMLError,
				}
			}(),
			func() RenderPDFTestCase {
				validationError := errors.New("validation error")

				validator := new(templatesMocks.Validator)
				validator.EXPECT().
					ValidateData(mock.Anything, templates.Data(templateData)).
					Return(validationError)

				return RenderPDFTestCase{
					name:                 "validation error",
					storeFetchError:      nil,
					templateValidator:    validator,
					validationError:      validationError,
					templateRenderResult: nil,
					templateRenderErr:    nil,
					expectedError:        services.NewDataValidationError(validationError),
				}
			}(),
			{
				name:                 "success render",
				storeFetchError:      nil,
				templateValidator:    nil,
				validationError:      nil,
				templateRenderResult: validPDFPayload,
				templateRenderErr:    nil,
				expectedError:        nil,
				expectedPayload:      validPDFPayload,
			},
		}
	)

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			tracerProvider := trace.NewNoopTracerProvider()

			storeMock := new(templatesMocks.Store)
			defer storeMock.AssertExpectations(t)

			var template *templatesMocks.Template

			if testCase.storeFetchError == nil {
				template = new(templatesMocks.Template)
				defer template.AssertExpectations(t)

				template.EXPECT().GetName().Return(templates.Name(templateName))
				template.EXPECT().GetValidator().Return(testCase.templateValidator)

				if testCase.templateRenderResult != nil || testCase.templateRenderErr != nil {
					template.EXPECT().
						Render(mock.Anything, templates.Data(templateData)).
						Return(testCase.templateRenderResult, testCase.templateRenderErr)
				}
			}

			storeMock.EXPECT().Fetch(templates.Name(templateName)).Return(template, testCase.storeFetchError)

			service := services.NewBasePDFRenderService(storeMock, tracerProvider)

			actualPayload, actualError := service.RenderPDFWithData(ctx, templateName, templateData)

			if testCase.expectedError == nil {
				assert.NoError(t, actualError)
			} else {
				assert.ErrorIs(t, actualError, testCase.expectedError)
			}

			assert.Equal(t, testCase.expectedPayload, actualPayload)
		})
	}
}
