package services

import (
	"context"
	"fmt"

	"gitlab.com/c0va23/pdf-server/app/pdfrender"
)

// RenderPoolStats container internal render pool stats.
type RenderPoolStats struct {
	Active      int
	Idle        int
	Destroyed   int
	Invalidated int
}

// RenderPoolInfo contains pool stats and type.
type RenderPoolInfo struct {
	Stats RenderPoolStats
	Type  string
}

// RenderInfo contains pool info and metadata.
type RenderInfo struct {
	Pool     RenderPoolInfo
	Metadata map[string]string
}

// Stats of service.
type Stats struct {
	RenderInfo RenderInfo
}

// StatsService collect service stats.
type StatsService interface {
	Stats(context.Context) (*Stats, error)
}

// DirectStatsService return service stats.
type DirectStatsService struct {
	renderer pdfrender.PDFRenderer
}

// NewDirectStatsService constructor.
func NewDirectStatsService(
	renderer pdfrender.PDFRenderer,
) *DirectStatsService {
	return &DirectStatsService{
		renderer: renderer,
	}
}

// Stats collect service stats.
func (service *DirectStatsService) Stats(ctx context.Context) (*Stats, error) {
	renderInfo, err := service.renderer.Info(ctx)
	if err != nil {
		return nil, fmt.Errorf("renderer info: %w", err)
	}

	return &Stats{
		RenderInfo: RenderInfo{
			Pool: RenderPoolInfo{
				Stats: RenderPoolStats{
					Active:      renderInfo.Pool.Stats.Active,
					Idle:        renderInfo.Pool.Stats.Idle,
					Destroyed:   renderInfo.Pool.Stats.Destroyed,
					Invalidated: renderInfo.Pool.Stats.Invalidated,
				},
				Type: renderInfo.Pool.Type,
			},
			Metadata: renderInfo.Metadata,
		},
	}, nil
}
