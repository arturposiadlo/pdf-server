package services

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

const (
	pdfRenderServiceLoggerName = "pdfRenderService"
)

// Data for render HTML.
type Data = templates.Data

// DataValidationError returned by services when template return data validation error.
type DataValidationError struct {
	inner error
}

// NewDataValidationError construct new DataValidationError.
func NewDataValidationError(err error) *DataValidationError {
	return &DataValidationError{
		inner: err,
	}
}

func (err *DataValidationError) Error() string {
	return fmt.Sprintf("data validation error: %s", err.inner)
}

// Unwrap inner error.
func (err *DataValidationError) Unwrap() error {
	return err.inner
}

// Is cast error type and match inner errors.
func (err *DataValidationError) Is(otherErr error) bool {
	if castedErr := new(DataValidationError); errors.As(otherErr, &castedErr) {
		return errors.Is(castedErr.inner, err.inner)
	}

	return false
}

// TemplateNotFoundError wrap templates.TemplateNotExistsError.
type TemplateNotFoundError struct {
	inner error
}

// NewTemplateNotFound construct new TemplateNotFoundError.
func NewTemplateNotFound(err error) *TemplateNotFoundError {
	return &TemplateNotFoundError{
		inner: err,
	}
}

func (err *TemplateNotFoundError) Error() string {
	return fmt.Sprintf("template not found: %s", err.inner)
}

// Is cast error type and match inner errors.
func (err *TemplateNotFoundError) Is(otherErr error) bool {
	if castedErr := new(TemplateNotFoundError); errors.As(otherErr, &castedErr) {
		return errors.Is(castedErr.inner, err.inner)
	}

	return false
}

// Unwrap inner error.
func (err *TemplateNotFoundError) Unwrap() error {
	return err.inner
}

// ExampleNotFoundError wrap templates error.
type ExampleNotFoundError struct {
	exampleName string
}

// NewExampleNotFoundError construct new ExampleNotFoundError.
func NewExampleNotFoundError(exampleName string) *ExampleNotFoundError {
	return &ExampleNotFoundError{
		exampleName: exampleName,
	}
}

func (err *ExampleNotFoundError) Error() string {
	return fmt.Sprintf("example %s not found", err.exampleName)
}

// Is change error type and call errors.Is on inner error.
func (err *ExampleNotFoundError) Is(otherErr error) bool {
	if castedErr := new(ExampleNotFoundError); errors.As(otherErr, &castedErr) {
		return err.exampleName == castedErr.exampleName
	}

	return false
}

// PDFRenderService interface describe service when render PDF by template name
// and data.
type PDFRenderService interface {
	RenderPDFWithData(
		ctx context.Context,
		templateName string,
		data Data,
	) ([]byte, error)
	RenderPDFExample(
		ctx context.Context,
		templateName string,
		exampleName string,
	) ([]byte, error)
	GetExamplesData(
		ctx context.Context,
		templateName string,
	) (map[string]Data, error)
}

// BasePDFRenderService base implementation of PDFRenderService.
type BasePDFRenderService struct {
	store  templates.Store
	tracer trace.Tracer
}

// NewBasePDFRenderService constructor.
func NewBasePDFRenderService(
	store templates.Store,
	tracerProvider trace.TracerProvider,
) *BasePDFRenderService {
	tracer := tracerProvider.Tracer("BasePDFRenderService")

	return &BasePDFRenderService{
		store: store,

		tracer: tracer,
	}
}

// RenderPDFWithData find template by name and render it to PDF.
func (pdfRender *BasePDFRenderService) RenderPDFWithData(
	ctx context.Context,
	templateName string,
	data Data,
) ([]byte, error) {
	ctxWithSpan, span := pdfRender.tracer.Start(ctx, "RenderPDFWithData")
	defer span.End()

	template, err := pdfRender.store.Fetch(templates.Name(templateName))
	if err != nil {
		return nil, NewTemplateNotFound(err)
	}

	payload, err := pdfRender.renderTemplate(ctxWithSpan, template, data)
	if err != nil {
		return nil, fmt.Errorf("render template: %w", err)
	}

	return payload, nil
}

func (pdfRender *BasePDFRenderService) renderTemplate(
	ctx context.Context,
	template templates.Template,
	data templates.Data,
) ([]byte, error) {
	logger := log.LoggerFromContext(ctx, pdfRenderServiceLoggerName).WithFields(logrus.Fields{
		log.FieldTemplateName: template.GetName(),
	})
	ctx = log.ContextWithLogger(ctx, logger)

	if validator := template.GetValidator(); validator != nil {
		if err := validator.ValidateData(ctx, data); err != nil {
			return nil, NewDataValidationError(err)
		}

		logger.Debug("Valid data")
	}

	payload, err := template.Render(ctx, data)
	if err != nil {
		return nil, pdfRender.parseRenderError(err)
	}

	return payload, nil
}

// RenderPDFExample find template and example by name and render it to PDF.
func (pdfRender *BasePDFRenderService) RenderPDFExample(
	ctx context.Context,
	templateName string,
	exampleName string,
) ([]byte, error) {
	ctxWithSpan, span := pdfRender.tracer.Start(ctx, "RenderPDFExample")
	defer span.End()

	template, err := pdfRender.store.Fetch(templates.Name(templateName))
	if err != nil {
		return nil, NewTemplateNotFound(err)
	}

	example, exampleExists := template.GetExamples()[exampleName]
	if !exampleExists {
		return nil, NewExampleNotFoundError(exampleName)
	}

	ctxWithLogger := log.WithLoggerFields(ctxWithSpan, logrus.Fields{
		log.FieldTemplateExample: exampleName,
	})

	payload, err := pdfRender.renderTemplate(ctxWithLogger, template, example)
	if err != nil {
		return nil, fmt.Errorf("render example: %w", err)
	}

	return payload, nil
}

func (*BasePDFRenderService) parseRenderError(err error) error {
	if notFound := new(templates.TemplateNotExistsError); errors.As(err, &notFound) {
		return NewTemplateNotFound(err)
	}

	return fmt.Errorf("unexpected render error: %w", err)
}

// GetExamplesData return examples data for template.
func (pdfRender *BasePDFRenderService) GetExamplesData(
	_ context.Context,
	templateName string,
) (map[string]Data, error) {
	template, err := pdfRender.store.Fetch(templates.Name(templateName))
	if err != nil {
		return nil, fmt.Errorf("get examples data: %w", err)
	}

	return template.GetExamples(), nil
}
