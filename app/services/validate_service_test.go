package services_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/services"
	"gitlab.com/c0va23/pdf-server/app/templates"

	templatesMocks "gitlab.com/c0va23/pdf-server/mocks/templates"
)

func TestSimpleValidateService_Validate(t *testing.T) {
	t.Parallel()

	var (
		logger = log.NewLogger(log.LevelFatal, log.TextFormat)
		ctx    = log.ContextWithLogger(context.Background(), logger)

		templateName = "template_with_examples"
		exampleName  = "simple"

		validTemplates = func(renderPayload []byte, renderError error) []templates.Template {
			templateMock := new(templatesMocks.Template)

			templateMock.EXPECT().GetName().Return(templates.Name(templateName))

			exampleData := templates.Data{
				"key": "value",
			}

			templateMock.EXPECT().GetExamples().Return(templates.Examples{
				exampleName: exampleData,
			})

			if renderPayload != nil || renderError != nil {
				templateMock.EXPECT().
					Render(ctx, exampleData).
					Return(renderPayload, renderError)
			}

			return []templates.Template{
				templateMock,
			}
		}
	)

	type TestCase struct {
		name          string
		listTemplates []templates.Template
		listError     error
		renderPayload []byte
		renderError   error
		expectedError error
	}

	testCases := []TestCase{
		func() TestCase {
			listErr := errors.New("list error")

			return TestCase{
				name:          "list error",
				listTemplates: nil,
				listError:     listErr,
				renderPayload: nil,
				renderError:   nil,
				expectedError: listErr,
			}
		}(),
		func() TestCase {
			renderErr := errors.New("render error")

			return TestCase{
				name:          "render error",
				listTemplates: validTemplates(nil, renderErr),
				listError:     nil,
				renderPayload: nil,
				renderError:   renderErr,
				expectedError: renderErr,
			}
		}(),
		func() TestCase {
			validPayload := []byte("%PDF-1.4")

			return TestCase{
				name:          "success",
				listTemplates: validTemplates(validPayload, nil),
				listError:     nil,
				renderError:   nil,
				expectedError: nil,
			}
		}(),
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			templatesStore := new(templatesMocks.Store)
			defer templatesStore.AssertExpectations(t)

			validateService := services.NewSimpleValidateService(
				templatesStore,
			)

			templatesStore.
				EXPECT().
				List().
				Return(testCase.listTemplates, testCase.listError)

			actualErr := validateService.Validate(ctx)

			assert.ErrorIs(t, actualErr, testCase.expectedError)
		})
	}
}
