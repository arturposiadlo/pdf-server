// Package log configuration
package log

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
)

// Logger fields.
const (
	LoggerField = "logger"

	FieldStack = "stack"

	FieldTemplateLoader  = "template_loader"
	FieldTemplateName    = "template_name"
	FieldTemplatePart    = "template_part"
	FieldTemplateType    = "template_type"
	FieldTemplateDir     = "template_dir"
	FieldTemplatePath    = "template_path"
	FieldTemplateExample = "template_example"

	FieldDocumentID  = "document_id"
	FieldContentType = "content_type"
	FieldAssetPath   = "asset_path"

	FieldListenAddr  = "listen_addr"
	FieldDocumentURL = "document_url"

	FieldBrowserReply = "browser_reply"
	FieldBrowserEvent = "browser_event"
	FieldBrowserOut   = "browser_out"
	FieldBrowserState = "browser_state"
	FieldBrowserData  = "browser_data"

	FieldRenderDataErrors = "render_data_errors"
	FieldRenderData       = "render_data"

	FieldConfig = "config"

	FieldDuration = "duration"

	FieldRequestID     = "request_id"
	FieldRequestIP     = "addr"
	FieldRequestMethod = "method"
	FieldRequestPath   = "path"
	FieldRequestRoute  = "route"
	FieldRequestQuery  = "query"

	FieldResponseCode = "response_code"
	FieldResponseSize = "response_size"
)

const timeFormat = "2006-01-02T15:04:05.000-0700"

// Logger levels.
const (
	LevelTrace = Level(logrus.TraceLevel)
	LevelDebug = Level(logrus.DebugLevel)
	LevelInfo  = Level(logrus.InfoLevel)
	LevelWarn  = Level(logrus.WarnLevel)
	LevelError = Level(logrus.ErrorLevel)
	LevelFatal = Level(logrus.FatalLevel)
)

// Level is logrus.Level wrapper with support flag.Value interface.
type Level logrus.Level

// AvailableLevels return all available log levels.
func AvailableLevels() []Level {
	return []Level{
		LevelTrace,
		LevelDebug,
		LevelInfo,
		LevelWarn,
		LevelError,
		LevelFatal,
	}
}

// Logger interface.
type Logger logrus.FieldLogger

// Set implement flag.Value.Set interface.
func (ll *Level) Set(s string) error {
	level, err := logrus.ParseLevel(s)
	if nil != err {
		return fmt.Errorf("parse level: %w", err)
	}

	*ll = Level(level)

	return nil
}

func (ll Level) String() string {
	return logrus.Level(ll).String()
}

// Format text or json.
type Format string

// Definition of available log formats.
const (
	TextFormat = Format("text")
	JSONFormat = Format("json")
)

// UnknownFormatError return by LogFormat.Set when provided unknown format.
type UnknownFormatError struct {
	value string
}

func (err *UnknownFormatError) Error() string {
	return fmt.Sprintf("unknown log format: %s", err.value)
}

// NewUnknownFormatError constructor.
func NewUnknownFormatError(value string) *UnknownFormatError {
	return &UnknownFormatError{
		value: value,
	}
}

// AvailableFormats return list all available log formats.
func AvailableFormats() []Format {
	return []Format{
		TextFormat,
		JSONFormat,
	}
}

// Set parse log format value on parsing cli arguments.
func (format *Format) Set(value string) error {
	for _, availableFormat := range AvailableFormats() {
		if availableFormat.String() == value {
			*format = availableFormat

			return nil
		}
	}

	return NewUnknownFormatError(value)
}

func (format Format) String() string {
	return string(format)
}

// NewLogger with selected level.
func NewLogger(level Level, format Format) *logrus.Logger {
	return NewLoggerFromConfig(Config{
		Level:  level,
		Format: format,
	})
}

// NewLoggerFromConfig constructor.
func NewLoggerFromConfig(config Config) *logrus.Logger {
	logger := logrus.New()

	switch config.Format {
	case TextFormat:
		logger.Formatter = &logrus.TextFormatter{
			DisableColors:    true,
			FullTimestamp:    true,
			QuoteEmptyFields: true,
			TimestampFormat:  timeFormat,
		}
	case JSONFormat:
		logger.Formatter = &logrus.JSONFormatter{
			TimestampFormat:  timeFormat,
			DisableTimestamp: false,
			PrettyPrint:      false,
			DataKey:          "data",
		}
	}

	logger.Level = logrus.Level(config.Level)

	return logger
}

// Duration used to wrap time.Duration value to log only seconds.
type Duration time.Duration

// String return test representation of Duration for logging value.
func (duration Duration) String() string {
	return fmt.Sprintf("%f", time.Duration(duration).Seconds())
}
