package pdfrender

import (
	"encoding/json"
	"fmt"
)

// WaitLifecycleEvent is type for waited LifecycleEvent.
type WaitLifecycleEvent string

// Wait Lifecycle events.
const (
	WaitLifecycleEventInit                 = WaitLifecycleEvent("init")
	WaitLifecycleEventLoad                 = WaitLifecycleEvent("load")
	WaitLifecycleEventDOMContentLoaded     = WaitLifecycleEvent("DOMContentLoaded")
	WaitLifecycleEventFirstPaint           = WaitLifecycleEvent("firstPaint")
	WaitLifecycleEventFirstContentfulPaint = WaitLifecycleEvent("firstContentfulPaint")

	WaitLifecycleEventFirstMeaningfulPaintCandidate = WaitLifecycleEvent(
		"firstMeaningfulPaintCandidate",
	)

	WaitLifecycleEventNetworkAlmostIdle    = WaitLifecycleEvent("networkAlmostIdle")
	WaitLifecycleEventFirstMeaningfulPaint = WaitLifecycleEvent("firstMeaningfulPaint")
	WaitLifecycleEventNetworkIdle          = WaitLifecycleEvent("networkIdle")
)

// WaitLifecycleEvents list of allowed WaitLifecycleEvent.
//
//nolint:gochecknoglobals
var WaitLifecycleEvents = []WaitLifecycleEvent{
	WaitLifecycleEventInit,
	WaitLifecycleEventLoad,
	WaitLifecycleEventDOMContentLoaded,
	WaitLifecycleEventFirstPaint,
	WaitLifecycleEventFirstContentfulPaint,
	WaitLifecycleEventFirstMeaningfulPaintCandidate,
	WaitLifecycleEventNetworkAlmostIdle,
	WaitLifecycleEventFirstMeaningfulPaint,
	WaitLifecycleEventNetworkIdle,
}

// InvalidWaitLifecycleEventError returned when provided unexpected event type.
type InvalidWaitLifecycleEventError struct {
	eventType string
}

// NewInvalidWaitLifecycleEventError construct new InvalidWaitLifecycleEventError.
func NewInvalidWaitLifecycleEventError(eventType string) *InvalidWaitLifecycleEventError {
	return &InvalidWaitLifecycleEventError{
		eventType: eventType,
	}
}

func (err *InvalidWaitLifecycleEventError) Error() string {
	return fmt.Sprintf("invalid WaitLifecycleEvent value: %s", err.eventType)
}

// String return event value.
func (event *WaitLifecycleEvent) String() string {
	return string(*event)
}

// Set value. On not valid value return InvalidWaitLifecycleEventError.
func (event *WaitLifecycleEvent) Set(value string) error {
	v := WaitLifecycleEvent(value)
	for _, e := range WaitLifecycleEvents {
		if v != e {
			continue
		}

		*event = v

		return nil
	}

	return NewInvalidWaitLifecycleEventError(value)
}

// UnmarshalJSON unmarshal json.
func (event *WaitLifecycleEvent) UnmarshalJSON(value []byte) error {
	var str string

	if err := json.Unmarshal(value, &str); err != nil {
		return fmt.Errorf("failed to unmarshal WaitLifecycleEvent: %w", err)
	}

	if err := event.Set(str); err != nil {
		return fmt.Errorf("failed to set WaitLifecycleEvent: %w", err)
	}

	return nil
}

// Default values for RenderConfig.
const (
	DefaultWaitLifecycleEvent = WaitLifecycleEventLoad
)
