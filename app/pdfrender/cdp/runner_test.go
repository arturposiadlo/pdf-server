package cdp_test

import (
	"context"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/cdp"
)

func TestBrowserRunner(t *testing.T) {
	t.Parallel()

	config := getBrowserRunnerConfig()
	if config == nil {
		t.Skip("Browser not found")

		return
	}

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	tracerProvider := trace.NewNoopTracerProvider()

	runner, err := cdp.NewBrowserRunner(*config, logger, tracerProvider)
	assert.NoError(t, err)

	devToolURLPattern := `^http://localhost:\d+$`

	ctx := context.Background()

	wsURL, runErr := runner.Run(ctx)
	assert.Nil(t, runErr)

	defer func() {
		stopErr := runner.Stop(ctx)
		assert.Nil(t, stopErr)
	}()

	wsURLMatched, err := regexp.MatchString(devToolURLPattern, wsURL)
	assert.True(t, wsURLMatched)
	assert.NoError(t, err)
}
