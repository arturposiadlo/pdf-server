package cdp

import (
	"context"
	"fmt"

	"github.com/mafredri/cdp/protocol/page"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/server"
)

const cdpRenderLoggerName = "cdp-render"

// Render is CeDT based PDFRender implementation.
type Render struct {
	ctx    context.Context
	pool   Borrower
	store  server.Store
	server server.Server
	logger log.Logger
	config RenderConfig
	tracer trace.Tracer
}

// NewRender constructor.
//
// revive:disable:argument-limit
func NewRender(
	ctx context.Context,
	pool Borrower,
	renderConfig RenderConfig,
	store server.Store,
	internalServer server.Server,
	tracerProvider trace.TracerProvider,
) *Render {
	logger := log.LoggerFromContext(ctx, cdpRenderLoggerName)

	tracer := tracerProvider.Tracer("Render")

	return &Render{
		ctx:    ctx,
		pool:   pool,
		server: internalServer,
		store:  store,
		logger: logger,
		config: renderConfig,
		tracer: tracer,
	}
}

func (render *Render) printPDF(
	ctx context.Context,
	pg Page,
	params page.PrintToPDFArgs,
	url string,
	waitLifecycleEvent *pdfrender.WaitLifecycleEvent,
) ([]byte, error) {
	logger := log.LoggerFromContext(ctx, cdpRenderLoggerName)
	logger.Debug("Print PDF")

	lifeCycleClient, err := pg.LifecycleEvent(ctx)
	if err != nil {
		return nil, fmt.Errorf("lifecycle events: %w", err)
	}

	defer render.closeLifeCycleClient(ctx, lifeCycleClient)

	logger.Debug("Lifecycle events open success")

	if err = render.navigatePage(ctx, pg, url); nil != err {
		return nil, fmt.Errorf("open page: %w", err)
	}

	if err = render.waitLifecycleEvent(ctx, lifeCycleClient, waitLifecycleEvent); err != nil {
		return nil, fmt.Errorf("wait lifecycle event: %w", err)
	}

	pdfReply, err := pg.PrintToPDF(ctx, &params)
	if nil != err {
		return nil, fmt.Errorf("print to pdf: %w", err)
	}

	logger.Debug("Print PDF success")
	logger.WithField(log.FieldBrowserReply, pdfReply).Trace("Print PDF reply")

	return pdfReply.Data, nil
}

func (*Render) navigatePage(
	ctx context.Context,
	pg Page,
	url string,
) (err error) {
	logger := log.LoggerFromContext(ctx, cdpRenderLoggerName)

	navigateArgs := page.NavigateArgs{
		URL: url,
	}

	navReply, err := pg.Navigate(ctx, &navigateArgs)
	if nil != err {
		return fmt.Errorf("navigate page: %w", err)
	}

	logger.Debug("Page navigate success")
	logger.WithField(log.FieldBrowserReply, navReply).Trace("Page navigate reply")

	return nil
}

func (render *Render) closeLifeCycleClient(
	ctx context.Context,
	lifeCycleClient page.LifecycleEventClient,
) {
	_, span := render.tracer.Start(ctx, "closeLifeCycleClient")
	defer span.End()

	logger := log.LoggerFromContext(ctx, cdpRenderLoggerName)

	if err := lifeCycleClient.Close(); nil != err {
		logger.WithError(err).Error("Close lifecycle events error")
	} else {
		logger.Debug("Lifecycle events closed success")
	}
}

func (render *Render) waitLifecycleEvent(
	ctx context.Context,
	lifeCycleClient page.LifecycleEventClient,
	waitLifecycleEvent *pdfrender.WaitLifecycleEvent,
) (err error) {
	_, span := render.tracer.Start(ctx, "waitLifecycleEvent")
	defer span.End()

	logger := log.LoggerFromContext(ctx, cdpRenderLoggerName)

	expectedWaitLifecycleEvent := render.config.WaitLifecycleEvent
	if waitLifecycleEvent != nil {
		expectedWaitLifecycleEvent = *waitLifecycleEvent
	}

	logger.Debugf("wait %s lifecycle event", expectedWaitLifecycleEvent)

	for {
		lifecycleEvent, err := lifeCycleClient.Recv()
		if nil != err {
			span.SetStatus(codes.Error, err.Error())

			return fmt.Errorf("lifecycle event: %w", err)
		}

		span.AddEvent("lifecycleEvent", trace.WithAttributes(attribute.KeyValue{
			Key:   "eventName",
			Value: attribute.StringValue(lifecycleEvent.Name),
		}))

		logger := logger.WithField(log.FieldBrowserEvent, lifecycleEvent.Name)

		if lifecycleEvent.Name == expectedWaitLifecycleEvent.String() {
			logger.Debug("Found expected lifecycle event")

			break
		}

		logger.Debug("Skip lifecycle event")
	}

	return nil
}

func (*Render) buildParams(
	config *pdfrender.Config,
	document pdfrender.Document,
) page.PrintToPDFArgs {
	displayHeaderFooter := document.HeaderHTML != nil || document.FooterHTML != nil

	return page.PrintToPDFArgs{
		Landscape:           config.Landscape,
		DisplayHeaderFooter: &displayHeaderFooter,
		PrintBackground:     config.PrintBackground,
		Scale:               config.Scale,
		PaperWidth:          config.PaperWidth,
		PaperHeight:         config.PaperHeight,
		MarginTop:           config.MarginTop,
		MarginBottom:        config.MarginBottom,
		MarginLeft:          config.MarginLeft,
		MarginRight:         config.MarginRight,
		HeaderTemplate:      document.HeaderHTML,
		FooterTemplate:      document.FooterHTML,
		PreferCSSPageSize:   config.PreferCSSPageSize,
	}
}

// RenderPDF via CDT.
func (render *Render) RenderPDF(
	ctx context.Context,
	config *pdfrender.Config,
	document pdfrender.Document,
) ([]byte, error) {
	ctxWithSpan, span := render.tracer.Start(ctx, "RenderPDF")
	defer span.End()

	if config == nil {
		config = new(pdfrender.Config)
	}

	return render.renderPDFWithPool(ctxWithSpan, config, document)
}

func (render *Render) renderPDFWithPool(
	ctx context.Context,
	config *pdfrender.Config,
	document pdfrender.Document,
) ([]byte, error) {
	requestID, _ := ctx.Value(log.ContextKeyRequestID).(string)

	ctx, timeoutCancel := context.WithTimeout(ctx, render.config.RenderTimeout)
	defer timeoutCancel()

	docID := render.store.Add(document)
	defer render.store.Remove(docID)

	url := render.server.GetURL(ctx, document.TemplateName, docID, requestID)

	params := render.buildParams(config, document)

	waitLifecycleEvent := config.WaitLifecycleEvent

	var pdfResult []byte

	err := render.pool.Borrow(ctx, func(ctx context.Context, pageProvider PageProvider) (err error) {
		ctxWithSpan, span := render.tracer.Start(ctx, "printPDF")
		defer span.End()

		pg := pageProvider.GetPage()
		pdfResult, err = render.printPDF(ctxWithSpan, pg, params, url, waitLifecycleEvent)
		if err != nil {
			return fmt.Errorf("print PDF: %w", err)
		}

		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("borrow: %w", err)
	}

	return pdfResult, nil
}

// Info about render.
func (render *Render) Info(
	ctx context.Context,
) (*pdfrender.RenderInfo, error) {
	ctxWithSpan, span := render.tracer.Start(ctx, "Info")
	defer span.End()

	return render.aggregateInfo(ctxWithSpan)
}

func (render *Render) aggregateInfo(
	ctx context.Context,
) (*pdfrender.RenderInfo, error) {
	metadata := map[string]string{}

	if err := render.pool.Borrow(ctx, func(ctx context.Context, pageProvider PageProvider) error {
		version, err := pageProvider.GetDevTools().Version(ctx)
		if err != nil {
			return fmt.Errorf("version: %w", err)
		}

		metadata["browser"] = version.Browser
		metadata["user_agent"] = version.UserAgent
		metadata["protocol"] = version.Protocol

		return nil
	}); err != nil {
		return nil, fmt.Errorf("borrow: %w", err)
	}

	poolStats := render.pool.Stats()

	return &pdfrender.RenderInfo{
		Metadata: metadata,
		Pool: pdfrender.RenderPoolInfo{
			Stats: pdfrender.RenderPoolStats{
				Active:      poolStats.Active,
				Idle:        poolStats.Idle,
				Destroyed:   poolStats.Destroyed,
				Invalidated: poolStats.Invalidated,
			},
			Type: render.pool.PoolType(),
		},
	}, nil
}
