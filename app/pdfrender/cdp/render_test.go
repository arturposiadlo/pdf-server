package cdp_test

import (
	"bytes"
	"context"
	"errors"
	"math/rand"
	"os"
	"strings"
	"testing"

	"github.com/mafredri/cdp/protocol/page"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	traceSdk "go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/sdk/trace/tracetest"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/cdp"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/server"

	pdfRenderMocks "gitlab.com/c0va23/pdf-server/mocks/pdfrender"
	cdpMocks "gitlab.com/c0va23/pdf-server/mocks/pdfrender/cdp"
	serverMocks "gitlab.com/c0va23/pdf-server/mocks/pdfrender/server"
)

func refValue[T any](value T) *T {
	return &value
}

func isContext(actualValue interface{}) bool {
	_, ok := actualValue.(context.Context)

	return ok
}

func TestBrowserRender(t *testing.T) {
	t.Parallel()

	for _, factoryType := range cdp.FactoryTypeList() {
		testBrowserRenderFactoryType(t, factoryType)
	}
}

func testBrowserRenderFactoryType(t *testing.T, factoryType cdp.FactoryType) {
	t.Helper()

	t.Run(factoryType.String(), func(t *testing.T) {
		t.Parallel()

		logger := log.NewLogger(log.LevelFatal, log.TextFormat)

		ctx := log.ContextWithLogger(context.TODO(), logger)

		renderCtx := context.WithValue(
			log.ContextWithLogger(ctx, logger),
			log.ContextKeyRequestID,
			"requestId",
		)

		poolConfig := cdp.PoolConfig{
			MaxTotal: 2,
			MaxIdle:  1,
		}

		renderConfig := cdp.DefaultRenderConfig()
		runnerConfig := getBrowserRunnerConfig()

		if runnerConfig == nil {
			t.Skip("Browser not found")
		}

		templateName := pdfrender.TemplateName("test")

		bodyHTML := `<html>
			<head>
				<link rel=stylesheet href="assets/styles.css" />
			</head>
			<body>
				<p>Example text</p>
			</body>
		</html>`

		assetName := "styles.css"
		assetData := strings.NewReader(`body {
			color: red;
		}`)

		buildDocument := func(assetStore pdfrender.Assets) pdfrender.Document {
			return pdfrender.Document{
				BodyHTML:     bodyHTML,
				FooterHTML:   nil,
				HeaderHTML:   nil,
				TemplateName: templateName,
				Assets:       assetStore,
			}
		}

		type TestCase struct {
			name   string
			params *pdfrender.Config
		}

		testCases := []TestCase{
			{
				name:   "with default config",
				params: new(pdfrender.Config),
			},
			{
				name: "with mocked config",
				params: &pdfrender.Config{
					Landscape:          refValue(true),
					PrintBackground:    refValue(true),
					Scale:              refValue(2.0),
					PaperWidth:         refValue(20.0),
					PaperHeight:        refValue(20.0),
					MarginTop:          refValue(2.0),
					MarginBottom:       refValue(2.0),
					MarginLeft:         refValue(2.0),
					MarginRight:        refValue(2.0),
					PreferCSSPageSize:  refValue(true),
					WaitLifecycleEvent: nil,
				},
			},
		}

		for _, testCase := range testCases {
			testCase := testCase

			t.Run(testCase.name, func(t *testing.T) {
				t.Parallel()

				defer mockAssertExpectations(t, testCase.params)

				tracerProvider := traceSdk.NewTracerProvider(
					traceSdk.WithSpanProcessor(
						traceSdk.NewSimpleSpanProcessor(
							tracetest.NewNoopExporter(),
						),
					),
				)

				rndSrc := rand.NewSource(1)
				store := server.NewMapStore(rndSrc)

				assetStore := new(pdfRenderMocks.Assets)
				defer assetStore.AssertExpectations(t)

				handler := server.NewHandler(store, logger, tracerProvider)
				server, closer, err := server.ProvideServer(ctx, handler, tracerProvider)
				assert.NoError(t, err)

				defer closer()

				factory, closeFactory, err := makeBrowserFactory(ctx, logger, *runnerConfig, factoryType)
				assert.NoError(t, err)

				defer closeFactory()

				borrower, borrowerClose := cdp.ProvidePoolBorrower(ctx, factory, poolConfig, tracerProvider)
				defer borrowerClose()

				render := cdp.NewRender(
					ctx,
					borrower,
					renderConfig,
					store,
					server,
					tracerProvider,
				)

				assetStore.EXPECT().Open(assetName).Return(assetData, nil)

				document := buildDocument(assetStore)

				dstPDF, err := render.RenderPDF(renderCtx, testCase.params, document)

				assert.True(t, bytes.HasPrefix(dstPDF, []byte("%PDF-1.4")))
				assert.NoError(t, err)
			})
		}
	})
}

type BorrowerMock struct {
	beforeCallbackError error
	afterCallbackError  error
	providedWrapper     cdp.PageProvider
}

func (borrowedMock *BorrowerMock) Borrow(ctx context.Context, callback cdp.BorrowFn) error {
	if borrowedMock.beforeCallbackError != nil {
		return borrowedMock.beforeCallbackError
	}

	if err := callback(ctx, borrowedMock.providedWrapper); err != nil {
		return err
	}

	if borrowedMock.afterCallbackError != nil {
		return borrowedMock.afterCallbackError
	}

	return nil
}

func (*BorrowerMock) PoolType() string {
	return "mock"
}

func (*BorrowerMock) Stats() cdp.BorrowStats {
	return cdp.BorrowStats{
		Active:      1,
		Idle:        0,
		Destroyed:   0,
		Invalidated: 0,
	}
}

func TestBrowserRender_PrintArgs(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	ctx := log.ContextWithLogger(context.TODO(), logger)

	requestID := "requestId"

	renderCtx := context.WithValue(
		log.ContextWithLogger(ctx, logger),
		log.ContextKeyRequestID,
		"requestId",
	)

	renderConfig := cdp.DefaultRenderConfig()

	templateName := pdfrender.TemplateName("test")

	bodyHTML := `<html>
		<head>
			<link rel=stylesheet href="assets/styles.css" />
		</head>
		<body>
			<p>Example text</p>
		</body>
	</html>`

	footerHTML := `<html><head></head><body><p>footer</p></body></html>`
	headerHTML := `<html><head></head><body><p>header</p></body></html>`

	url := "http://localhost:8080/"

	assetsMock := new(pdfRenderMocks.Assets)
	assetsMock.EXPECT().Open(mock.Anything).Return(nil, os.ErrNotExist)

	simpleDocument := pdfrender.Document{
		BodyHTML:     bodyHTML,
		FooterHTML:   nil,
		HeaderHTML:   nil,
		TemplateName: templateName,
		Assets:       assetsMock,
	}

	pdfPayload := []byte("pdf payload")

	type TestCase struct {
		name              string
		params            *pdfrender.Config
		beforeBorrowError error
		afterBorrowError  error
		pageProvider      *cdpMocks.PageProvider
		document          pdfrender.Document
		expectedError     error
		expectedPayload   []byte
		assertMocks       []assertExpectationser
	}

	footerHeaderTestCase := func(name string, header *string, footer *string) TestCase {
		lifecycleEventMock := new(cdpMocks.LifecycleEventClient)
		{
			lifecycleEventMock.EXPECT().Recv().Return(&page.LifecycleEventReply{
				Name: string(pdfrender.DefaultWaitLifecycleEvent),
			}, nil)
			lifecycleEventMock.EXPECT().Close().Return(nil)
		}

		pageMock := new(cdpMocks.Page)
		{
			displayHeaderFooter := true
			pageMock.EXPECT().
				PrintToPDF(mock.MatchedBy(isContext), &page.PrintToPDFArgs{
					DisplayHeaderFooter: &displayHeaderFooter,
					HeaderTemplate:      header,
					FooterTemplate:      footer,
				}).
				Return(&page.PrintToPDFReply{
					Data: pdfPayload,
				}, nil)
			pageMock.EXPECT().
				Navigate(mock.MatchedBy(isContext), &page.NavigateArgs{
					URL: url,
				}).
				Return(nil, nil)

			pageMock.EXPECT().
				LifecycleEvent(mock.MatchedBy(isContext)).
				Return(lifecycleEventMock, nil)
		}

		pageProviderMock := new(cdpMocks.PageProvider)
		pageProviderMock.EXPECT().GetPage().Return(pageMock)

		return TestCase{
			name:         name,
			params:       new(pdfrender.Config),
			pageProvider: pageProviderMock,
			document: pdfrender.Document{
				BodyHTML:     bodyHTML,
				HeaderHTML:   header,
				FooterHTML:   footer,
				TemplateName: templateName,
				Assets:       assetsMock,
			},
			expectedError:   nil,
			expectedPayload: pdfPayload,
			assertMocks: []assertExpectationser{
				pageProviderMock,
				pageMock,
				lifecycleEventMock,
			},
		}
	}

	testCases := []TestCase{
		func() TestCase {
			borrowError := errors.New("before borrow error")

			return TestCase{
				name:              "borrow return error",
				params:            new(pdfrender.Config),
				beforeBorrowError: borrowError,
				document:          simpleDocument,
				expectedError:     borrowError,
				expectedPayload:   nil,
			}
		}(),
		func() TestCase {
			navigateError := errors.New("navigate error")

			lifecycleEventMock := new(cdpMocks.LifecycleEventClient)
			{
				lifecycleEventMock.EXPECT().Close().Return(nil)
			}

			pageMock := new(cdpMocks.Page)
			{
				pageMock.EXPECT().
					Navigate(mock.MatchedBy(isContext), &page.NavigateArgs{
						URL: url,
					}).
					Return(nil, navigateError)

				pageMock.EXPECT().
					LifecycleEvent(mock.MatchedBy(isContext)).
					Return(lifecycleEventMock, nil)
			}

			pageProviderMock := new(cdpMocks.PageProvider)
			pageProviderMock.EXPECT().GetPage().Return(pageMock)

			return TestCase{
				name:            "navigate error",
				params:          new(pdfrender.Config),
				pageProvider:    pageProviderMock,
				document:        simpleDocument,
				expectedError:   navigateError,
				expectedPayload: nil,
				assertMocks: []assertExpectationser{
					pageMock,
					pageProviderMock,
				},
			}
		}(),
		func() TestCase {
			lifecycleEventError := errors.New("lifecycle event error")

			pageMock := new(cdpMocks.Page)
			{
				pageMock.EXPECT().
					LifecycleEvent(mock.MatchedBy(isContext)).
					Return(nil, lifecycleEventError)
			}

			pageProviderMock := new(cdpMocks.PageProvider)
			pageProviderMock.EXPECT().GetPage().Return(pageMock)

			return TestCase{
				name:             "get lifecycle error",
				params:           new(pdfrender.Config),
				pageProvider:     pageProviderMock,
				afterBorrowError: nil,
				document:         simpleDocument,
				expectedError:    lifecycleEventError,
				expectedPayload:  nil,
				assertMocks: []assertExpectationser{
					pageMock,
					pageProviderMock,
				},
			}
		}(),
		func() TestCase {
			receiveEventError := errors.New("receive event error")

			lifecycleEventMock := new(cdpMocks.LifecycleEventClient)
			{
				lifecycleEventMock.EXPECT().Recv().Return(nil, receiveEventError)
				lifecycleEventMock.EXPECT().Close().Return(nil)
			}

			pageMock := new(cdpMocks.Page)
			{
				pageMock.EXPECT().
					Navigate(mock.MatchedBy(isContext), &page.NavigateArgs{
						URL: url,
					}).
					Return(nil, nil)

				pageMock.EXPECT().
					LifecycleEvent(mock.MatchedBy(isContext)).
					Return(lifecycleEventMock, nil)
			}

			pageProviderMock := new(cdpMocks.PageProvider)
			pageProviderMock.EXPECT().GetPage().Return(pageMock)

			return TestCase{
				name:            "receive event error",
				params:          new(pdfrender.Config),
				pageProvider:    pageProviderMock,
				document:        simpleDocument,
				expectedError:   receiveEventError,
				expectedPayload: nil,
				assertMocks: []assertExpectationser{
					lifecycleEventMock,
					pageMock,
					pageProviderMock,
				},
			}
		}(),
		func() TestCase {
			closeLifecycleClientError := errors.New("close lifecycle client error")

			lifecycleEventMock := new(cdpMocks.LifecycleEventClient)
			{
				lifecycleEventMock.EXPECT().Recv().Return(&page.LifecycleEventReply{
					Name: string(pdfrender.DefaultWaitLifecycleEvent),
				}, nil)
				lifecycleEventMock.EXPECT().Close().Return(closeLifecycleClientError)
			}

			pageMock := new(cdpMocks.Page)
			{
				pageMock.EXPECT().
					Navigate(mock.MatchedBy(isContext), &page.NavigateArgs{
						URL: url,
					}).
					Return(nil, nil)

				pageMock.EXPECT().
					LifecycleEvent(mock.MatchedBy(isContext)).
					Return(lifecycleEventMock, nil)

				displayHeaderFooter := false
				pageMock.EXPECT().
					PrintToPDF(mock.MatchedBy(isContext), &page.PrintToPDFArgs{
						DisplayHeaderFooter: &displayHeaderFooter,
					}).
					Return(&page.PrintToPDFReply{
						Data: pdfPayload,
					}, nil)
			}

			pageProviderMock := new(cdpMocks.PageProvider)
			pageProviderMock.EXPECT().GetPage().Return(pageMock)

			return TestCase{
				name:            "close lifecycle client error",
				params:          new(pdfrender.Config),
				pageProvider:    pageProviderMock,
				document:        simpleDocument,
				expectedError:   nil,
				expectedPayload: pdfPayload,
				assertMocks: []assertExpectationser{
					lifecycleEventMock,
					pageMock,
					pageProviderMock,
				},
			}
		}(),
		func() TestCase {
			printError := errors.New("print error")

			lifecycleEventMock := new(cdpMocks.LifecycleEventClient)
			{
				lifecycleEventMock.EXPECT().Recv().Return(&page.LifecycleEventReply{
					Name: string(pdfrender.DefaultWaitLifecycleEvent),
				}, nil)
				lifecycleEventMock.EXPECT().Close().Return(nil)
			}

			pageMock := new(cdpMocks.Page)
			{
				pageMock.EXPECT().
					Navigate(mock.MatchedBy(isContext), &page.NavigateArgs{
						URL: url,
					}).
					Return(nil, nil)

				pageMock.EXPECT().
					LifecycleEvent(mock.MatchedBy(isContext)).
					Return(lifecycleEventMock, nil)

				displayHeaderFooter := false
				pageMock.EXPECT().
					PrintToPDF(mock.MatchedBy(isContext), &page.PrintToPDFArgs{
						DisplayHeaderFooter: &displayHeaderFooter,
					}).
					Return(nil, printError)
			}

			pageProviderMock := new(cdpMocks.PageProvider)
			pageProviderMock.EXPECT().GetPage().Return(pageMock)

			return TestCase{
				name:            "print error",
				params:          new(pdfrender.Config),
				pageProvider:    pageProviderMock,
				document:        simpleDocument,
				expectedError:   printError,
				expectedPayload: nil,
				assertMocks: []assertExpectationser{
					lifecycleEventMock,
					pageMock,
					pageProviderMock,
				},
			}
		}(),
		func() TestCase {
			lifecycleEventMock := new(cdpMocks.LifecycleEventClient)
			{
				lifecycleEventMock.EXPECT().Recv().Return(&page.LifecycleEventReply{
					Name: string(pdfrender.DefaultWaitLifecycleEvent),
				}, nil)
				lifecycleEventMock.EXPECT().Close().Return(nil)
			}

			pageMock := new(cdpMocks.Page)
			{
				displayHeaderFooter := false
				pageMock.EXPECT().
					PrintToPDF(mock.MatchedBy(isContext), &page.PrintToPDFArgs{
						DisplayHeaderFooter: &displayHeaderFooter,
					}).
					Return(&page.PrintToPDFReply{
						Data: pdfPayload,
					}, nil)
				pageMock.EXPECT().
					Navigate(mock.MatchedBy(isContext), &page.NavigateArgs{
						URL: url,
					}).
					Return(nil, nil)

				pageMock.EXPECT().
					LifecycleEvent(mock.MatchedBy(isContext)).
					Return(lifecycleEventMock, nil)
			}

			pageProviderMock := new(cdpMocks.PageProvider)
			pageProviderMock.EXPECT().GetPage().Return(pageMock)

			afterBorrowError := errors.New("after borrow error")

			return TestCase{
				name:             "after borrow error",
				params:           new(pdfrender.Config),
				pageProvider:     pageProviderMock,
				afterBorrowError: afterBorrowError,
				document:         simpleDocument,
				expectedError:    afterBorrowError,
				expectedPayload:  nil,
				assertMocks: []assertExpectationser{
					lifecycleEventMock,
					pageMock,
					pageProviderMock,
				},
			}
		}(),
		func() TestCase {
			lifecycleEventMock := new(cdpMocks.LifecycleEventClient)
			{
				lifecycleEventMock.EXPECT().Recv().Return(&page.LifecycleEventReply{
					Name: string(pdfrender.DefaultWaitLifecycleEvent),
				}, nil)
				lifecycleEventMock.EXPECT().Close().Return(nil)
			}

			pageMock := new(cdpMocks.Page)
			{
				displayHeaderFooter := false
				pageMock.EXPECT().
					PrintToPDF(mock.MatchedBy(isContext), &page.PrintToPDFArgs{
						DisplayHeaderFooter: &displayHeaderFooter,
					}).
					Return(&page.PrintToPDFReply{
						Data: pdfPayload,
					}, nil)
				pageMock.EXPECT().
					Navigate(mock.MatchedBy(isContext), &page.NavigateArgs{
						URL: url,
					}).
					Return(nil, nil)

				pageMock.EXPECT().
					LifecycleEvent(mock.MatchedBy(isContext)).
					Return(lifecycleEventMock, nil)
			}

			pageProviderMock := new(cdpMocks.PageProvider)
			pageProviderMock.EXPECT().GetPage().Return(pageMock)

			return TestCase{
				name:            "with default config",
				params:          new(pdfrender.Config),
				pageProvider:    pageProviderMock,
				document:        simpleDocument,
				expectedError:   nil,
				expectedPayload: pdfPayload,
				assertMocks: []assertExpectationser{
					lifecycleEventMock,
					pageMock,
					pageProviderMock,
				},
			}
		}(),
		footerHeaderTestCase("with header", &headerHTML, nil),
		footerHeaderTestCase("with footer", nil, &footerHTML),
		footerHeaderTestCase("with header and footer", &headerHTML, &footerHTML),
		func() TestCase {
			printBackground := true
			landscape := true
			scale := 2.0
			paperWidth := 20.0
			paperHeight := 20.0
			marginTop := 2.0
			marginBottom := 2.0
			marginLeft := 2.0
			marginRight := 2.0
			preferCSSPageSize := true

			paramsMock := pdfrender.Config{
				Landscape:          refValue(landscape),
				PrintBackground:    refValue(printBackground),
				Scale:              refValue(scale),
				PaperWidth:         refValue(paperWidth),
				PaperHeight:        refValue(paperHeight),
				MarginTop:          refValue(marginTop),
				MarginBottom:       refValue(marginBottom),
				MarginLeft:         refValue(marginLeft),
				MarginRight:        refValue(marginRight),
				PreferCSSPageSize:  refValue(preferCSSPageSize),
				WaitLifecycleEvent: nil,
			}

			lifecycleEventMock := new(cdpMocks.LifecycleEventClient)
			{
				lifecycleEventMock.EXPECT().Recv().Return(&page.LifecycleEventReply{
					Name: string(pdfrender.DefaultWaitLifecycleEvent),
				}, nil)
				lifecycleEventMock.EXPECT().Close().Return(nil)
			}

			pageMock := new(cdpMocks.Page)
			{
				displayHeaderFooter := false
				pageMock.EXPECT().PrintToPDF(mock.MatchedBy(isContext), &page.PrintToPDFArgs{
					Landscape:           &landscape,
					DisplayHeaderFooter: &displayHeaderFooter,
					PrintBackground:     &printBackground,
					Scale:               &scale,
					PaperWidth:          &paperWidth,
					PaperHeight:         &paperHeight,
					MarginTop:           &marginTop,
					MarginBottom:        &marginBottom,
					MarginLeft:          &marginLeft,
					MarginRight:         &marginRight,
					PreferCSSPageSize:   &preferCSSPageSize,
				}).Return(&page.PrintToPDFReply{
					Data: pdfPayload,
				}, nil)
				pageMock.EXPECT().
					Navigate(mock.MatchedBy(isContext), &page.NavigateArgs{
						URL: url,
					}).
					Return(nil, nil)

				pageMock.EXPECT().
					LifecycleEvent(mock.MatchedBy(isContext)).
					Return(lifecycleEventMock, nil)
			}

			pageProviderMock := new(cdpMocks.PageProvider)
			pageProviderMock.EXPECT().GetPage().Return(pageMock)

			return TestCase{
				name:              "with mocked config",
				beforeBorrowError: nil,
				pageProvider:      pageProviderMock,
				params:            &paramsMock,
				document:          simpleDocument,
				expectedError:     nil,
				expectedPayload:   pdfPayload,
				assertMocks: []assertExpectationser{
					pageMock,
					lifecycleEventMock,
					pageProviderMock,
				},
			}
		}(),
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			defer mockAssertExpectations(t, testCase.params)

			storeMock := new(serverMocks.Store)
			defer storeMock.AssertExpectations(t)

			documentID := server.DocumentID(rand.Int63())
			storeMock.EXPECT().Add(testCase.document).Return(documentID)
			storeMock.EXPECT().Remove(documentID).Return()

			serverMock := new(serverMocks.Server)
			defer serverMock.AssertExpectations(t)

			serverMock.EXPECT().
				GetURL(mock.MatchedBy(isContext), templateName, documentID, requestID).
				Return(url)

			borrowerMock := &BorrowerMock{
				beforeCallbackError: testCase.beforeBorrowError,
				providedWrapper:     testCase.pageProvider,
				afterCallbackError:  testCase.afterBorrowError,
			}

			tracerProvider := trace.NewNoopTracerProvider()

			render := cdp.NewRender(
				ctx,
				borrowerMock,
				renderConfig,
				storeMock,
				serverMock,
				tracerProvider,
			)

			dstPDF, err := render.RenderPDF(renderCtx, testCase.params, testCase.document)

			assert.ErrorIs(t, err, testCase.expectedError)
			assert.Equal(t, testCase.expectedPayload, dstPDF)

			for _, assertMock := range testCase.assertMocks {
				assertMock.AssertExpectations(t)
			}
		})
	}
}
