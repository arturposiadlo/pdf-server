package cdp

import (
	"context"
	"fmt"
	"net/http"
	"time"

	pool "github.com/jolestar/go-commons-pool/v2"
	"github.com/mafredri/cdp"
	"github.com/mafredri/cdp/devtool"
	"github.com/mafredri/cdp/protocol/page"
	"github.com/mafredri/cdp/rpcc"
	"github.com/urfave/cli/v2"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/utils/bindings"
)

// FactoryType define used factory type (new browser instance or new tab into browser).
type FactoryType string

// runner - start new browser instance.
// runner - open browser tab.
const (
	RunnerFactoryType = FactoryType("runner")
	PageFactoryType   = FactoryType("page")
)

// Set value from flags.
func (factoryType *FactoryType) Set(value string) error {
	for _, expectedFactoryType := range FactoryTypeList() {
		if expectedFactoryType.String() == value {
			*factoryType = expectedFactoryType

			return nil
		}
	}

	return NewUnexpectedFactoryTypeError(value)
}

func (factoryType FactoryType) String() string {
	return string(factoryType)
}

// FactoryTypeList return all valid factory types.
func FactoryTypeList() []FactoryType {
	return []FactoryType{
		RunnerFactoryType,
		PageFactoryType,
	}
}

// UnexpectedFactoryTypeError returns by (*FactoryType).Set when value is unexpected.
type UnexpectedFactoryTypeError struct {
	value string
}

// NewUnexpectedFactoryTypeError constructor.
func NewUnexpectedFactoryTypeError(value string) *UnexpectedFactoryTypeError {
	return &UnexpectedFactoryTypeError{
		value: value,
	}
}

func (err *UnexpectedFactoryTypeError) Error() string {
	return fmt.Sprintf(
		"unexpected factory type: %s (expected values: %s)",
		err.value,
		FactoryTypeList(),
	)
}

// FactoryConfig for browser factory pool.
type FactoryConfig struct {
	MaxGetPageAttempts int
	GetPageRetryDelay  time.Duration
	MaxUsageCount      int
	RPCBufferSize      int
	FactoryType        FactoryType
}

// DefaultFactoryConfig provider.
//
// revive:disable:add-constant
//
//nolint:gomnd
func DefaultFactoryConfig() FactoryConfig {
	return FactoryConfig{
		MaxGetPageAttempts: 20,
		GetPageRetryDelay:  10 * time.Microsecond,
		MaxUsageCount:      1000,
		RPCBufferSize:      8096,
		FactoryType:        RunnerFactoryType,
	}
}

// BindFlags bind factory config flags.
func (config *FactoryConfig) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.IntFlag{
			Name:        "factory-max-get-page-attempts",
			Usage:       "Maximum attempts to get default chromium page",
			EnvVars:     []string{"FACTORY_MAX_GET_PAGE_ATTEMPTS"},
			Value:       config.MaxGetPageAttempts,
			Destination: &config.MaxGetPageAttempts,
			Hidden:      false,
		},
		&cli.GenericFlag{
			Name:    "factory-get-page-retry-delay",
			Usage:   "Delay between retry get page",
			EnvVars: []string{"FACTORY_GET_PAGE_RETRY_DELAY"},
			Value:   bindings.NewDuration(&config.GetPageRetryDelay),
			Hidden:  false,
		},
		&cli.IntFlag{
			Name:        "factory-max-usage-count",
			Usage:       "Maximum chromium page usage count",
			EnvVars:     []string{"FACTORY_MAX_USAGE_COUNT"},
			Value:       config.MaxUsageCount,
			Destination: &config.MaxUsageCount,
			Hidden:      false,
		},
		&cli.IntFlag{
			Name:        "factory-rpc-buffer-size",
			Usage:       "Maximum RPC buffer size",
			EnvVars:     []string{"FACTORY_RPC_BUFFER_SIZE"},
			Value:       config.RPCBufferSize,
			Destination: &config.RPCBufferSize,
			Hidden:      false,
		},
		&cli.GenericFlag{
			Name:    "factory-type",
			Usage:   fmt.Sprintf("Factory type. Allowed values: %s", FactoryTypeList()),
			EnvVars: []string{"FACTORY_TYPE"},
			Value:   &config.FactoryType,
			Hidden:  false,
		},
	}
}

// PageProvider define *CdpWrapper interface.
type PageProvider interface {
	GetPage() Page
	GetDevTools() *devtool.DevTools
}

// LifecycleEventClient alias of page.LifecycleEventClient for mocks.
type LifecycleEventClient interface {
	page.LifecycleEventClient
}

// PageWrapper argument for borrower.
type PageWrapper struct {
	runner   Runner
	devTools *devtool.DevTools
	target   *devtool.Target
	conn     *rpcc.Conn
	page     Page

	usageCount int
}

// GetPage returns wrapped page object.
func (wrapper *PageWrapper) GetPage() Page {
	return wrapper.page
}

// GetDevTools returns wrapped *devTools.DevTools object.
func (wrapper *PageWrapper) GetDevTools() *devtool.DevTools {
	return wrapper.devTools
}

// PageWrapperFactory define factory for CDpWrapper instances.
type PageWrapperFactory interface {
	Make(context.Context) (*PageWrapper, error)
	Destroy(context.Context, *PageWrapper) error
	Close(context.Context) error
	Type() string
}

// TypedFactory wrap pool.PooledObjectFactory and provide Type() method.
type TypedFactory interface {
	pool.PooledObjectFactory
	Type() string
}

// Factory make CDP client.
type Factory struct {
	logger            log.Logger
	runnerBuilder     RunnerBuilder
	config            FactoryConfig
	cdpWrapperFactory PageWrapperFactory
	tracer            trace.Tracer
}

// NewFactory constructor.
func NewFactory(
	ctx context.Context,
	logger log.Logger,
	runnerFactory RunnerBuilder,
	config FactoryConfig,
	tracerProvider trace.TracerProvider,
) (*Factory, error) {
	namedLogger := logger.WithField(log.LoggerField, "browser-factory")

	var (
		cdpWrapperFactory PageWrapperFactory
		err               error
	)

	logger.Infof("Create %s factory", config.FactoryType)

	switch config.FactoryType {
	case RunnerFactoryType:
		cdpWrapperFactory = NewRunnerCdpWrapperFactory(
			logger,
			runnerFactory,
			config,
			tracerProvider,
		)
	case PageFactoryType:
		cdpWrapperFactory, err = NewPageCdpWrapperFactory(
			ctx,
			logger,
			runnerFactory,
			config,
			tracerProvider,
		)
		if err != nil {
			return nil, fmt.Errorf("new page factory: %w", err)
		}
	}

	tracer := tracerProvider.Tracer("CdpFactory")

	return &Factory{
		logger:            namedLogger,
		runnerBuilder:     runnerFactory,
		config:            config,
		cdpWrapperFactory: cdpWrapperFactory,
		tracer:            tracer,
	}, nil
}

// Close factory (stop runner).
func (factory *Factory) Close(ctx context.Context) error {
	if err := factory.cdpWrapperFactory.Close(ctx); err != nil {
		return fmt.Errorf("wrapper factory close: %w", err)
	}

	return nil
}

// ProvideFactory like NewFactory but return safe close function.
func ProvideFactory(
	ctx context.Context,
	logger log.Logger,
	runnerFactory RunnerBuilder,
	config FactoryConfig,
	tracerProvider trace.TracerProvider,
) (*Factory, func(), error) {
	factory, err := NewFactory(
		ctx,
		logger,
		runnerFactory,
		config,
		tracerProvider,
	)
	if err != nil {
		return nil, nil, fmt.Errorf("new factory: %w", err)
	}

	safeClose := func() {
		if err := factory.Close(ctx); err != nil {
			logger.WithError(err)
		}
	}

	return factory, safeClose, nil
}

func buildDevTools(cdtURL string, tracerProvider trace.TracerProvider) *devtool.DevTools {
	transport := otelhttp.NewTransport(
		http.DefaultTransport,
		otelhttp.WithTracerProvider(tracerProvider),
		otelhttp.WithSpanNameFormatter(func(operation string, r *http.Request) string {
			return fmt.Sprintf("CDT(%s)", r.URL.Path)
		}),
	)
	httpClient := &http.Client{
		Transport: transport,
	}

	return devtool.New(cdtURL, devtool.WithClient(httpClient))
}

// RunnerCdpWrapperFactory make and destroy runner (browser instance) CdpWrapper's.
type RunnerCdpWrapperFactory struct {
	logger         log.Logger
	runnerBuilder  RunnerBuilder
	config         FactoryConfig
	tracerProvider trace.TracerProvider
	tracer         trace.Tracer
}

// NewRunnerCdpWrapperFactory constructor.
func NewRunnerCdpWrapperFactory(
	logger log.Logger,
	runnerBuilder RunnerBuilder,
	config FactoryConfig,
	tracerProvider trace.TracerProvider,
) *RunnerCdpWrapperFactory {
	return &RunnerCdpWrapperFactory{
		logger:         logger,
		runnerBuilder:  runnerBuilder,
		config:         config,
		tracerProvider: tracerProvider,
		tracer:         tracerProvider.Tracer("RunnerCdpWrapperFactory"),
	}
}

// Close factory (stop runner).
func (*RunnerCdpWrapperFactory) Close(context.Context) error {
	return nil
}

// Make CDP wrapper instance.
func (factory *RunnerCdpWrapperFactory) Make(ctx context.Context) (*PageWrapper, error) {
	ctxWithSpan, span := factory.tracer.Start(ctx, "Make")
	defer span.End()

	return factory.createPageWrapper(ctxWithSpan)
}

// revive:disable:cognitive-complexity
func (factory *RunnerCdpWrapperFactory) createPageWrapper(
	ctx context.Context,
) (*PageWrapper, error) {
	factory.logger.Debug("Browser run")

	runner, err := factory.runnerBuilder.BuildRunner()
	if err != nil {
		return nil, fmt.Errorf("build runner: %w", err)
	}

	cdtURL, err := runner.Run(ctx)
	if nil != err {
		return nil, fmt.Errorf("run: %w", err)
	}

	factory.logger.Debug("Browser started")

	defer func() {
		if err == nil {
			return
		}

		factory.logger.Warn("Close broken browser")

		if stopErr := runner.Stop(ctx); nil != stopErr {
			factory.logger.WithError(err).Error("Browser stop error")
		}
	}()

	devTools := buildDevTools(cdtURL, factory.tracerProvider)

	target, err := factory.getPageWithRetries(ctx, devTools)
	if nil != err {
		return nil, fmt.Errorf("get page: %w", err)
	}

	conn, err := rpcc.DialContext(
		ctx,
		target.WebSocketDebuggerURL,
		rpcc.WithWriteBufferSize(factory.config.RPCBufferSize),
	)
	if nil != err {
		return nil, fmt.Errorf("dial page websocket debugger: %w", err)
	}

	factory.logger.Debug("Connection dialed")

	cdpClient := cdp.NewClient(conn)

	pg := TracePage(cdpClient.Page, factory.tracerProvider)

	factory.logger.Debug("Client Created")

	return &PageWrapper{
		runner:   runner,
		devTools: devTools,
		target:   target,
		conn:     conn,
		page:     pg,
	}, nil
}

// Type of factory.
func (*RunnerCdpWrapperFactory) Type() string {
	return RunnerFactoryType.String()
}

// MakeObject create CDP client.
func (factory *Factory) MakeObject(ctx context.Context) (*pool.PooledObject, error) {
	ctxWithSpan, span := factory.tracer.Start(ctx, "MakeObject")
	defer span.End()

	return factory.createPageObject(ctxWithSpan)
}

func (factory *Factory) createPageObject(ctx context.Context) (*pool.PooledObject, error) {
	cdpWrapper, err := factory.cdpWrapperFactory.Make(ctx)
	if err != nil {
		return nil, fmt.Errorf("make: %w", err)
	}

	return pool.NewPooledObject(cdpWrapper), nil
}

func (factory *RunnerCdpWrapperFactory) getPageWithRetries(
	ctx context.Context,
	devTools *devtool.DevTools,
) (
	target *devtool.Target,
	err error,
) {
	ctxWithSpan, span := factory.tracer.Start(ctx, "getPageWithRetries")
	defer span.End()

	for attempt := 0; attempt < factory.config.MaxGetPageAttempts; attempt++ {
		factory.logger.Debug("Get page")

		target, err = devTools.Get(ctxWithSpan, devtool.Page)

		if target != nil {
			factory.logger.Debug("Page fetched")

			break
		}

		factory.logger.Debug("Page not found")
		time.Sleep(factory.config.GetPageRetryDelay)
	}

	if err != nil {
		return nil, fmt.Errorf("get page: %w", err)
	}

	return target, nil
}

// DestroyObject destroy CDP client.
func (factory *Factory) DestroyObject(ctx context.Context, object *pool.PooledObject) (
	err error,
) {
	ctxWithSpan, span := factory.tracer.Start(ctx, "DestroyObject")
	defer span.End()

	return factory.destroyPageObject(ctxWithSpan, object)
}

func (factory *Factory) destroyPageObject(ctx context.Context, object *pool.PooledObject) (
	err error,
) {
	client := object.Object.(*PageWrapper)

	if err := factory.cdpWrapperFactory.Destroy(ctx, client); err != nil {
		return fmt.Errorf("destroy: %w", err)
	}

	return nil
}

// Destroy CdpWrapper instance.
func (factory *RunnerCdpWrapperFactory) Destroy(ctx context.Context, client *PageWrapper) (
	err error,
) {
	ctxWithSpan, span := factory.tracer.Start(ctx, "Destroy")
	defer span.End()

	return factory.destroyPageObject(ctxWithSpan, client)
}

func (factory *RunnerCdpWrapperFactory) destroyPageObject(
	ctx context.Context,
	client *PageWrapper,
) (
	err error,
) {
	factory.logger.Debug("Destroy page")

	// Always stop browser
	defer func() {
		if stopErr := client.runner.Stop(ctx); nil != stopErr {
			factory.logger.WithError(stopErr).Error("Browser close error")

			if err == nil {
				err = stopErr
			}
		} else {
			factory.logger.Info("Browser destroyed")
		}
	}()

	if err = client.conn.Close(); err != nil {
		return fmt.Errorf("close connection: %w", err)
	}

	factory.logger.Debug("Connection closed")

	if err = client.devTools.Close(ctx, client.target); err != nil {
		return fmt.Errorf("close devtool: %w", err)
	}

	factory.logger.Debug("Devtool closed")

	return nil
}

// ValidateObject validate CDP client (reload page).
func (factory *Factory) ValidateObject(
	ctx context.Context,
	object *pool.PooledObject,
) bool {
	ctxWithSpan, span := factory.tracer.Start(ctx, "ValidateObject")
	defer span.End()

	return factory.validatePageObject(ctxWithSpan, object)
}

func (factory *Factory) validatePageObject(
	ctx context.Context,
	object *pool.PooledObject,
) bool {
	factory.logger.Debug("Validate page")

	client := object.Object.(*PageWrapper)

	if client.usageCount > factory.config.MaxUsageCount {
		factory.logger.Warn("Page invalidated: page used too many times")

		return false
	}

	if err := client.page.Reload(ctx, &page.ReloadArgs{}); nil != err {
		factory.logger.WithError(err).Error("Validation error")

		return false
	}

	factory.logger.Debug("Page validation success")

	return true
}

// ActivateObject enable page.
func (factory *Factory) ActivateObject(
	ctx context.Context,
	object *pool.PooledObject,
) error {
	ctxWithSpan, span := factory.tracer.Start(ctx, "ActivateObject")
	defer span.End()

	return factory.activatePageObject(ctxWithSpan, object)
}

func (factory *Factory) activatePageObject(
	ctx context.Context,
	object *pool.PooledObject,
) error {
	factory.logger.Debug("Activate page")

	client := object.Object.(*PageWrapper)

	pg := client.page

	if err := pg.Enable(ctx); nil != err {
		return fmt.Errorf("enable page: %w", err)
	}

	factory.logger.Debug("Page enabled")

	if err := pg.SetLifecycleEventsEnabled(ctx, &page.SetLifecycleEventsEnabledArgs{
		Enabled: true,
	}); nil != err {
		return fmt.Errorf("set lifecycle events enabled: %w", err)
	}

	factory.logger.Debug("Set lifecycle events enabled success")

	return nil
}

// PassivateObject disable page.
func (factory *Factory) PassivateObject(
	ctx context.Context,
	object *pool.PooledObject,
) error {
	ctxWithSpan, span := factory.tracer.Start(ctx, "PassivateObject")
	defer span.End()

	return factory.passivatePageObject(ctxWithSpan, object)
}

func (factory *Factory) passivatePageObject(
	ctx context.Context,
	object *pool.PooledObject,
) error {
	factory.logger.Debug("Passivate page")

	client := object.Object.(*PageWrapper)
	pg := client.page

	if err := pg.SetLifecycleEventsEnabled(ctx, &page.SetLifecycleEventsEnabledArgs{
		Enabled: false,
	}); nil != err {
		return fmt.Errorf("set lifecycle events disabled: %w", err)
	}

	factory.logger.Debug("Set lifecycle events disabled success")

	if err := pg.Disable(ctx); nil != err {
		return fmt.Errorf("disable page: %w", err)
	}

	factory.logger.Debug("Page disabled")

	client.usageCount++

	return nil
}

// Type of factory.
func (factory *Factory) Type() string {
	return factory.cdpWrapperFactory.Type()
}

// PageCdpWrapperFactory create page (tab) on browser instance.
type PageCdpWrapperFactory struct {
	logger         log.Logger
	runner         Runner
	devTools       *devtool.DevTools
	config         FactoryConfig
	tracer         trace.Tracer
	tracerProvider trace.TracerProvider
}

// NewPageCdpWrapperFactory constructor.
func NewPageCdpWrapperFactory(
	ctx context.Context,
	logger log.Logger,
	runnerBuilder RunnerBuilder,
	config FactoryConfig,
	tracerProvider trace.TracerProvider,
) (*PageCdpWrapperFactory, error) {
	logger.Debug("Browser run")

	runner, err := runnerBuilder.BuildRunner()
	if err != nil {
		return nil, fmt.Errorf("build runner: %w", err)
	}

	cdtURL, err := runner.Run(ctx)
	if nil != err {
		return nil, fmt.Errorf("run: %w", err)
	}

	devTools := buildDevTools(cdtURL, tracerProvider)

	tracer := tracerProvider.Tracer("PageCdpWrapperFactory")

	return &PageCdpWrapperFactory{
		logger:         logger,
		config:         config,
		runner:         runner,
		devTools:       devTools,
		tracer:         tracer,
		tracerProvider: tracerProvider,
	}, nil
}

// Close factory (stop browser instance).
func (factory *PageCdpWrapperFactory) Close(ctx context.Context) error {
	if err := factory.runner.Stop(ctx); err != nil {
		factory.logger.WithError(err).Error("Browser stop error")

		return fmt.Errorf("stop runner: %w", err)
	}

	return nil
}

// Make create new page and open debugger endpoint.
func (factory *PageCdpWrapperFactory) Make(ctx context.Context) (*PageWrapper, error) {
	ctxWithSpan, span := factory.tracer.Start(ctx, "Make")
	defer span.End()

	return factory.createPage(ctxWithSpan)
}

func (factory *PageCdpWrapperFactory) createPage(ctx context.Context) (*PageWrapper, error) {
	factory.logger.Info("Create page")

	target, err := factory.devTools.CreateURL(ctx, "")
	if nil != err {
		return nil, fmt.Errorf("create page: %w", err)
	}

	conn, err := rpcc.DialContext(
		ctx,
		target.WebSocketDebuggerURL,
		rpcc.WithWriteBufferSize(factory.config.RPCBufferSize),
	)
	if nil != err {
		return nil, fmt.Errorf("dial page websocket debugger: %w", err)
	}

	factory.logger.Debug("Connection dialed")

	cdpClient := cdp.NewClient(conn)

	pg := TracePage(cdpClient.Page, factory.tracerProvider)

	factory.logger.Debug("Client Created")

	return &PageWrapper{
		runner:   factory.runner,
		devTools: factory.devTools,
		target:   target,
		conn:     conn,
		page:     pg,
	}, nil
}

// Destroy page.
func (factory *PageCdpWrapperFactory) Destroy(ctx context.Context, client *PageWrapper) (
	err error,
) {
	ctxWithSpan, span := factory.tracer.Start(ctx, "Destroy")
	defer span.End()

	return factory.destroyPage(ctxWithSpan, client)
}

func (factory *PageCdpWrapperFactory) destroyPage(ctx context.Context, client *PageWrapper) (
	err error,
) {
	factory.logger.Debug("Destroy page")

	if err = client.conn.Close(); err != nil {
		return fmt.Errorf("close connection: %w", err)
	}

	factory.logger.Debug("Connection closed")

	if err = client.devTools.Close(ctx, client.target); err != nil {
		return fmt.Errorf("close devtool: %w", err)
	}

	factory.logger.Debug("Devtool closed")

	return nil
}

// Type of factory.
func (*PageCdpWrapperFactory) Type() string {
	return PageFactoryType.String()
}
