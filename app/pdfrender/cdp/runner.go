package cdp

import (
	"bufio"
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"net/url"
	"os"
	"os/exec"
	"strings"
	"syscall"

	"github.com/urfave/cli/v2"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/utils/bindings"
)

// DevToolsURLNotFoundError browser not return DevTools URL.
type DevToolsURLNotFoundError struct{}

// NewDevToolsURLNotFoundError constructor.
func NewDevToolsURLNotFoundError() *DevToolsURLNotFoundError {
	return new(DevToolsURLNotFoundError)
}

func (*DevToolsURLNotFoundError) Error() string {
	return "devTools WebSocket url not found"
}

// Runner for browser instance.
type Runner interface {
	Run(context.Context) (wsURL string, err error)
	Stop(context.Context) error
}

// RunnerConfig have command and additions args for run browser.
type RunnerConfig struct {
	Command string
	Args    []string
}

// DefaultRunnerConfig default RunnerConfig.
func DefaultRunnerConfig() RunnerConfig {
	return RunnerConfig{
		Command: "chromium",
		Args: []string{
			"--single-process",
			"--no-zygote",
			"--disable-audio-input",
			"--disable-audio-output",
			"--headless",
			"--hide-scrollbars",
			"--mute-audio",
			"--no-default-browser-check",
			"--no-first-run",
			"--no-sandbox",
			"--safebrowsing-disable-auto-update",
			"--metrics-recording-only",
			"--disable-background-networking",
			"--disable-background-timer-throttling",
			"--disable-breakpad",
			"--disable-extensions",
			"--disable-file-system",
			"--disable-databases",
			"--disable-default-apps",
			"--disable-dev-shm-usage",
			"--disable-gpu",
			"--disable-ipc-flooding-protection",
			"--disable-sync",
			"--disable-logging",
			"--disable-notifications",
			"--disable-permissions-api",
			"--disable-popup-blocking",
			"--disable-prompt-on-repost",
			"--disable-renderer-backgrounding",
			"--disable-translate",
		},
	}
}

// BindFlags bind RunnerConfig to cli flags.
func (config *RunnerConfig) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "browser-command",
			EnvVars:     []string{"BROWSER_COMMAND", "CHROMIUM_COMMAND"},
			Aliases:     []string{"chromium-command"},
			Value:       config.Command,
			Destination: &config.Command,
			Hidden:      false,
			Usage:       "Browser run command",
		},
		&cli.GenericFlag{
			Name:    "browser-args",
			Aliases: []string{"chromium-args"},
			EnvVars: []string{"BROWSER_ARGS", "CHROMIUM_ARGS"},
			Value:   bindings.NewStringSlice(&config.Args),
			Usage: fmt.Sprintf(
				`Arguments passed to browser command. Argument separator: ",". Allowed substitutions: %v.`,
				ListAllowedVariables(),
			),
			Hidden: false,
		},
	}
}

// BrowserData passed to browser command line arguments.
type BrowserData struct {
	tmpID  string
	tmpDir string
}

// BrowserRunner run browser with command and args, and parse debugger WebSocket URL
// from stdout.
type BrowserRunner struct {
	cmd         *exec.Cmd
	logger      log.Logger
	browserData BrowserData
	tracer      trace.Tracer
}

const idSize = 12

func genID() (string, error) {
	buf := make([]byte, idSize)

	if _, err := rand.Read(buf); err != nil {
		return "", fmt.Errorf("read random: %w", err)
	}

	return hex.EncodeToString(buf), nil
}

// Substitution variables.
const (
	TempDirVariable = "%{TMP_DIR}"
	TempIDVariable  = "%{TMP_ID}"
)

// ListAllowedVariables return all known variables.
func ListAllowedVariables() []string {
	return []string{
		TempDirVariable,
		TempIDVariable,
	}
}

func buildBrowserArgs(browserData BrowserData, configArgs []string) []string {
	defaultArgs := []string{
		"--remote-debugging-port=0",
		"about:blank",
	}

	args := make([]string, 0, len(configArgs)+len(defaultArgs))

	for _, arg := range configArgs {
		arg = strings.Replace(arg, TempDirVariable, browserData.tmpDir, 1)
		arg = strings.Replace(arg, TempIDVariable, browserData.tmpID, 1)

		args = append(args, arg)
	}

	args = append(args, defaultArgs...)

	return args
}

func buildBrowserData() (BrowserData, error) {
	tmpID, err := genID()
	if err != nil {
		return BrowserData{}, fmt.Errorf("gen id: %w", err)
	}

	tmpDir, err := os.MkdirTemp("", fmt.Sprintf("%s-browser", tmpID))
	if err != nil {
		return BrowserData{}, fmt.Errorf("make tmp dir: %w", err)
	}

	return BrowserData{
		tmpID:  tmpID,
		tmpDir: tmpDir,
	}, nil
}

// Delete temporal browser data.
func (data *BrowserData) Delete() error {
	if err := os.RemoveAll(data.tmpDir); err != nil {
		return fmt.Errorf("remove tmp dir: %w", err)
	}

	return nil
}

// NewBrowserRunner constructor.
func NewBrowserRunner(
	config RunnerConfig,
	logger log.Logger,
	tracerProvider trace.TracerProvider,
) (*BrowserRunner, error) {
	runnerLogger := logger.WithField(log.LoggerField, "cmd-runner")

	browserData, err := buildBrowserData()
	if err != nil {
		return nil, fmt.Errorf("build browser data: %w", err)
	}

	runnerLogger.WithField(log.FieldBrowserData, browserData).Debugf("Browser data for command args")

	args := buildBrowserArgs(browserData, config.Args)

	runnerLogger.Debugf("Command %s with args %v", config.Command, args)

	cmd := exec.Command(config.Command, args...) //nolint:gosec

	tracer := tracerProvider.Tracer("BrowserRunner")

	return &BrowserRunner{
		cmd:         cmd,
		logger:      runnerLogger,
		browserData: browserData,
		tracer:      tracer,
	}, nil
}

// Run browser process and scan stdout for WebSocket URL.
func (runner *BrowserRunner) Run(ctx context.Context) (devToolURL string, err error) {
	ctxWithSpan, span := runner.tracer.Start(ctx, "Run")
	defer span.End()

	return runner.performRun(ctxWithSpan)
}

// revive:disable:cognitive-complexity
func (runner *BrowserRunner) performRun(ctx context.Context) (devToolURL string, err error) {
	stdout, err := runner.cmd.StdoutPipe()
	if nil != err {
		return "", fmt.Errorf("get stdout pipe error: %w", err)
	}

	runner.cmd.Stderr = runner.cmd.Stdout

	err = runner.cmd.Start()
	if nil != err {
		return "", fmt.Errorf("start browser error: %w", err)
	}

	rawBrowserWSURL, err := runner.extractBrowserWSURL(ctx, stdout)
	if err != nil {
		return "", fmt.Errorf("extract browser URL: %w", err)
	}

	browserWSURL, err := url.Parse(rawBrowserWSURL)
	if nil != err {
		return "", fmt.Errorf("parse url: %w", err)
	}

	devToolURL = fmt.Sprintf("http://localhost:%s", browserWSURL.Port())

	runner.logger.WithField(log.FieldListenAddr, devToolURL).Debug("Browser listener")

	return devToolURL, nil
}

func (runner *BrowserRunner) extractBrowserWSURL(
	ctx context.Context,
	stdout io.Reader,
) (string, error) {
	_, span := runner.tracer.Start(ctx, "extractBrowserWSURL")
	defer span.End()

	browserWSURLChan := make(chan string)

	go func() {
		defer close(browserWSURLChan)

		scanner := bufio.NewScanner(stdout)
		scanner.Split(bufio.ScanLines)

		var browserWSURL string

		var browserWSURLFound bool

		for {
			if !scanner.Scan() {
				return
			}

			line := scanner.Text()

			runner.logger.WithField(log.FieldBrowserOut, line).Debug("Browser out")

			if browserWSURLFound {
				continue
			}

			_, scanErr := fmt.Sscanf(line, "DevTools listening on %s", &browserWSURL)
			if scanErr != nil {
				continue
			}

			browserWSURLChan <- browserWSURL

			browserWSURLFound = true
		}
	}()

	browserWSURL, ok := <-browserWSURLChan
	if !ok {
		return "", NewDevToolsURLNotFoundError()
	}

	return browserWSURL, nil
}

// Stop browser process.
func (runner *BrowserRunner) Stop(ctx context.Context) error {
	_, span := runner.tracer.Start(ctx, "Stop")
	defer span.End()

	logger := runner.logger

	logger.Debug("Browser stop beginning")

	process := runner.cmd.Process
	if err := process.Signal(syscall.SIGTERM); err != nil {
		return fmt.Errorf("browser termination: %w", err)
	}

	state, err := process.Wait()
	if err != nil {
		return fmt.Errorf("wait browser process: %w", err)
	}

	logger.WithField(log.FieldBrowserState, state).Info("Browser termination state")

	if err := runner.browserData.Delete(); err != nil {
		return fmt.Errorf("delete browser data: %w", err)
	}

	logger.WithField(log.FieldBrowserData, runner.browserData).Debug("Browser data deleted")

	return nil
}

// RunnerBuilder build runners.
type RunnerBuilder interface {
	BuildRunner() (Runner, error)
}

// CmdRunnerBuilder default implementation of RunnerFactory.
type CmdRunnerBuilder struct {
	runnerConfig   RunnerConfig
	logger         log.Logger
	tracerProvider trace.TracerProvider
}

// NewCmdRunnerBuilder constructor.
func NewCmdRunnerBuilder(
	runnerConfig RunnerConfig,
	logger log.Logger,
	tracerProvider trace.TracerProvider,
) *CmdRunnerBuilder {
	return &CmdRunnerBuilder{
		runnerConfig:   runnerConfig,
		logger:         logger,
		tracerProvider: tracerProvider,
	}
}

// BuildRunner with runner config.
func (runnerBuilder *CmdRunnerBuilder) BuildRunner() (Runner, error) {
	browser, err := NewBrowserRunner(
		runnerBuilder.runnerConfig,
		runnerBuilder.logger,
		runnerBuilder.tracerProvider,
	)
	if err != nil {
		return nil, fmt.Errorf("new browser runner: %w", err)
	}

	return browser, nil
}
