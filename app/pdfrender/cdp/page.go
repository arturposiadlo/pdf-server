package cdp

import (
	"context"
	"fmt"

	"github.com/mafredri/cdp"
	"github.com/mafredri/cdp/protocol/page"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

// Page alias of cdp.Page for mocks.
type Page interface {
	Enable(context.Context) error
	Disable(context.Context) error
	Reload(context.Context, *page.ReloadArgs) error
	SetLifecycleEventsEnabled(context.Context, *page.SetLifecycleEventsEnabledArgs) error
	LifecycleEvent(context.Context) (page.LifecycleEventClient, error)
	PrintToPDF(context.Context, *page.PrintToPDFArgs) (*page.PrintToPDFReply, error)
	Navigate(context.Context, *page.NavigateArgs) (*page.NavigateReply, error)
}

// TracedPage wrap cdp.Page with tracing.
type TracedPage struct {
	page   cdp.Page
	tracer trace.Tracer
}

// TracePage wrap cdp.Page.
func TracePage(cdpPage cdp.Page, tracerProvider trace.TracerProvider) *TracedPage {
	tracer := tracerProvider.Tracer("TracedPage")

	return &TracedPage{
		page:   cdpPage,
		tracer: tracer,
	}
}

// Enable wraps cdp.Page.Enable with span.
func (tracedPage *TracedPage) Enable(ctx context.Context) error {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "Enable")
	defer span.End()

	if err := tracedPage.page.Enable(ctxWithSpan); err != nil {
		span.SetStatus(codes.Error, err.Error())

		return tracedPage.wrapTraceError(err)
	}

	return nil
}

// Disable wraps cdp.Page.Disable with span.
func (tracedPage *TracedPage) Disable(ctx context.Context) error {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "Disable")
	defer span.End()

	if err := tracedPage.page.Disable(ctxWithSpan); err != nil {
		span.SetStatus(codes.Error, err.Error())

		return tracedPage.wrapTraceError(err)
	}

	return nil
}

// Reload wraps cdp.Page.Reload with span.
func (tracedPage *TracedPage) Reload(ctx context.Context, args *page.ReloadArgs) error {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "Reload")
	defer span.End()

	if err := tracedPage.page.Reload(ctxWithSpan, args); err != nil {
		span.SetStatus(codes.Error, err.Error())

		return tracedPage.wrapTraceError(err)
	}

	return nil
}

// SetLifecycleEventsEnabled wraps cdp.Page.SetLifecycleEventsEnabled with span.
func (tracedPage *TracedPage) SetLifecycleEventsEnabled(
	ctx context.Context,
	args *page.SetLifecycleEventsEnabledArgs,
) error {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "SetLifecycleEventsEnabled")
	defer span.End()

	if err := tracedPage.page.SetLifecycleEventsEnabled(ctxWithSpan, args); err != nil {
		span.SetStatus(codes.Error, err.Error())

		return tracedPage.wrapTraceError(err)
	}

	return nil
}

// LifecycleEvent wraps cdp.Page.LifecycleEvent with span.
func (tracedPage *TracedPage) LifecycleEvent(
	ctx context.Context,
) (page.LifecycleEventClient, error) {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "LifecycleEvent")
	defer span.End()

	client, err := tracedPage.page.LifecycleEvent(ctxWithSpan)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, tracedPage.wrapTraceError(err)
	}

	return client, nil
}

// PrintToPDF wraps cdp.Page.PrintToPDF with span.
func (tracedPage *TracedPage) PrintToPDF(
	ctx context.Context,
	args *page.PrintToPDFArgs,
) (*page.PrintToPDFReply, error) {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "PrintToPDF")
	defer span.End()

	reply, err := tracedPage.page.PrintToPDF(ctxWithSpan, args)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, tracedPage.wrapTraceError(err)
	}

	return reply, nil
}

// Navigate wraps cdp.Page.Navigate with span.
func (tracedPage *TracedPage) Navigate(
	ctx context.Context,
	args *page.NavigateArgs,
) (*page.NavigateReply, error) {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "Navigate")
	defer span.End()

	reply, err := tracedPage.page.Navigate(ctxWithSpan, args)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, tracedPage.wrapTraceError(err)
	}

	return reply, nil
}

func (*TracedPage) wrapTraceError(err error) error {
	return fmt.Errorf("trace: %w", err)
}
