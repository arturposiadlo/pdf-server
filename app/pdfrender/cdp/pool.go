package cdp

import (
	"context"
	"fmt"
	"time"

	pool "github.com/jolestar/go-commons-pool/v2"
	"github.com/urfave/cli/v2"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/utils/bindings"
)

const forceEvictionPolicyName = "forceEvictionPolicy"

const loggerName = "browser-pool"

type forceEvictionPolicy struct{}

// Evict implement github.com/jolestar/go-commons-pool/v2.EvictionPolicy interface.
//
// revive:disable:unused-parameter
func (*forceEvictionPolicy) Evict(
	config *pool.EvictionConfig,
	underTest *pool.PooledObject,
	idleCount int,
) bool {
	return true
}

//nolint:gochecknoinits
func init() {
	evictionPolicy := new(forceEvictionPolicy)
	pool.RegistryEvictionPolicy(forceEvictionPolicyName, evictionPolicy)
}

// BorrowFn called when borrowed CDP client.
type BorrowFn func(context.Context, PageProvider) error

// BorrowStats returned by Borrower.Stats().
type BorrowStats struct {
	Active      int
	Idle        int
	Destroyed   int
	Invalidated int
}

// Borrower of CDP clients.
type Borrower interface {
	Borrow(context.Context, BorrowFn) error
	Stats() BorrowStats
	PoolType() string
}

// PoolConfig for PDFRender object pool.
type PoolConfig struct {
	MaxTotal int
	MinIdle  int
	MaxIdle  int

	TimeBetweenEviction time.Duration
}

// DefaultPoolConfig provider.
func DefaultPoolConfig() PoolConfig {
	return PoolConfig{
		MaxTotal:            1,
		MinIdle:             1,
		MaxIdle:             1,
		TimeBetweenEviction: time.Hour,
	}
}

// BindFlags bind flags to config.
func (config PoolConfig) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.IntFlag{
			Name:        "render-pool-max-total",
			EnvVars:     []string{"RENDER_POOL_MAX_TOTAL"},
			Value:       config.MaxTotal,
			Destination: &config.MaxTotal,
			Hidden:      false,
			Usage:       "Max idle renders into pool",
		},
		&cli.IntFlag{
			Name:        "render-pool-min-idle",
			EnvVars:     []string{"RENDER_POOL_MIN_IDLE"},
			Value:       config.MinIdle,
			Destination: &config.MinIdle,
			Hidden:      false,
			Usage:       "Min idle renders into pool",
		},
		&cli.IntFlag{
			Name:        "render-pool-max-idle",
			EnvVars:     []string{"RENDER_POOL_MAX_IDLE"},
			Value:       config.MaxIdle,
			Destination: &config.MaxIdle,
			Hidden:      false,
			Usage:       "Max idle renders into pool",
		},
		&cli.GenericFlag{
			Name:    "render-pool-time-between-eviction",
			EnvVars: []string{"RENDER_POOL_TIME_BETWEEN_EVICTION"},
			Value:   bindings.NewDuration(&config.TimeBetweenEviction),
			Hidden:  false,
			Usage:   "Time duration between render pool eviction",
		},
	}
}

// PoolBorrower borrow CDP client from pool.
type PoolBorrower struct {
	clientPool *pool.ObjectPool
	logger     log.Logger
	config     PoolConfig
	poolType   string
	tracer     trace.Tracer
}

// ProvidePoolBorrower build borrower pool and start it.
func ProvidePoolBorrower(
	ctx context.Context,
	clientFactory TypedFactory,
	config PoolConfig,
	tracerProvider trace.TracerProvider,
) (*PoolBorrower, func()) {
	logger := log.LoggerFromContext(ctx, loggerName)

	poolConfig := pool.ObjectPoolConfig{
		MaxTotal:               config.MaxTotal,
		MinIdle:                config.MinIdle,
		MaxIdle:                config.MaxIdle,
		EvitionContext:         ctx,
		NumTestsPerEvictionRun: config.MaxIdle,
		BlockWhenExhausted:     true,
		EvictionPolicyName:     forceEvictionPolicyName,
		// Invalid dead objects
		TestOnReturn: true,
	}
	clientPool := pool.NewObjectPool(
		ctx,
		clientFactory,
		&poolConfig,
	)

	clientPool.Config.TimeBetweenEvictionRuns = config.TimeBetweenEviction
	clientPool.StartEvictor()

	tracer := tracerProvider.Tracer("PoolBorrower")

	poolBorrower := &PoolBorrower{
		clientPool: clientPool,
		logger:     logger,
		config:     config,
		poolType:   clientFactory.Type(),
		tracer:     tracer,
	}

	closer := func() {
		logger.Info("Stop pool evictor")
		poolBorrower.clientPool.Close(ctx)
	}

	return poolBorrower, closer
}

// Borrow CDP client.
func (poolBorrower *PoolBorrower) Borrow(ctx context.Context, fn BorrowFn) (err error) {
	logger := log.LoggerFromContext(ctx, loggerName)

	ctxWithSpan, span := poolBorrower.tracer.Start(ctx, "Borrow")
	defer span.End()

	clientObject, err := poolBorrower.borrowObject(ctxWithSpan)
	if nil != err {
		return fmt.Errorf("borrow object: %w", err)
	}

	defer func() {
		returnErr := poolBorrower.returnObject(ctxWithSpan, clientObject)
		if returnErr != nil {
			logger.WithError(returnErr).Error("Return context error")

			if err == nil {
				err = fmt.Errorf("return context: %w", returnErr)
			}
		} else {
			logger.Debug("Return context success")
		}
	}()

	client := clientObject.(*PageWrapper)

	logger.Debug("PDF render borrowed success")

	if err := fn(ctxWithSpan, client); err != nil {
		return fmt.Errorf("fn: %w", err)
	}

	return nil
}

func (poolBorrower *PoolBorrower) borrowObject(ctx context.Context) (interface{}, error) {
	ctxWithSpan, span := poolBorrower.tracer.Start(ctx, "borrowObject")
	defer span.End()

	clientObject, err := poolBorrower.clientPool.BorrowObject(ctxWithSpan)
	if nil != err {
		return nil, fmt.Errorf("span: %w", err)
	}

	return clientObject, nil
}

func (poolBorrower *PoolBorrower) returnObject(ctx context.Context, object interface{}) error {
	ctxWithSpan, span := poolBorrower.tracer.Start(ctx, "returnObject")
	defer span.End()

	err := poolBorrower.clientPool.ReturnObject(ctxWithSpan, object)
	if nil != err {
		return fmt.Errorf("span: %w", err)
	}

	return nil
}

// Stats from internal pool.
func (poolBorrower *PoolBorrower) Stats() BorrowStats {
	return BorrowStats{
		Active:      poolBorrower.clientPool.GetNumActive(),
		Idle:        poolBorrower.clientPool.GetNumIdle(),
		Destroyed:   poolBorrower.clientPool.GetDestroyedCount(),
		Invalidated: poolBorrower.clientPool.GetDestroyedByBorrowValidationCount(),
	}
}

// PoolType return type of internal pool.
func (poolBorrower *PoolBorrower) PoolType() string {
	return poolBorrower.poolType
}
