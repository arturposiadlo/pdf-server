package cdp_test

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/cdp"
)

const defaultBrowserCommand = "chromium"

func getBrowserCommand() string {
	command, found := os.LookupEnv("BROWSER_COMMAND")
	if !found {
		command = defaultBrowserCommand
	}

	return command
}

func getBrowserArguments() []string {
	argsLine, found := os.LookupEnv("BROWSER_ARGS")
	if !found {
		return []string{
			"--headless",
			"--disable-gpu",
			"--no-default-browser-check",
			"--no-first-run",
			"--no-sandbox",
		}
	}

	args := strings.Split(argsLine, " ")

	return args
}

func getBrowserRunnerConfig() *cdp.RunnerConfig {
	command := getBrowserCommand()

	if _, err := exec.LookPath(command); err != nil {
		return nil
	}

	return &cdp.RunnerConfig{
		Command: command,
		Args:    getBrowserArguments(),
	}
}

type assertExpectationser interface {
	AssertExpectations(mock.TestingT) bool
}

func mockAssertExpectations(t *testing.T, maybeMock interface{}) {
	t.Helper()

	if mockedObject, ok := maybeMock.(assertExpectationser); ok {
		mockedObject.AssertExpectations(t)
	}
}

func makeBrowserFactory(
	ctx context.Context,
	logger log.Logger,
	config cdp.RunnerConfig,
	factoryType cdp.FactoryType,
) (*cdp.Factory, func(), error) {
	traceProvider := trace.NewNoopTracerProvider()

	runnerBuilder := cdp.NewCmdRunnerBuilder(config, logger, traceProvider)
	factoryConfig := cdp.FactoryConfig{
		MaxGetPageAttempts: 5,
		GetPageRetryDelay:  100 * time.Millisecond,
		FactoryType:        factoryType,
	}

	factory, closeFactory, err := cdp.ProvideFactory(
		ctx,
		logger,
		runnerBuilder,
		factoryConfig,
		traceProvider,
	)
	if err != nil {
		return nil, nil, fmt.Errorf("new factory: %w", err)
	}

	return factory, closeFactory, nil
}
