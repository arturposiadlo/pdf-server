package server_test

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/server"

	pdfRenderMocks "gitlab.com/c0va23/pdf-server/mocks/pdfrender"
	pdfRenderServerMocks "gitlab.com/c0va23/pdf-server/mocks/pdfrender/server"
)

func TestHandlers_Get(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelWarn, log.TextFormat)
	ctx := log.ContextWithLogger(context.TODO(), logger)

	templateName := "demo_template"
	documentID := 123

	validRequestPath := fmt.Sprintf("/templates/%s/%d", templateName, documentID)
	validDocumentHTML := "valid html"

	tracerProvider := trace.NewNoopTracerProvider()

	type TestCase struct {
		name              string
		requestPath       string
		requestDocumentID string
		getDocumentResult *pdfrender.Document
		getDocumentCalled bool
		getDocumentFound  bool
		expectedError     error
		expectedBody      []byte
	}

	testCases := []TestCase{
		{
			name:              "invalid document ID",
			requestPath:       fmt.Sprintf("/templates/%s/invalid-id", templateName),
			requestDocumentID: "invalid-id",
			getDocumentCalled: false,
			getDocumentResult: nil,
			getDocumentFound:  false,
			expectedError:     echo.ErrBadRequest,
			expectedBody:      nil,
		},
		{
			name:              "document ID not found",
			requestPath:       validRequestPath,
			requestDocumentID: fmt.Sprint(documentID),
			getDocumentCalled: true,
			getDocumentResult: nil,
			getDocumentFound:  false,
			expectedError:     echo.ErrNotFound,
			expectedBody:      nil,
		},
		{
			name:              "document ID found",
			requestPath:       validRequestPath,
			requestDocumentID: fmt.Sprint(documentID),
			getDocumentCalled: true,
			getDocumentResult: &pdfrender.Document{
				BodyHTML:     validDocumentHTML,
				TemplateName: pdfrender.TemplateName(templateName),
				Assets:       nil,
			},
			getDocumentFound: true,
			expectedError:    nil,
			expectedBody:     []byte(validDocumentHTML),
		},
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			documentStore := new(pdfRenderServerMocks.Store)
			defer documentStore.AssertExpectations(t)

			handler := server.NewHandler(documentStore, logger, tracerProvider)

			req, err := http.NewRequestWithContext(ctx, http.MethodGet, testCase.requestPath, nil)
			assert.NoError(t, err)

			responseRecorder := httptest.NewRecorder()

			if testCase.getDocumentCalled {
				documentStore.
					EXPECT().
					Get(server.DocumentID(documentID)).
					Return(testCase.getDocumentResult, testCase.getDocumentResult != nil)
			}

			echoCtx := echo.New().NewContext(req, responseRecorder)
			echoCtx.SetPath("/templates/:template_name/:document_id")
			echoCtx.SetParamNames("template_name", "document_id")
			echoCtx.SetParamValues(templateName, testCase.requestDocumentID)

			err = handler.Get(echoCtx)

			assert.ErrorIs(t, err, testCase.expectedError)

			assert.Equal(t, testCase.expectedBody, responseRecorder.Body.Bytes())
		})
	}
}

func TestHandlers_GetAsset(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelWarn, log.TextFormat)
	ctx := log.ContextWithLogger(context.TODO(), logger)

	tracerProvider := trace.NewNoopTracerProvider()

	templateName := "demo_template"
	assetPath := "styles.css"

	documentID := server.DocumentID(123)

	requestPath := fmt.Sprintf("/templates/%s/assets/%s", templateName, assetPath)

	type TestCase struct {
		name              string
		getDocumentResult *pdfrender.Document
		expectedError     error
		expectedBody      []byte
	}

	testCases := []TestCase{
		{
			name:              "document not found",
			getDocumentResult: nil,
			expectedError:     echo.ErrNotFound,
			expectedBody:      nil,
		},
		{
			name: "asset not found",
			getDocumentResult: &pdfrender.Document{
				TemplateName: pdfrender.TemplateName(templateName),
				Assets: func() *pdfRenderMocks.Assets {
					assetsMock := new(pdfRenderMocks.Assets)
					assetsMock.EXPECT().Open(assetPath).Return(nil, os.ErrNotExist)

					return assetsMock
				}(),
			},
			expectedError: echo.ErrNotFound,
			expectedBody:  nil,
		},
		{
			name: "unexpected open asset error",
			getDocumentResult: &pdfrender.Document{
				TemplateName: pdfrender.TemplateName(templateName),
				Assets: func() *pdfRenderMocks.Assets {
					assetsMock := new(pdfRenderMocks.Assets)

					unexpectedError := errors.New("unexpected open asset error")
					assetsMock.EXPECT().Open(assetPath).Return(nil, unexpectedError)

					return assetsMock
				}(),
			},
			expectedError: echo.ErrInternalServerError,
			expectedBody:  nil,
		},
		func() TestCase {
			assetPayload := []byte("valid asset")

			return TestCase{
				name: "open asset successful",
				getDocumentResult: &pdfrender.Document{
					TemplateName: pdfrender.TemplateName(templateName),
					Assets: func() *pdfRenderMocks.Assets {
						assetsMock := new(pdfRenderMocks.Assets)
						assetsMock.EXPECT().Open(assetPath).Return(
							bytes.NewBuffer(assetPayload),
							nil,
						)

						return assetsMock
					}(),
				},
				expectedError: nil,
				expectedBody:  assetPayload,
			}
		}(),
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			documentStore := new(pdfRenderServerMocks.Store)
			defer documentStore.AssertExpectations(t)

			handler := server.NewHandler(documentStore, logger, tracerProvider)

			req, err := http.NewRequestWithContext(ctx, http.MethodGet, requestPath, nil)
			assert.NoError(t, err)

			responseRecorder := httptest.NewRecorder()

			documentStore.
				EXPECT().
				Get(documentID).
				Return(testCase.getDocumentResult, testCase.getDocumentResult != nil)

			echoCtx := echo.New().NewContext(req, responseRecorder)
			echoCtx.SetPath("/templates/:template_name/documents/:document_id/assets/*")
			echoCtx.SetParamNames("template_name", "document_id", "*")
			echoCtx.SetParamValues(templateName, documentID.String(), assetPath)

			err = handler.GetAsset(echoCtx)

			assert.ErrorIs(t, err, testCase.expectedError)

			assert.Equal(t, testCase.expectedBody, responseRecorder.Body.Bytes())
		})
	}
}
