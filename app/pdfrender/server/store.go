package server

import (
	"fmt"
	"math/rand"
	"sync"

	"gitlab.com/c0va23/pdf-server/app/pdfrender"
)

// DocumentID is store key.
type DocumentID uint64

func (id DocumentID) String() string {
	return fmt.Sprintf("%d", id)
}

// Store interface for Handler.
type Store interface {
	Add(pdfrender.Document) DocumentID
	Remove(DocumentID)
	Get(DocumentID) (*pdfrender.Document, bool)
}

// MapStore is default implementation for Store.
type MapStore struct {
	sync.RWMutex
	data map[DocumentID]pdfrender.Document
	rand *rand.Rand
}

// NewMapStore constructor.
func NewMapStore(randSrc rand.Source) *MapStore {
	return &MapStore{
		data: map[DocumentID]pdfrender.Document{},
		rand: rand.New(randSrc),
	}
}

// Add document to store.
func (mapStore *MapStore) Add(document pdfrender.Document) DocumentID {
	mapStore.Lock()
	defer mapStore.Unlock()

	documentID := DocumentID(mapStore.rand.Uint64())

	mapStore.data[documentID] = document

	return documentID
}

// Remove document by ID.
func (mapStore *MapStore) Remove(documentID DocumentID) {
	mapStore.Lock()
	defer mapStore.Unlock()

	delete(mapStore.data, documentID)
}

// Get document by ID.
func (mapStore *MapStore) Get(documentID DocumentID) (*pdfrender.Document, bool) {
	mapStore.RLock()
	defer mapStore.RUnlock()

	document, found := mapStore.data[documentID]

	return &document, found
}
