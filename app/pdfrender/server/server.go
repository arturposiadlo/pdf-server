package server

import (
	"context"
	"encoding/hex"
	"errors"
	"fmt"
	"mime"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"

	echo "github.com/labstack/echo/v4"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	outerServer "gitlab.com/c0va23/pdf-server/app/server"
	"gitlab.com/c0va23/pdf-server/app/utils/httputil"
)

const (
	requestIDParam = "request_id"
	traceID        = "trace_id"
	spanID         = "span_id"
	traceState     = "trace_state"
	traceFlags     = "trace_flags"

	decimalNumber = 10
	bits64        = 64
)

// Handler of CDP render Server.
type Handler struct {
	documentsStore Store
	logger         log.Logger
	tracer         trace.Tracer
}

// NewHandler constructor.
func NewHandler(
	documentsStore Store,
	logger log.Logger,
	tracerProvider trace.TracerProvider,
) *Handler {
	tracer := tracerProvider.Tracer("InnerHandler")

	return &Handler{
		documentsStore: documentsStore,
		logger:         logger,
		tracer:         tracer,
	}
}

const (
	templateNameParam = "template_name"
	documentIDParam   = "document_id"
	assetPathParam    = "asset_path"
)

// Get document HTML.
func (handler *Handler) Get(c echo.Context) error {
	templateName := c.Param(templateNameParam)
	documentIDValue := c.Param(documentIDParam)
	requestID := c.QueryParam(requestIDParam)

	_, span := handler.tracer.Start(c.Request().Context(), "Get", trace.WithAttributes(
		attribute.String(templateNameParam, templateName),
		attribute.String(requestIDParam, requestID),
	))
	defer span.End()

	logger := handler.logger.
		WithField(log.FieldRequestID, requestID).
		WithField(log.FieldTemplateName, templateName)

	documentID, err := handler.parseDocumentID(documentIDValue)
	if nil != err {
		logger.WithError(err).Error("Invalid document ID")
		span.SetStatus(codes.Error, fmt.Sprintf("invalid document id: %s", err.Error()))

		return echo.ErrBadRequest
	}

	logger = logger.WithField(log.FieldDocumentID, documentID)

	document, found := handler.documentsStore.Get(documentID)
	if !found {
		logger.Warn("Document not found")
		span.SetStatus(codes.Error, "Document not found")

		return echo.ErrNotFound
	}

	logger.Debug("Document found")

	httputil.SetContentLength(c, []byte(document.BodyHTML))

	return c.HTML(http.StatusOK, document.BodyHTML)
}

func (*Handler) parseDocumentID(documentIDValue string) (DocumentID, error) {
	documentID, err := strconv.ParseUint(documentIDValue, decimalNumber, bits64)
	if err != nil {
		return DocumentID(0), fmt.Errorf("invalid document id: %w", err)
	}

	return DocumentID(documentID), nil
}

const cacheControlMaxAge = 31536000 // One year

// GetAsset handler.
//
// revive:disable:function-length // TODO: change type of documentID to string
func (handler *Handler) GetAsset(c echo.Context) error {
	templateName := c.Param(templateNameParam)
	documentIDValue := c.Param(documentIDParam)
	path := c.Param("*")

	_, span := handler.tracer.Start(c.Request().Context(), "GetAsset", trace.WithAttributes(
		attribute.String(templateNameParam, templateName),
		attribute.String(assetPathParam, path),
	))
	defer span.End()

	logger := handler.logger.WithField(log.FieldTemplateName, templateName).
		WithField(log.FieldDocumentID, documentIDValue)

	documentID, err := handler.parseDocumentID(documentIDValue)
	if err != nil {
		logger.WithError(err).Error("Invalid document ID")
		span.SetStatus(codes.Error, fmt.Sprintf("invalid document id: %s", err.Error()))

		return echo.ErrBadRequest
	}

	template, found := handler.documentsStore.Get(documentID)
	if !found {
		logger.Warn("Template not found")
		span.SetStatus(codes.Error, "Template not found")

		return echo.ErrNotFound
	}

	logger.Debug("Document found")

	logger = logger.WithField(log.FieldAssetPath, path)

	file, err := template.Assets.Open(path)

	switch {
	case err == nil:
		ext := filepath.Ext(path)
		contentType := mime.TypeByExtension(ext)

		logger.WithField(log.FieldContentType, contentType).Debug("Asset loaded")

		cacheControl := fmt.Sprintf("max-age=%d", cacheControlMaxAge)

		c.Response().Header().Add(
			"Cache-Control",
			cacheControl,
		)

		return c.Stream(http.StatusOK, contentType, file)
	case os.IsNotExist(err):
		logger.Warn("Asset not found")

		return echo.ErrNotFound
	default:
		logger.WithError(err).Error("File open error")

		return echo.ErrInternalServerError
	}
}

// Server provide GetURL.
type Server interface {
	GetURL(
		ctx context.Context,
		templateName pdfrender.TemplateName,
		documentHash DocumentID,
		requestID string,
	) string
}

const (
	getRoute = "templates.get"

	browserRenderServerLogger = "browser-render-server"
)

// EchoServer for browser render.
type EchoServer struct {
	httpServer *http.Server
	router     *echo.Echo
	listener   net.Listener
	logger     log.Logger
	tracer     trace.Tracer
}

// ProvideServer create, start server and return closer function.
func ProvideServer(
	ctx context.Context,
	handler *Handler,
	tracerProvider trace.TracerProvider,
) (*EchoServer, func(), error) {
	e := echo.New()

	logger := log.LoggerFromContext(ctx, browserRenderServerLogger)

	//nolint:contextcheck
	e.Use(outerServer.RequestIDLogger(logger))
	//nolint:contextcheck
	e.Use(TraceMiddleware(logger, tracerProvider))

	e.GET(
		"/templates/:template_name/documents/:document_id/page",
		handler.Get,
	).Name = getRoute
	e.GET(
		"/templates/:template_name/documents/:document_id/assets/*",
		handler.GetAsset,
	)

	logger.Debug("Start internal server")

	listener, err := net.Listen("tcp", "127.0.0.1:")
	if nil != err {
		return nil, nil, fmt.Errorf("listen: %w", err)
	}

	logger.WithField(log.FieldListenAddr, listener.Addr()).
		Info("Internal server listener is ready")

	tracer := tracerProvider.Tracer("EchoServer")

	server := &EchoServer{
		httpServer: &http.Server{
			Handler: e,
		},
		router:   e,
		logger:   logger,
		listener: listener,
		tracer:   tracer,
	}

	go server.startHTTPServer(listener)

	closer := func() {
		server.Close(ctx)
	}

	return server, closer, nil
}

// GetURL on internal server by templateName and documentID.
func (server *EchoServer) GetURL(
	ctx context.Context,
	templateName pdfrender.TemplateName,
	documentHash DocumentID,
	requestID string,
) string {
	logger := log.LoggerFromContext(ctx, browserRenderServerLogger)

	path := server.router.Reverse(getRoute, templateName, documentHash)

	spanContext := trace.SpanContextFromContext(ctx)

	query := url.Values{
		requestIDParam: []string{requestID},
		traceID:        []string{spanContext.TraceID().String()},
		spanID:         []string{spanContext.SpanID().String()},
		traceState:     []string{spanContext.TraceState().String()},
		traceFlags:     []string{spanContext.TraceFlags().String()},
	}
	documentURL := url.URL{
		Host:     server.listener.Addr().String(),
		Scheme:   "http",
		Path:     path,
		RawQuery: query.Encode(),
	}

	logger.WithField(log.FieldDocumentURL, documentURL.String()).
		Debug("Document URL")

	return documentURL.String()
}

func (server *EchoServer) startHTTPServer(listener net.Listener) {
	if err := server.httpServer.Serve(listener); errors.Is(err, http.ErrServerClosed) {
		server.logger.Debug("Internal server closed")
	} else {
		server.logger.WithError(err).Error("Server close error")
	}
}

// Close HTTP server.
func (server *EchoServer) Close(ctx context.Context) {
	server.logger.Debug("Stop internal server")

	if err := server.httpServer.Shutdown(ctx); err != nil {
		server.logger.WithError(err).Info("Internal server shutdown error")
	} else {
		server.logger.Info("Internal server shutdown is successful")
	}
}

// TraceMiddleware extract parent span from query parameters or from referer header.
// And start new span.
func TraceMiddleware(logger log.Logger, tracerProvider trace.TracerProvider) echo.MiddlewareFunc {
	tracer := tracerProvider.Tracer("TraceMiddleware")

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			request := c.Request()

			spanContext, err := extractSpanContextFromRequest(request)
			if err != nil {
				logger.WithError(err).Debug("extract span context error")
			}

			ctxWithSpanContext := trace.ContextWithRemoteSpanContext(request.Context(), spanContext)

			ctxWithSpan, span := tracer.Start(
				ctxWithSpanContext,
				c.Path(),
				trace.WithAttributes(semconv.HTTPClientAttributesFromHTTPRequest(request)...),
			)
			defer span.End()

			c.SetRequest(request.WithContext(ctxWithSpan))

			if err := next(c); err != nil {
				span.SetStatus(codes.Error, err.Error())

				return err
			}

			span.SetAttributes(semconv.HTTPAttributesFromHTTPStatusCode(c.Response().Status)...)

			return nil
		}
	}
}

// TraceIDNotProvidedError returns from extractSpanContextFromRequest.
type TraceIDNotProvidedError struct{}

// NewTraceIDNotProvidedError construct new TraceIDNotProvidedError.
func NewTraceIDNotProvidedError() *TraceIDNotProvidedError {
	return &TraceIDNotProvidedError{}
}

// Error implements error interface.
func (*TraceIDNotProvidedError) Error() string {
	return fmt.Sprintf("query parameter %s not provided", traceID)
}

const (
	refererHeader = "referer"
)

func emptySpanContext() trace.SpanContext {
	return trace.SpanContext{}
}

func extractSpanContextFromRequest(request *http.Request) (trace.SpanContext, error) {
	query := request.URL.Query()

	spanContext, err := extractSpanContextFromQuery(query)
	if err == nil {
		return spanContext, nil
	}

	if notTraceIDErr := new(TraceIDNotProvidedError); !errors.As(err, &notTraceIDErr) {
		return spanContext, fmt.Errorf("from query: %w", err)
	}

	spanContext, err = extractSpanContextFromReferer(request.Header.Get(refererHeader))
	if err != nil {
		return emptySpanContext(), fmt.Errorf("from referer: %w", err)
	}

	return spanContext, nil
}

func extractSpanContextFromReferer(referer string) (trace.SpanContext, error) {
	refererURL, err := url.Parse(referer)
	if err != nil {
		return emptySpanContext(), fmt.Errorf("parse url: %w", err)
	}

	spanContext, err := extractSpanContextFromQuery(refererURL.Query())
	if err != nil {
		return emptySpanContext(), fmt.Errorf("from query: %w", err)
	}

	return spanContext, nil
}

func extractSpanContextFromQuery(query url.Values) (trace.SpanContext, error) {
	emptySpanContext := trace.SpanContext{}

	traceIDstring := query.Get(traceID)
	spanIDstring := query.Get(spanID)
	traceStateString := query.Get(traceState)
	traceFlagsString := query.Get(traceFlags)

	if traceIDstring == "" {
		return emptySpanContext, NewTraceIDNotProvidedError()
	}

	traceIDValue, err := trace.TraceIDFromHex(traceIDstring)
	if err != nil {
		return emptySpanContext, fmt.Errorf("parse trace ID (%s): %w", traceIDstring, err)
	}

	spanIDValue, err := trace.SpanIDFromHex(spanIDstring)
	if err != nil {
		return emptySpanContext, fmt.Errorf("parse span ID (%s): %w", spanIDstring, err)
	}

	traceStateValue, err := trace.ParseTraceState(traceStateString)
	if err != nil {
		return emptySpanContext, fmt.Errorf("parse trace state (%s): %w", traceStateString, err)
	}

	traceFlagsParsed, err := hex.DecodeString(traceFlagsString)
	if err != nil {
		return emptySpanContext, fmt.Errorf("parse trace flags (%s): %w", traceFlagsString, err)
	}

	traceFlagsValue := trace.TraceFlags(traceFlagsParsed[0] & byte(trace.FlagsSampled))

	spanContext := trace.NewSpanContext(trace.SpanContextConfig{
		TraceID:    traceIDValue,
		SpanID:     spanIDValue,
		TraceState: traceStateValue,
		TraceFlags: traceFlagsValue,
	})

	return spanContext, nil
}
