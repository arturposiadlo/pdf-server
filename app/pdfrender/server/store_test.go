package server_test

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/server"
)

func TestMapStore(t *testing.T) {
	t.Parallel()

	randomSource := rand.NewSource(1)
	mapStore := server.NewMapStore(randomSource)

	emptyDocumentID := 0

	firstDocument := pdfrender.Document{
		BodyHTML:     "doc 1",
		TemplateName: "doc1",
		Assets:       nil,
	}

	secondDocument := pdfrender.Document{
		BodyHTML:     "doc 1",
		TemplateName: "doc1",
		Assets:       nil,
	}

	firstDocumentID := mapStore.Add(firstDocument)
	assert.NotEqual(t, emptyDocumentID, firstDocumentID)

	secondDocumentID := mapStore.Add(secondDocument)
	assert.NotEqual(t, emptyDocumentID, secondDocumentID)

	foundedFirstDocument, found := mapStore.Get(firstDocumentID)
	assert.True(t, found)
	assert.Equal(t, &firstDocument, foundedFirstDocument)

	foundedSecondDocument, found := mapStore.Get(secondDocumentID)
	assert.True(t, found)
	assert.Equal(t, &secondDocument, foundedSecondDocument)

	mapStore.Remove(secondDocumentID)

	_, found = mapStore.Get(secondDocumentID)
	assert.False(t, found)

	_, found = mapStore.Get(firstDocumentID)
	assert.True(t, found)
}
