package config

import (
	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/cdp"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
)

// GlobalConfig for application.
type GlobalConfig struct {
	Log        log.Config
	RenderPool cdp.PoolConfig
	Factory    cdp.FactoryConfig
	CdpRunner  cdp.RunnerConfig
	CdpRender  cdp.RenderConfig
	Templates  fileloader.Config
	Tracing    TracingConfig
}

// DefaultGlobalConfig build default GlobalConfig.
func DefaultGlobalConfig() GlobalConfig {
	return GlobalConfig{
		Log:        log.DefaultConfig(),
		CdpRunner:  cdp.DefaultRunnerConfig(),
		CdpRender:  cdp.DefaultRenderConfig(),
		RenderPool: cdp.DefaultPoolConfig(),
		Factory:    cdp.DefaultFactoryConfig(),
		Templates:  fileloader.DefaultConfig(),
		Tracing:    DefaultTracingConfig(),
	}
}
