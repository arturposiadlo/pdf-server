package config

import (
	"context"
	"fmt"
	"time"

	"github.com/bombsimon/logrusr/v3"
	"github.com/urfave/cli/v2"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdkTrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/c0va23/pdf-server/app/log"
)

const (
	tracingProviderLoggerName = "tracing-provider"
)

// UnknownTracingCollectorError returned when provided unknown tracing collector.
type UnknownTracingCollectorError struct {
	value string
}

// NewUnknownTracingCollectorError constructs new UnknownTracingCollectorError.
func NewUnknownTracingCollectorError(value string) *UnknownTracingCollectorError {
	return &UnknownTracingCollectorError{
		value: value,
	}
}

// Error implements error interface.
func (err *UnknownTracingCollectorError) Error() string {
	return fmt.Sprintf(
		"unknown tracing collector: %s (allowed values: %s)",
		err.value,
		allowedTracingCollectors(),
	)
}

// TracingCollector type.
type TracingCollector string

const (
	otelTracingCollector = TracingCollector("otel")
)

func allowedTracingCollectors() []TracingCollector {
	return []TracingCollector{
		otelTracingCollector,
	}
}

// Set implements github.com/urfave/cli/v2.Generic interface.
func (collector *TracingCollector) Set(value string) error {
	for _, allowedCollector := range allowedTracingCollectors() {
		if string(allowedCollector) == value {
			*collector = allowedCollector

			return nil
		}
	}

	return NewUnknownTracingCollectorError(value)
}

// String implements fmt.Stringer interface.
func (collector TracingCollector) String() string {
	return string(collector)
}

// UnknownOtelCollectorProtocolError can be returned when provided unknown protocol
// for OTEL collector.
type UnknownOtelCollectorProtocolError struct {
	value string
}

// NewUnknownOtelCollectorProtocolError constructs new UnknownOtelCollectorProtocolError.
func NewUnknownOtelCollectorProtocolError(value string) *UnknownOtelCollectorProtocolError {
	return &UnknownOtelCollectorProtocolError{
		value: value,
	}
}

// Error implements error interface.
func (err *UnknownOtelCollectorProtocolError) Error() string {
	return fmt.Sprintf(
		"unknown otel collector protocol: %s (allowed values: %s)",
		err.value,
		allowedOtelCollectorProtocols(),
	)
}

// OtelCollectorProtocol type.
type OtelCollectorProtocol string

const (
	grpcOtelCollectorProtocol = OtelCollectorProtocol("grpc")
	httpOtelCollectorProtocol = OtelCollectorProtocol("http")
)

func allowedOtelCollectorProtocols() []OtelCollectorProtocol {
	return []OtelCollectorProtocol{
		grpcOtelCollectorProtocol,
		httpOtelCollectorProtocol,
	}
}

// Set implements github.com/urfave/cli/v2.Generic interface.
func (protocol *OtelCollectorProtocol) Set(value string) error {
	for _, allowedProtocol := range allowedOtelCollectorProtocols() {
		if string(allowedProtocol) == value {
			*protocol = allowedProtocol

			return nil
		}
	}

	return NewUnknownOtelCollectorProtocolError(value)
}

// String implements fmt.Stringer interface.
func (protocol OtelCollectorProtocol) String() string {
	return string(protocol)
}

const (
	defaultGrpcOtelCollectorAddr = "localhost:4317"
)

// GrpcOtelCollectorConfig is config for gRPC protocol of OTEL collector.
type GrpcOtelCollectorConfig struct {
	Addr string
}

// DefaultGrpcOtelCollectorConfig constructs GrpcOtelCollectorConfig with default values.
func DefaultGrpcOtelCollectorConfig() GrpcOtelCollectorConfig {
	return GrpcOtelCollectorConfig{
		Addr: defaultGrpcOtelCollectorAddr,
	}
}

// BindFlags of GrpcOtelCollectorConfig for github.com/urfave/cli/v2.Command.
func (config *GrpcOtelCollectorConfig) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "otel-collector-grpc-addr",
			EnvVars:     []string{"OTEL_COLLECTOR_GRPC_ADDR"},
			Usage:       "gRPC receiver address of OTEL collector. Format ip:port",
			Destination: &config.Addr,
			Value:       config.Addr,
		},
	}
}

const (
	defaultHTTPOtelCollectorAddr = "localhost:4318"
)

// HTTPOtelCollectorConfig is config for HTTP protocol of OTEL collector.
type HTTPOtelCollectorConfig struct {
	Addr string
}

// DefaultHTTPOtelCollectorConfig constructs HTTPOtelCollectorConfig with default values.
func DefaultHTTPOtelCollectorConfig() HTTPOtelCollectorConfig {
	return HTTPOtelCollectorConfig{
		Addr: defaultHTTPOtelCollectorAddr,
	}
}

// BindFlags of HTTPOtelCollectorConfig for github.com/urfave/cli/v2.Command.
func (config *HTTPOtelCollectorConfig) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "otel-collector-http-addr",
			EnvVars:     []string{"OTEL_COLLECTOR_HTTP_ADDR"},
			Usage:       "HTTP receiver address of OTEL collector. Format ip:port",
			Destination: &config.Addr,
			Value:       config.Addr,
		},
	}
}

const (
	defaultOtelCollectorProtocol = grpcOtelCollectorProtocol
)

// OtelCollectorConfig is config for OTEL collector.
type OtelCollectorConfig struct {
	Protocol OtelCollectorProtocol
	Grpc     GrpcOtelCollectorConfig
	HTTP     HTTPOtelCollectorConfig
}

// DefaultOtelCollectorConfig constructs OtelCollectorConfig with default values.
func DefaultOtelCollectorConfig() OtelCollectorConfig {
	return OtelCollectorConfig{
		Protocol: defaultOtelCollectorProtocol,
		Grpc:     DefaultGrpcOtelCollectorConfig(),
		HTTP:     DefaultHTTPOtelCollectorConfig(),
	}
}

// BindFlags returns bindings of OtelCollectorConfig for github.com/urfave/cli/v2.Command.
func (config *OtelCollectorConfig) BindFlags() []cli.Flag {
	flags := []cli.Flag{
		&cli.GenericFlag{
			Name:    "otel-collector-protocol",
			EnvVars: []string{"OTEL_COLLECTOR_PROTOCOL"},
			Usage: fmt.Sprintf(
				"Connection protocol for OTEL collector. Allowed: %s",
				allowedOtelCollectorProtocols(),
			),
			Value: &config.Protocol,
		},
	}

	flags = append(flags, config.HTTP.BindFlags()...)
	flags = append(flags, config.Grpc.BindFlags()...)

	return flags
}

const (
	defaultServiceName        = "pdf-server"
	defaultTracingDialTimeout = 15 * time.Second
	defaultTracingCollector   = otelTracingCollector
)

// TracingConfig used for enabling and configuration of tracing.
type TracingConfig struct {
	Collector     TracingCollector
	ServiceName   string
	OtelCollector OtelCollectorConfig
	DialTimeout   time.Duration
	Enabled       bool
}

// DefaultTracingConfig returns TracingConfig with default values.
func DefaultTracingConfig() TracingConfig {
	return TracingConfig{
		Enabled:       false,
		ServiceName:   defaultServiceName,
		OtelCollector: DefaultOtelCollectorConfig(),
		DialTimeout:   defaultTracingDialTimeout,
		Collector:     defaultTracingCollector,
	}
}

// BindFlags returns TracingConfig bindings for github.com/urfave/cli/v2.Command.
func (config *TracingConfig) BindFlags() []cli.Flag {
	return append(
		[]cli.Flag{
			&cli.BoolFlag{
				Name:        "tracing-enabled",
				EnvVars:     []string{"TRACING_ENABLED"},
				Usage:       "Enable tracing",
				Destination: &config.Enabled,
				Value:       config.Enabled,
			},
			&cli.GenericFlag{
				Name:    "tracing-collector",
				EnvVars: []string{"TRACING_COLLECTOR"},
				Usage: fmt.Sprintf(
					"Tracing collector type. Allowed values: %s",
					allowedTracingCollectors(),
				),
				Value: &config.Collector,
			},
			&cli.DurationFlag{
				Name:        "tracing-dial-timeout",
				EnvVars:     []string{"TRACING_DIAL_TIMEOUT"},
				Usage:       "Dial tracing collector timeout. Format: [0-9]+(ms|s|m|h)",
				Destination: &config.DialTimeout,
				Value:       config.DialTimeout,
			},
			&cli.StringFlag{
				Name:        "tracing-service-name",
				EnvVars:     []string{"TRACING_SERVICE_NAME"},
				Usage:       "Reported service name to tracing collector",
				Destination: &config.ServiceName,
				Value:       config.ServiceName,
			},
		},
		config.OtelCollector.BindFlags()...,
	)
}

func configureOtelLogging(ctx context.Context) {
	logger := log.LoggerFromContext(ctx, tracingProviderLoggerName)
	logrusR := logrusr.New(logger)
	otel.SetLogger(logrusR)

	otel.SetErrorHandler(otel.ErrorHandlerFunc(func(err error) {
		logger.WithError(err).Warn("OTEL error")
	}))
}

// ProvideTracerProvider wire constructor for tracer.
func ProvideTracerProvider(
	ctx context.Context,
	config TracingConfig,
) (
	tracerProvider trace.TracerProvider,
	cleaner func(),
	err error,
) {
	configureOtelLogging(ctx)

	if !config.Enabled {
		return trace.NewNoopTracerProvider(), noopCleaner, nil
	}

	tracerProvider, cleaner, err = newTracerProvider(ctx, config)
	if err != nil {
		return nil, nil, fmt.Errorf("tracer provider: %w", err)
	}

	return tracerProvider, cleaner, nil
}

func noopCleaner() {}

func newTracerProvider(
	ctx context.Context,
	config TracingConfig,
) (
	tracerProvider *sdkTrace.TracerProvider,
	cleaner func(),
	err error,
) {
	logger := log.LoggerFromContext(ctx, tracingProviderLoggerName)

	res, err := resource.New(ctx, resource.WithAttributes(
		semconv.ServiceNameKey.String(config.ServiceName),
	))
	if err != nil {
		return nil, noopCleaner, fmt.Errorf("resource: %w", err)
	}

	traceExporter, err := newTracerExporter(ctx, config)
	if err != nil {
		return nil, noopCleaner, fmt.Errorf("new exporter: %w", err)
	}

	logger.Info("tracing exporter initialized")

	spanProcessor := sdkTrace.NewBatchSpanProcessor(traceExporter)

	tracerProvider = sdkTrace.NewTracerProvider(
		sdkTrace.WithSpanProcessor(spanProcessor),
		sdkTrace.WithResource(res),
	)

	cleaner = func() {
		logger := log.LoggerFromContext(ctx, tracingProviderLoggerName)

		if err := tracerProvider.Shutdown(ctx); err != nil {
			logger.WithError(err).Error("shutdown tracer error")
		}
	}

	return tracerProvider, cleaner, nil
}

func newTracerExporter(
	ctx context.Context,
	config TracingConfig,
) (
	exporter sdkTrace.SpanExporter,
	err error,
) {
	ctxWithTimeout, cancelCtx := context.WithTimeout(ctx, config.DialTimeout)
	defer cancelCtx()

	switch config.Collector {
	case otelTracingCollector:
		exporter, err = newOtelCollectorExporter(ctxWithTimeout, config.OtelCollector)
		if err != nil {
			return nil, fmt.Errorf("new otel exporter: %w", err)
		}
	default:
		return nil, NewUnknownTracingCollectorError(config.Collector.String())
	}

	return exporter, nil
}

func newOtelCollectorExporter(
	ctx context.Context,
	config OtelCollectorConfig,
) (
	exporter sdkTrace.SpanExporter,
	err error,
) {
	logger := log.LoggerFromContext(ctx, tracingProviderLoggerName)
	logger.Debugf("use OTEL collector with proto %s", config.Protocol)

	switch config.Protocol {
	case grpcOtelCollectorProtocol:
		exporter, err = newGrpcOtelCollectorExporter(ctx, config.Grpc)
		if err != nil {
			return nil, fmt.Errorf("new grpc exporter: %w", err)
		}
	case httpOtelCollectorProtocol:
		exporter, err = newHTTPOtelCollectorExporter(ctx, config.HTTP)
		if err != nil {
			return nil, fmt.Errorf("new http exporter: %w", err)
		}
	default:
		return nil, NewUnknownOtelCollectorProtocolError(config.Protocol.String())
	}

	return exporter, nil
}

func newGrpcOtelCollectorExporter(
	ctx context.Context,
	config GrpcOtelCollectorConfig,
) (
	sdkTrace.SpanExporter,
	error,
) {
	logger := log.LoggerFromContext(ctx, tracingProviderLoggerName)
	logger.Debugf("connect OTEL collector by gRPC to %s", config.Addr)

	grpcCredentials := insecure.NewCredentials()

	grpcConn, err := grpc.DialContext(
		ctx,
		config.Addr,
		grpc.WithTransportCredentials(grpcCredentials),
	)
	if err != nil {
		return nil, fmt.Errorf("grpc dial: %w", err)
	}

	otelConn := otlptracegrpc.WithGRPCConn(grpcConn)

	exporter, err := otlptracegrpc.New(ctx, otelConn)
	if err != nil {
		return nil, fmt.Errorf("new grpc trace: %w", err)
	}

	return exporter, nil
}

func newHTTPOtelCollectorExporter(
	ctx context.Context,
	config HTTPOtelCollectorConfig,
) (
	sdkTrace.SpanExporter,
	error,
) {
	logger := log.LoggerFromContext(ctx, tracingProviderLoggerName)
	logger.Debugf("connection OTEL collector by HTTP to %s", config.Addr)

	exporter, err := otlptracehttp.New(
		ctx,
		otlptracehttp.WithEndpoint(config.Addr),
		otlptracehttp.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("new http trace: %w", err)
	}

	return exporter, nil
}

// ProvideTracePropagator wire constructor for Propagator.
func ProvideTracePropagator() propagation.TextMapPropagator {
	return propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{},
	)
}
