package main

import (
	cliV2 "github.com/urfave/cli/v2"

	"gitlab.com/c0va23/pdf-server/app/config"
)

// BuildGlobalConfigFlags on args and environment variables.
func BuildGlobalConfigFlags(globalConfig *config.GlobalConfig) []cliV2.Flag {
	flags := []cliV2.Flag{}

	type flagProvider interface {
		BindFlags() []cliV2.Flag
	}

	flagProviders := []flagProvider{
		&globalConfig.Log,
		&globalConfig.Templates,
		&globalConfig.Tracing,
		&globalConfig.CdpRunner,
		&globalConfig.RenderPool,
		&globalConfig.Factory,
		&globalConfig.CdpRender,
	}

	for _, flagProvider := range flagProviders {
		flags = append(flags, flagProvider.BindFlags()...)
	}

	return flags
}
