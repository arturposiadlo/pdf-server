//go:build wireinject
// +build wireinject

package main

import (
	"context"

	"github.com/google/wire"

	"gitlab.com/c0va23/pdf-server/app/config"
	"gitlab.com/c0va23/pdf-server/app/server"
	"gitlab.com/c0va23/pdf-server/app/services"
)

func InitServer(
	ctx context.Context,
	globalConfig config.GlobalConfig,
	httpServerConfig server.HTTPServerConfig,
) (*server.HTTPServer, func(), error) {
	wire.Build(wireSet)
	return nil, nil, nil
}

func BuildValidateService(
	ctx context.Context,
	globalConfig config.GlobalConfig,
) (services.ValidateService, func(), error) {
	wire.Build(wireSet)
	return nil, nil, nil
}
