package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	cliV2 "github.com/urfave/cli/v2"

	"gitlab.com/c0va23/pdf-server/app/config"
	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/server"
	"gitlab.com/c0va23/pdf-server/app/utils/bindings"
)

// BuildServerConfigBindings return cliV2.Flag bindings on server.HTTPServerConfig.
func BuildServerConfigBindings(httpServerConfig *server.HTTPServerConfig) []cliV2.Flag {
	serverFlags := []cliV2.Flag{
		&cliV2.StringFlag{
			Name:        "addr",
			Usage:       "Server bind address",
			Value:       httpServerConfig.Addr,
			Destination: &httpServerConfig.Addr,
			EnvVars:     []string{"SERVER_ADDR"},
		},
		&cliV2.GenericFlag{
			Name:    "stop-timeout",
			Usage:   "Stop server timeout",
			Value:   bindings.NewDuration(&httpServerConfig.StopTimeout),
			EnvVars: []string{"SERVER_STOP_TIMEOUT"},
		},
		&cliV2.BoolFlag{
			Name:        "server-reuse-addr",
			Usage:       "Allow listener to reuse address",
			Value:       httpServerConfig.ReuseAddr,
			Destination: &httpServerConfig.ReuseAddr,
			EnvVars:     []string{"SERVER_REUSE_ADDR"},
		},
	}

	serverFlags = append(serverFlags, httpServerConfig.Static.Flags()...)

	return serverFlags
}

// BuildServerCommand start HTTP server.
func BuildServerCommand(globalConfig *config.GlobalConfig) *cliV2.Command {
	httpServerConfig := server.NewHTTPServerConfig()

	return &cliV2.Command{
		Name:  "server",
		Flags: BuildServerConfigBindings(&httpServerConfig),
		Action: func(cliCtx *cliV2.Context) error {
			ctx := cliCtx.Context

			logger := log.LoggerFromContext(ctx, "server")

			logger.WithField(log.FieldConfig, globalConfig).Info("Global config")
			logger.WithField(log.FieldConfig, httpServerConfig).Info("Server config")

			_, closer, err := InitServer(ctx, *globalConfig, httpServerConfig)
			if nil != err {
				return fmt.Errorf("server initialization: %w", err)
			}

			defer closer()

			waitSignal(logger)

			return nil
		},
	}
}

func waitSignal(logger log.Logger) {
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM)

	receivedSignal := <-signalCh
	logger.Infof("Receive %s signal", receivedSignal)
}
