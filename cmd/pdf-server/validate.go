package main

import (
	"fmt"

	cliV2 "github.com/urfave/cli/v2"

	"gitlab.com/c0va23/pdf-server/app/config"
	"gitlab.com/c0va23/pdf-server/app/log"
)

// BuildValidateCommand parse templates and render all examples.
func BuildValidateCommand(globalConfig *config.GlobalConfig) *cliV2.Command {
	return &cliV2.Command{
		Name:  "validate",
		Usage: "Render all examples of all templates",
		Action: func(cliCtx *cliV2.Context) (err error) {
			ctx := cliCtx.Context

			logger := log.LoggerFromContext(ctx, "validate")

			validateService, closer, err := BuildValidateService(ctx, *globalConfig)
			if nil != err {
				return fmt.Errorf("init validate service: %w", err)
			}

			defer closer()

			if err := validateService.Validate(ctx); err != nil {
				return fmt.Errorf("validate: %w", err)
			}

			logger.Info("Validation successful")

			return nil
		},
	}
}
