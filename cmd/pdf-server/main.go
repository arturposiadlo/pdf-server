package main

import (
	"context"
	"os"

	cliV2 "github.com/urfave/cli/v2"

	"gitlab.com/c0va23/pdf-server/app/config"
	"gitlab.com/c0va23/pdf-server/app/config/version"
	"gitlab.com/c0va23/pdf-server/app/log"
)

func main() {
	globalConfig := config.DefaultGlobalConfig()

	logger := log.
		NewLoggerFromConfig(globalConfig.Log).
		WithField(log.LoggerField, "main")

	ctx := context.Background()

	app := buildApp(&globalConfig)

	if err := app.RunContext(ctx, os.Args); err != nil {
		logger.WithError(err).Fatal("app error")
	}
}

func buildApp(globalConfig *config.GlobalConfig) *cliV2.App {
	versionInfo := version.GetInfo()

	return &cliV2.App{
		Name:    "pdf-server",
		Usage:   "HTTP server for render predefined HTML template to PDF files",
		Version: versionInfo.String(),
		Flags:   BuildGlobalConfigFlags(globalConfig),
		Before: func(cliCtx *cliV2.Context) error {
			ctx := cliCtx.Context

			logger := log.NewLoggerFromConfig(globalConfig.Log)

			logger.Infof("Version: %s", versionInfo)

			cliCtx.Context = log.ContextWithLogger(ctx, logger)

			return nil
		},
		Commands: []*cliV2.Command{
			BuildServerCommand(globalConfig),
			BuildValidateCommand(globalConfig),
		},
	}
}
