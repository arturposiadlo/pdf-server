*** Settings ***
Library  RequestsLibrary
Resource  variables.resource


*** Keywords ***

Response Should be PDF
    [Arguments]  ${templateResponse}
    Should Be Equal  ${templateResponse.headers['Content-Type']}  application/pdf
    Should Match Regexp  ${templateResponse.text}  ${PDF_MAGIC_REGEX}


*** Test Cases ***

Success Mustache Template
    &{requestPayload}=  Create dictionary  value=example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/mustache_template/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}

Bad Request on Mustache Template
    &{requestPayload}=  Create dictionary  invalidKey=example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/mustache_template/render
    ...  expected_status=500
    ...  json=${requestPayload}

Success Go Template
    &{requestPayload}=  Create dictionary  value=example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/go_template/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}

Success Handlebars Template
    @{requestItems}=  Create List  First Item  Second Item  Third Item
    &{requestPayload}=  Create dictionary  value=example  items=${requestItems}

    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/handlebars_partials/render
    ...  expected_status=200
    ...  json=${requestPayload}

    Response Should be PDF  ${templateResponse}

Success Composite Template
    @{iterations}=  Create List  ${1}  ${2}  ${3}  ${4}  ${5}
    &{requestPayload}=  Create dictionary  value=example  iterations=@{iterations}
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/composition:only_static/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}
