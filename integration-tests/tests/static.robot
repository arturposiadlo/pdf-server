*** Settings ***
Library  RequestsLibrary
Resource  variables.resource


*** Keywords ***

Render static template
    [Arguments]  ${templateName}

    &{requestPayload}=  Create dictionary

    ${staticResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/${templateName}/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Should Be Equal  ${staticResponse.headers['Content-Type']}  application/pdf
    Should Match Regexp  ${staticResponse.text}  ${PDF_MAGIC_REGEX}


*** Test Cases ***

Static Template
    Render static template  static_template

Params Template
    Render static template  params_template

Footer Template
    Render static template  footer_template

Assets Template
    Render static template  assets_template
