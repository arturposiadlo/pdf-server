
*** Settings ***
Library  lib.SwaggerLibrary
Library  RequestsLibrary
Resource  variables.resource

*** Test Cases ***

Success Swagger 2.0 spec
    Should be swagger spec  ${PDF_SERVER_BASE_URL}/spec/swagger.json

Success get json schema
    GET  ${PDF_SERVER_BASE_URL}/templates/schema_template/schema.json  expected_status=200

Schema not found on template without schema
    GET  ${PDF_SERVER_BASE_URL}/templates/static_template/schema.json  expected_status=404

Schema not found on unknown template
    GET  ${PDF_SERVER_BASE_URL}/templates/unknown_template/schema.json  expected_status=404

Success get api doc page
    GET  ${PDF_SERVER_BASE_URL}/static/swagger.html  expected_status=200
