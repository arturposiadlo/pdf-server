#!/bin/sh

set -xe

export BROWSER=${1:-chromium}
echo BROWSER=${BROWSER}

FACTORY_TYPES=${3:-runner}
echo "FACTORY_TYPES=${FACTORY_TYPES}"

BUILD_ID=$(uuidgen)

export EXAMPLES_IMAGE=pdf-server:${BUILD_ID}-${BROWSER}
echo "EXAMPLES_IMAGE=${EXAMPLES_IMAGE}"

export ROBOT_IMAGE=pdf-server-robot:${BUILD_ID}

clean() {
    if docker compose down --rmi local --remove-orphans; then
        echo "Docker compose down successful"
    fi

    if docker image rm "${EXAMPLES_IMAGE}"; then
        echo "Image ${EXAMPLES_IMAGE} removed"
    fi

    if docker image rm "${ROBOT_IMAGE}"; then
        echo "Image ${ROBOT_IMAGE} removed"
    fi
}

trap clean EXIT

earthly --output ../examples/+${BROWSER}-image-push --EXAMPLES_IMAGE=${EXAMPLES_IMAGE}

earthly --output +robot-image-push --ROBOT_IMAGE=${ROBOT_IMAGE}

export EXPOSED_PORT=9998

docker compose run --rm  tests sh
