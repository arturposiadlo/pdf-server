#!/bin/sh

set -xe

export PYTHONPATH=${PWD}

export PDF_SERVER_BASE_URL="${PDF_SERVER_BASE_URL:-http://localhost:9999}"

echo "Wait for ${PDF_SERVER_BASE_URL}"

max_tries=10

for i in $(seq 0 "${max_tries}"); do
    if wget -O /dev/null --quiet "${PDF_SERVER_BASE_URL}/_status"; then
        break;
    fi

    if [[ "${i}" = "${max_tries}" ]]; then
        exit 1
    fi

    sleep 1
done

echo "Start tests"

time robot --console verbose --outputdir ./reports/ ./tests/
