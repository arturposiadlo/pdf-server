from swagger_spec_validator import validate_spec_url

class SwaggerLibrary:
    def should_be_swagger_spec(self, url):
        validate_spec_url(url)
